%%This is a very basic article template.
%%There is just one section and two subsections.
\documentclass[a4paper]{article}

\usepackage{palatino}
\usepackage{euler}

\usepackage[british]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[a4paper, margin = 32mm]{geometry}

\usepackage{amsmath,amsthm,amssymb,amstext}

\theoremstyle{plain}
\newtheorem{Theorem}{Theorem}
\newtheorem{InformalTheorem}{Informal Theorem}
\newtheorem{Proposition}[Theorem]{Proposition}
\newtheorem{Corollary}[Theorem]{Corollary}
\newtheorem{Lemma}[Theorem]{Lemma}
\newtheorem{Conjecture}[Theorem]{Conjecture}

\theoremstyle{definition}
\newtheorem{Definition}[Theorem]{Definition}
\theoremstyle{remark}
\newtheorem{Remark}[Theorem]{Remark}

\usepackage{graphicx}
\usepackage{color}

\usepackage{nicefrac}

\usepackage{listings}

\lstset{
  texcl,
  mathescape,
  basicstyle=\small\tt,
  stringstyle=\color{darkred},
  showstringspaces=false,
  keywordstyle=\color{blue}\bfseries,
  commentstyle=\color{gray50}\it,
  numberstyle=\tiny\itshape,
  frame=single,
%  resetmargins,
%  breaklines=true,
%  breakautoindent=true,
  language=Haskell
}
\definecolor{brown}{rgb}{0.7,0.3,0.2}
\definecolor{darkgreen}{rgb}{0.0,0.2,0.0}
\definecolor{darkred}{rgb}{0.5,0.1,0.1}
\definecolor{gray40}{rgb}{0.4,0.4,0.4}
\definecolor{gray50}{rgb}{0.5,0.5,0.5}

\title{Formal verification of exact real limit computation}
\author{Michal Kone\v{c}n\'y\\[1ex] \texttt{\small m.konecny@aston.ac.uk}}
% \affil{Aston University, Birmingham, UK}
% \makeindex

\newcommand{\ie}{i.e.,~}
\newcommand{\eg}{e.g.,~}
\newcommand{\Eg}{E.g.,~}
\newcommand{\etal}{et al.~}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\Qpos}{\Q_{+}}
\newcommand{\B}{\mathbb{B}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Rpos}{\R_{+}}
\newcommand{\D}{\mathbb{D}}
\renewcommand{\P}{\mathcal{P}}

\newcommand{\eleq}{\mathrel{\|{\leq}}}
\newcommand{\inar}{\mathrel{{}_{\R}{\in}}}
\newcommand{\notinar}{\mathrel{{}_{\R}{\notin}}}
% \newcommand{\Qinar}{\mathrel{{}_{\Q}{\in}}}

\newcommand{\eqtol}[1]{\mathrel{\texttt{=}#1\texttt{=}}}
\newcommand{\eqe}{\eqtol{e}}
\newcommand{\leqtol}[1]{\mathrel{\texttt{<}#1\texttt{=}}}
\newcommand{\leqe}{\leqtol{e}}

\newcommand{\inv}{{\mathop{{}^{\texttt{-}1}}}}
\newcommand{\AR}{\mathcal{R}}
\newcommand{\ARQ}{\mathcal{R}_{\Q}}
\newcommand{\ofQ}{\mathrm{of\Q}}
\newcommand{\limAR}{\mathop{\mathtt{lim}}}
\newcommand{\limIX}{\mathop{\texttt{lim-ix}}}

\setlength{\parindent}{0em}
\setlength{\parskip}{1ex}

\begin{document}

\maketitle

\section{Introduction}

To motivate the formal development, we first outline the story of this paper using an example.

Consider a program for computing $\sqrt x$ for a real number $x>1$ using Newton iteration:

\begin{lstlisting}[numbers=left,language=Haskell, commentstyle=\color{darkred},basicstyle=\fontsize{8}{10pt}\tt]
sqrt_Newton(x) = lim (${e \to 0}$, sqrt_Newton_iter($e$, x, x)) 

sqrt_Newton_iter($e$, x, y$_n$)
  | abs(y$_n$ - x/y$_n$) <= $e$     = y$_n$                                -- i.e., $y_n$ close to $\sqrt x$
  | otherwise            = let y$_{n+1}$ = (y$_n$ + x/y$_n$)/2              -- Newton step
                           in sqrt_Newton_iter($e$, x, y$_{n+1}$)      -- recursive call
\end{lstlisting}

\noindent
where $y_n$ and $y_{n+1}$ are real numbers and $e$ is always a positive rational number.
Our goal is to give programs such as \lstinline{sqrt_Newton} 
an executable semantics and formally verify their semantics, \eg that 
\lstinline{sqrt_Newton}
computes the exact $\sqrt x$ for any $1 < x$.
Of particular interest is how to express and verify \emph{terminatation} (for any output accuracy) 
in the presence of recursion and \lstinline{lim}.

\subsection{Executable semantics}

Function \lstinline{sqrt_Newton} 
``computes'' the exact $\sqrt x$ on RealRAM
but this model cannot be faithfully implemented.
With an approximate arithmetic such as floating-point arithmetic,\\ \lstinline{sqrt_Newton_iter}
will typically not terminate for very small $e$ and moreover, after fixing this issue, 
verification is very difficult.

We advocate using \emph{exact real arithmetic} \cite{irram2001,aern2018} to execute such programs exactly
with an intuitive well-behaved semantics, making verification feasible.
Real numbers are usually represented using a variant of Cauchy sequences.
For topological reasons each real number will necessarily be represented by many different sequences.
A function that operates on such sequences as inputs and outputs is called \emph{extensional} if it encodes
the same real number function irrespective of which sequences are chosen for the input real numbers.
A non-extensional sequence function can be used to implement a non-deterministic real function.

In the above program we stumble on the fact the comparison on line 4 is not computable:
When $|y_n - x/y_n|=e$, no matter how good (but still not exact) 
approximations we have for $x$ and $y_n$,
we cannot decide the branch condition $|y_n - x/y_n|\le e$.
% This problem can be solved by using parallel-if \cite{RealPCF}, ie executing both branches in 
% parallel until the branch condition is decided and assume that both branches 
% return the same number in cases where it is never decided.
% Another, more efficient solution is to 
A standard solution for this problem is to replace 
\lstinline{abs(y$_n$ - x/y$_n$) <= $e$}
with the non-deterministic approximate comparison
\lstinline{y$_n$ $\eqe$ x/y$_n$} 
which returns true if the difference between the numbers is less than $e/2$,
returns false if the difference is over $e$ and 
returns either true of false if the difference is
between $e/2$ and $e$.  (This is analogous to the behaviour of iRRAM function \lstinline{bound}
\cite{iRRAM}.)
This comparison is deterministic on the level
of real number encodings as sequences 
but since it is not extensional, it
is non-deterministic at the level of denotational semantics with real numbers.

With this change, we can execute \lstinline{sqrt_Newton} 
in exact real arithmetic.
Note that the limit \lstinline{sqrt_Newton} 
is extensional despite using non-extensional \lstinline{sqrt_Newton_iter}. 


\subsection{Formalisation of semantics}

We first define a suitable minimal programming language and
its exact real operational semantics in a theorem prover.
We base our formalisation on CIDR-F1, the first-order
functional fragment of CIDR, a minimal imperative language
for verifiable real number computation, currently under
development with coleagues within CID, EU RISE project
731143.

Formalising operational semantics of CIDR requires a
formalisation of exact real arithmetic.  Developing an
efficient formally verified exact real arithmetic is a
fairly big undertaking and offers many choices.  To get a
more stable semantics independent of these choices, (in Section~\ref{sec:pera}) we
axiomatise an abstract exact real arithmetic and (in Section~\ref{sec:opsem}) define
CIDR-F1 semantics relative to this axiomatisation instead of
using any specific arithmetic.  Thus instead of saying that
a real number is a sequence of some type, we say that real
numbers are a data type whose signature includes operations
such as $+$, $\div$, $\eqe$, \lstinline{lim}.

An operational semantics of a functional language can be
either \emph{small-step} or \emph{big-step}.
A small-step semantics defines a step-wise evolution of a
program term into a term that is equal to a final value: 
\[
    (i_1,t_1) \rightsquigarrow (i_2,t_2) \rightsquigarrow 
    \cdots 
    \rightsquigarrow
    (i_n, t_n)
    \quad
    \text{where}
    \quad
    t_n \in \mathrm{Val} \subset \mathrm{Term}
\]
where $\rightsquigarrow$ is defined using the function:
\[
    \textrm{small-step-opsem} {:{}} (\mathrm{FnName}\to \mathrm{Term}) \to 
    \mathrm{Input} \times \mathrm{Term} \to \mathrm{Input} \times\mathrm{Term}
\]
while a big-step semantics relates a program and its input to a result value without explicit
intermediate states:
\[
    \textrm{big-step-opsem} {:{}} (\mathrm{FnName}\to \mathrm{Term}) \to \mathrm{Term} 
    \to \mathrm{Input} \to \mathrm{Val} 
\]

\subsection{Non-termination}

We wish to express operational semantics of CIDR-F1 as a
function which we can use to extract verified executables
from formalised programs.
This means that the operational semantics is a potentially
non-terminating function.

An argument in favour of small-step semantics is that one
could represent \emph{non-termination} by an infinite
sequence of steps.
Theorem provers such as Coq and Isabelle have some support
for co-induction and co-recursive type definitions that
would allow one to define a total semantics that returns a
potentially infinite sequence of steps when the program does
not terminate. One can then prove that a specific program
terminates by showing that its step sequence is finite.

We use a big-step semantics, as it is easier to formalise
and gives more efficient code than small-step semantics.  
Instead of allowing infinite sequences of steps, non-termination will be dealt
with by making the semantics depend on specific resource
bounds (corresponding to ``fuel'' in \cite{McBride-MPC-2015}).
If resources are exhausted, the result is an
exception (represented, \eg by the value $\top$).
For a functional language, the only resource that needs to
be specified is the stack size, \ie the number of nested
function calls:
\[
    \mathrm{opsem} {:{}} (\mathrm{FnName}\to \mathrm{Term}) \to \mathrm{Term} 
    \to (\mathrm{stackBound}:\N) \to \mathrm{Input} \to \mathrm{Val}^\top 
\]

\subsection{Partial and inconsistent reals}

Functions such as \lstinline{sqrt_Newton_iter} could be
given an operational semantics that takes exact reals as
input and returns either an exact real number or $\top$.
The situation gets more complicated for programs with
limits, such as \lstinline{sqrt_Newton}.
Our limit takes more and more resources with increasing
accuracy and thus may return a real number approximation for a
lower accuracy, but fail for a higher accuracy.
To give semantics to such limits, our abstract exact real
arithmetic need to support ``partial'' real numbers that are
defined only up to some accuracy.
Our exact real abstract data type signature includes the relations 
${\eleq}$ and ${\inar}$  which can be used to make statements such as:
\[
\begin{array}{ll}
    x \inar X & \text{(abstract) real number $x$ is compatible with (concrete) partial real number $X$}
    \\
    X \eleq \varepsilon & \text{meaning: }x_1\inar X, x_2\inar X \implies |x_1-x_2| \leq \varepsilon
\end{array}
\]
In \(x \inar X\) the number $x$ ranges over the non-constructive algebraic structure of real numbers,
which is available in most theorem provers,
while $X$ ranges over constructive real numbers, represented, \eg as Cauchy sequences, which can be
used directly as values in computation.

As we cannot generally rule out inconsistent limits such as $\lim_{e \to 0} 1/e$, 
our exact real arithmetic must support \emph{inconsistent values}.
Nevertheless, for an inconsistent value $X$ it will never hold $x \inar X$ nor $X \eleq \varepsilon$.

For each operation on our abstract data type, we postulate that the
operation is safe with regards to the corresponding real number operation and that
the partial real operation has a (non-uniform) modulus of continuity, 
implying that it preserves consistency and converges for exact inputs.  For example:
\[
    \begin{array}{rclcll}
    x\inar X&\land& y \inar Y \;\land\; 0\notin Y&\implies& x/y\inar X/Y
    &\text{(safety)}
    \\[1ex]
    X\eleq\omega_{\div,L}(X,Y,\varepsilon)&\land& 
    Y\eleq\omega_{\div,R}(X,Y,\varepsilon)&\implies& 
    X/Y\eleq \varepsilon
    &\text{(modulus)}
    \end{array}
\]
(These statements are simplified. For the complete version, see Subsection~\ref{ssec:ar-arith}).

\subsection{Specification}

We specify the meaning of CIDR-F1 programs using pre- and post-conditions.
For example, for \lstinline{sqrt_Newton_iter($e$,x,y)} we have as precondition:
\[
    \mathrm{pre}(e,x,y): \qquad 
        1 \leq x
        \;\land\; \sqrt x \leq y 
        \;\land\; y \leq x
        \;\land\; 0 < e
\]
and as postcondition for result $r$:
\[
    \mathrm{post}(e, x,y, r): \qquad 
    \sqrt x \leq r \;\land\; |r - x/r| \leq e
\]
These conditions do not take into account termination, consistency and convergence or partial reals.
To guarantee termination, we add the following pre-condition for the stack bound $s$:
\[
    \textrm{stack-pre}(s,e,x,y): \qquad 
    2^{s} \geq \frac{2(y^2-x)}{e\cdot y}
\]

Safety and consistency of a partial result $R$ in the
presence of partial input values are specified as follows:
\[
    R \neq \top
    \;\land\;
    \Big(\forall x \inar X, y \inar Y.\; \mathrm{pre}(e,x,y)
    \implies
    (\exists r\in R.\; \mathrm{post}(e,x,y,r))\Big)
\]
Termination and convergence is specified using specific moduli of continuity $\omega_x$
and $\omega_y$:
\[
    \forall \varepsilon > 0.\;
    \left\{
    \begin{array}{l}
    X \eleq \omega_x(e,x,y),\;
    Y \eleq \omega_y(e,x,y)
    \\[1ex]
    \big(\forall x \inar X, y \inar Y.\; \mathrm{pre}(e,x,y)
    \;\land\; \textrm{stack-pre}(s,e,x,y)\big)
    \end{array}
    \right\}
    \implies
    R \neq \top\;\land\; R \eleq \varepsilon
\]
Let $\mathrm{spec}(\lstinline{sqrt_Newton_iter})(s,e,X,Y,R)$ stand for the two statements above, 
formally stating safety, consistency, termination and convergence of function 
\lstinline{sqrt_Newton_iter}.
Specifications $\mathrm{spec}(f)(s,\mathit{Params},R)$ for other real number functions $f$ will be stated similarly. 
\Eg our $\mathrm{spec}(\lstinline{sqrt_Newton})(s,X,R)$
differs essentially only in the preconditions and postcondition:
\[
\begin{array}{rl}
    \mathrm{pre}(x):& 1 \leq x
    \\
    \mathrm{post}(x,r):& r = \sqrt x
    \\
    \textrm{stack-pre}(s,x,\varepsilon):& s \ge \max(1,1+\log_2(\nicefrac{2(x-1)}{\varepsilon}))
\end{array}
\]
Note that here the stack pre-condition features $\varepsilon$, the result accuracy target,
as this function uses a limit whose stack usage grows with accuracy. 
This is OK since $\varepsilon$ is available where the stack precondition appears:
\[
    \forall \varepsilon > 0.\;
    \left\{
    \begin{array}{l}
    X \eleq \omega_x(x)
    \\[1ex]
    \big(\forall x \inar X.\; \mathrm{pre}(x)
    \;\land\; \textrm{stack-pre}(s,x,\varepsilon)\big)
    \end{array}
    \right\}
    \implies
    R \neq \top\;\land\; R \eleq \varepsilon
\]

\subsection{Verification}

Assume a finite program $P{{:\subseteq}{}} \mathrm{FnName} \to \mathrm{Term}$.
A correctness theorem for a function $f$ in $P$ can be stated as follows:
\[
    \mathrm{spec}(f)(s,\mathit{Params},R)
    \quad\text{where}\quad
    R = \mathrm{opsem}(P,P(f),s,\mathit{Params}) 
\]

% A program specification $\mathrm{spec}(P)$ is the collections of
% function specifications $\mathrm{spec}(f)$ for all functions $f$ in program $P$.

For recursively defined functions, we prove the above correctness theorem
indirectly, using a non-deterministic denotational semantics:
\[
\begin{array}{l}
    \mathrm{dsem} {:{}} (\mathrm{FnName}\to \mathrm{Spec}) \to \mathrm{Term} \to \mathrm{Spec}
    \\
    \text{where}\quad
    \mathrm{Spec} = (\mathrm{stackBound}:\N) \to \mathrm{Input} \to \mathrm{Val}^\top \to \mathrm{Bool}
\end{array}
\]
Thus $\mathrm{dsem}$ turns program specification $\mathrm{spec}(P)$ and term $t$
into a specification of the function defined by term $t$.

Denotational semantics safely approximates operational semantics, \ie
\[
    R = \mathrm{opsem}(P,t,s,\mathit{Params}) \implies 
    \mathrm{dsem}(\mathrm{spec}(P),t,s,\mathit{Params},R)
\]
if each specifications $\mathrm{spec}(f)$ safely approximates the denotational semantics of
the term $P(f)$, \ie
\begin{equation}\label{eq:f-dsem-implies-spec}
    \mathrm{dsem}(\mathrm{spec}(P),P(f),s,\mathit{Params},R)
    \implies
    \mathrm{spec}(f)(s,\mathit{Params},R).
\end{equation}
Thus, as long as we prove implication \ref{eq:f-dsem-implies-spec} for all functions we call, 
the specification of the functions as well as specifications obtained 
by denotational semantics are correct with respect to the operational semantics.

\section{Arithmetic of partial exact real numbers}
\label{sec:pera}

Formally, we define the abstract type of partial exact real numbers $\AR$ as a multi-sorted algebraic structure with signature:
\[
\begin{array}{rl}
    \inar & \subseteq \R \times \AR
    \\[1ex]
    \eleq & \subseteq \AR \times \Rpos
    \\[1ex]
    \ofQ & : \Q \to \AR
    \\[1ex]
    +,-,*,\div & : \AR \times \AR \to \AR
    \\[1ex]
    \omega_{+,L}, \omega_{+,R}, \omega_{*,L}, \omega_{*,R}, \omega_{\div,L}, \omega_{\div,R} & : \R \times \R \to (\Rpos \to \Rpos)
%     \\[1ex]
%     -, \inv & : \AR \to \AR
%     \\[1ex]
%     \omega_\inv & : \R \to (\Qpos \to \Qpos)
    \\[1ex]
    \eqtol{\textunderscore} & : \Qpos \times \AR \times \AR \to \B^\top
    \\[1ex]
    \leqtol{\textunderscore} & : \Qpos \times \AR \times \AR \to \B^\top
    \\[1ex]
    \limAR & : (\Qpos \to \AR^\top) \to \AR
    \\[1ex]
    \limIX & : \Qpos \to \Qpos
\end{array}
\]
that satisfies the axioms stated in the remainder of this section.

In our Isabelle formalisation, this abstract type is encoded as a type class.  
To aid Isabelle type inference, all of the above operators that do not feature the type $\AR$ 
have an additional dummy parameter of type $\AR$.
Also, in our Isabelle encoding we use type $\Q$ instead of $\Qpos$ and state the positivity
constraint explicitly where necessary. 

To validate the consistency of these axioms and to execute our programs,
we developed $\ARQ$, a model of partial exact real numbers with
the carrier set $\Qpos \to (\Q\times \Q_{\ge 0})^\top$ where the two
rational numbers $(c,r)$ returned for some input $e$ are interpreted as 
the centre and radius of a compact interval.
Typically, but not always, $r\leq e/2$ and the set of intervals for all $e$ have a non-empty intersection. 
More efficient implementations are likely to use dyadic numbers instead of rational numbers.
Partial real numbers return $\top$ for sufficiently small $e$ while exact numbers
should never return $\top$.
This paper does not give further details of $\ARQ$ except a few aspects for illustration.   

\subsection{Basics}

The relation $X \eleq \varepsilon$ primarily bounds
the diameter of the set of reals contained in $X$:
\[
    X \eleq \varepsilon \implies x_1,x_2 \inar X \implies |x_1-x_2| \leq \varepsilon
\]
Moreover, it implies that $X$ is consistent:
\[
    X \eleq \varepsilon \implies \exists x.\, x \inar X
\]
Thus, while $\limAR f$ always returns an element of $\AR$, unless $f$ is
consistent, we cannot prove $\limAR f \eleq \varepsilon$ for any $\varepsilon$.
One could argue that the signature should contain a separate consistency predicate.
For the sake of simpler proofs, we ``hide'' consistency in $\eleq$ 
since consistency is almost always implied from convergence.
% To further simplify formal proofs, we forbid $\varepsilon = 0$:
% \[
%     X \eleq \varepsilon \implies \varepsilon > 0
% \]

The bounding predicate is closed under increasing $\varepsilon$:
\[
    \varepsilon_1 < \varepsilon_2  \implies X \eleq \varepsilon_1 \implies X \eleq \varepsilon_2
\]

Since $\varepsilon$ is strictly positive, we express that $X$ is exact using a quantifier over all $\varepsilon$:
\[
    \textrm{is-exact}(X) \equiv (\forall \varepsilon > 0. \, X \eleq \varepsilon)
\]
Embedding of rational numbers is required to have the natural properties:
\[
    q \inar \ofQ(q)
    \qquad
    \textrm{is-exact}(\ofQ(q)) 
\]

% Let \(q\Qinar X\) be a shortcut for $q \inar X \;\land\; q\in\Q$.
% The next axiom states that a consistent partial real number that does not contain any
% rational number has to be exact:
% \[
%     (\exists x.\,x\inar X)\implies \neg(\exists q.\,q\Qinar X) \implies \textrm{is-exact}(X)
% \] 

\subsection{Field operations}

\label{ssec:ar-arith}

We state in detail only the properties of the division operator $\div$.
Axioms for the other operations ($+$, $-$, $*$) and their moduli of continuity are analogous, 
except the precondition $0\notinar Y$ is dropped and subtraction shares moduli of continuity
with addition.
% and for unary operators there
% is only one modulus of continuity instead of two.

The division operator must satisfy \emph{safety}:
\[
    0\notinar Y  \implies 
    x\inar X \;\land\; y \inar Y \implies  
    x/y\inar X/Y
\]
and \emph{convergence}:
\[
    \left\{
    \begin{array}{l}
        \exists x.x\inar X  
        \\
        \exists y.y\inar Y  
        \\
        0\notinar Y
        \\
        \varepsilon > 0
    \end{array}
    \right\}    
    \implies
    \left\{
    \begin{array}{l}
        \big(\forall x\inar X, y\inar Y\big)\big(\omega_{\div,L}(x,y,\varepsilon) > 0\;\land\; \omega_{\div,R}(x,y,\varepsilon) > 0\big)
        \\[1ex]
        \Big(\big(\forall x\inar X, y\inar Y\big)\big(
        X\eleq\omega_{\div,L}(x,y,\varepsilon) \;\land\;
        Y\eleq\omega_{\div,R}(x,y,\varepsilon)\big)
        \\
        \qquad \implies 
            X/Y\eleq \varepsilon\Big)
    \end{array}
    \right\}
\]
Intuitively, the above states that for any legal and consistent parameters 
and for any positive $\varepsilon$, 
if the diameters of $X$ and $Y$ are bounded by the
respective moduli of continuity for any \emph{rational} $x\inar X$ and $y\inar Y$, 
then the result $X/Y$ is bounded by $\varepsilon$.
Moreover, to ensure that the convergence axiom is never empty, it includes 
a requirement that moduli of continuity are positive.
% \[
%     (\forall x,y\in\R,\,y\neq 0)
%     \big(\omega_{\div,L}(x,y,\varepsilon) > 0\;\land\; \omega_{\div,R}(x,y,\varepsilon) > 0\big)
% \] 


Note that the moduli of continuity must depend on the magnitude of the parameters
as the derivative of division is unbounded in the neighbourhood of 0.
The same holds for unbounded multiplication.  
For addition, the modulus can be uniform in $x,y$.  For example, $\ARQ$
has $\omega_{+,L}(x,y,\varepsilon) = \omega_{+,R}(x,y,\varepsilon) = \varepsilon/2$.

It is tempting to parametrise the moduli directly by $X$ and $Y$, but it
would allow one to write moduli that satisfy the corresponding rules by 
``cheating'', \ie making the modulus depend on the diameter of $X$ and $Y$
instead of just its magnitude.

\subsection{Approximate comparison}

The approximate equality operator $\eqtol{\textunderscore}$ is characterised by the following three axioms:
\[
\begin{array}{ll}
    x \inar X,\; y \inar Y,\; |x-y|\leq e/2 & \implies (X \eqe Y) \neq \texttt{false}
    \\[1ex]
    x \inar X,\; y \inar Y,\; |x-y|> e & \implies (X \eqe Y) \neq \texttt{true}
    \\[1ex]
    X \eleq e/4, \quad Y \eleq e/4 & \implies (X \eqe Y) \neq \top
\end{array}
\]
and the inequality comparison operator by:
\[
\begin{array}{ll}
    x \inar X,\; y \inar Y,\; x-y< -e & \implies (X \leqe Y) \neq \texttt{false}
    \\[1ex]
    x \inar X,\; y \inar Y,\; x-y> e & \implies (X \leqe Y) \neq \texttt{true}
    \\[1ex]
    X \eleq e, \quad Y \eleq e & \implies (X \leqe Y) \neq \top
\end{array}
\]
Our $0\eqe x$ and $0\leqe x$ have similar properties
as iRRAM's functions \lstinline{bound(x,k)} and \lstinline{positive(x,k)}, respectively,
with $e=2^{-k}$.

The ``definedness'' axioms above could be strengthened by allowing different diameter bounds for
$X$ and $Y$.  We use these simpler versions as we are currently not aware of an application where
the stronger axioms would be useful.

\subsection{Limits}

The value $\limAR f$ is defined even for an inconsistent ``sequence'' $f{:{}} \Qpos\to \AR^\top$.  Nevertheless, we cannot say anything
about safety or convergence of the limit unless $f$ is consistent.  Consistency of $f$ with some real number $x$
 is defined as follows:
\[
    \textrm{seq-consistent}(x,f) \equiv
    (\forall e \in\Qpos)\big(f(e) = \top \;\lor\; (\exists y)(y \inar f(e) \;\land\; |x-y| \leq e)\big) 
\]
Satefy axiom for limits is simply:
\[
    \textrm{seq-consistent}(x,f) \implies x \inar \limAR f
\]
while convergence is a bit more subtle:
\[
    (\exists x)(\textrm{seq-consistent}(x,f))
    \implies
    (\forall e \in\Qpos)
    \Big(\big(f(e') \neq \top \;\land\; f(e') \eleq e-e'\big) \implies \limAR f \eleq e\Big)
\]
where $e' = \limIX(e)$.  
Notice that here $e$ ranges over $\Qpos$ while for $\div$ it ranges over $\Rpos$.
Restriction to rationals is necessary since we derive $e'$ from $e$ and $e'$ is a rational
index for $f$.  The only continuous functions from reals to rationals are the constant functions,
which are clearly not suitable as $\limIX$.

Finally, we add an axiom that prevents convergence failures due to unsuitable $e'$:
\[
    0 < \limIX(e) < e
\]
\section{Syntax and operational semantics}



\section{Denotational semantics}

\section{Conclusion}

\end{document}
