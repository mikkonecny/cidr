\documentclass[a4paper]{article}

\usepackage[british]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[a4paper, margin = 32mm]{geometry}


\usepackage{amsmath,amsthm,amssymb,amstext}

\theoremstyle{plain}
\newtheorem{Theorem}{Theorem}
\newtheorem{InformalTheorem}{Informal Theorem}
\newtheorem{Proposition}[Theorem]{Proposition}
\newtheorem{Corollary}[Theorem]{Corollary}
\newtheorem{Lemma}[Theorem]{Lemma}
\newtheorem{Conjecture}[Theorem]{Conjecture}

\theoremstyle{definition}
\newtheorem{Definition}[Theorem]{Definition}
\theoremstyle{remark}
\newtheorem{Remark}[Theorem]{Remark}

\usepackage{graphicx}
\usepackage{color}

\usepackage{nicefrac}

\usepackage{listings}

\usepackage{hyperref}

\lstset{
  texcl,
  mathescape,
  basicstyle=\small\tt,
  stringstyle=\color{darkred},
  showstringspaces=false,
  keywordstyle=\color{blue}\bfseries,
  commentstyle=\color{gray50}\it,
  numberstyle=\tiny\itshape,
  frame=single,
%  resetmargins,
%  breaklines=true,
%  breakautoindent=true,
  language=Haskell
}
\definecolor{brown}{rgb}{0.7,0.3,0.2}
\definecolor{darkgreen}{rgb}{0.0,0.2,0.0}
\definecolor{darkred}{rgb}{0.5,0.1,0.1}
\definecolor{gray40}{rgb}{0.4,0.4,0.4}
\definecolor{gray50}{rgb}{0.5,0.5,0.5}

\pagenumbering{gobble}

\date{}

\newcommand{\ie}{i.e.~}
\newcommand{\eg}{e.g.~}
\newcommand{\etal}{et al.~}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\D}{\mathbb{D}}
\renewcommand{\P}{\mathcal{P}}

% \newcommand{\Poly}{\operatorname{Poly}}
% \newcommand{\PPoly}{\operatorname{PPoly}}
% \newcommand{\Frac}{\operatorname{Frac}}
% \newcommand{\PFrac}{\operatorname{PFrac}}
% \newcommand{\Fun}{\operatorname{Fun}}
% 
% \newcommand{\Max}{\operatorname{Max}}
% \newcommand{\Int}{\operatorname{Int}}

\title{Formally veryfing exact real limit computations}
\title{Exact real limit computation, fake or fact?}
\title{Verified exact real limit computation}
\author{Michal Konečný\\
		Aston University, Birmingham, United Kingdom\\
		\texttt{m.konecny@aston.ac.uk}}

\parskip 1ex
% \parindent 0pt

\begin{document}
\maketitle

Consider a program for computing $\sqrt x$ for a real number $x>1$ using Newton iteration:

\begin{lstlisting}[numbers=left,language=Haskell, commentstyle=\color{darkred},basicstyle=\fontsize{8}{10pt}\tt]
sqrt_Newton x = lim ($\lambda\varepsilon.$ sqrt_Newton_iter $\varepsilon$ x x) 

sqrt_Newton_iter $\varepsilon$ x y$_n$
  | abs(y$_n$ - x/y$_n$) <= $\varepsilon$     = y$_n$  -- $y_n$ close to $x/y_n$, ie $y_n$ close to $\sqrt x$
  | otherwise            = let y$_{n+1}$ = (y$_n$ + x/y$_n$)/2 -- Newton step
                           in sqrt_Newton_iter $\varepsilon$ x y$_{n+1}$ -- recursive call
\end{lstlisting}

\noindent
where $y_n$ and $y_{n+1}$ are real numbers and $\varepsilon$ is always a positive rational number.
Our goal is to give programs such as \lstinline{sqrt_Newton} 
an executable semantics and formally verify their semantics, eg that 
\lstinline{sqrt_Newton}
computes the exact $\sqrt x$ for any $1 < x$.
Of particular interest is how to express and verify program termination 
in the presence of recursion and \lstinline{lim}.

Function \lstinline{sqrt_Newton} 
``computes'' the exact $\sqrt x$ on RealRAM
but this model cannot be faithfully implemented.
With an approximate arithmetic such as floating-point arithmetic, \lstinline{sqrt_Newton_iter}
will typically not terminate for very small $\varepsilon$ and verification is very difficult.

We will use exact real arithmetic \cite{irram2001,aern2018} to execute such programs exactly. 
In this example we stumble on the fact the comparison on line 4 is not computable
due to a discontinuity at $|y_n - x/y_n|=\varepsilon$.
% This problem can be solved by using parallel-if \cite{RealPCF}, ie executing both branches in 
% parallel until the branch condition is decided and assume that both branches 
% return the same number in cases where it is never decided.
% Another, more efficient solution is to 
A standard solution for this problem is to replace 
\lstinline{abs(y$_n$ - x/y$_n$) <= $\varepsilon$}
with non-deterministic approximate comparison
\lstinline{y$_n$ =$\varepsilon$= x/y$_n$} 
which returns true if the difference between the numbers is less than $\varepsilon/2$,
returns false if the difference is over $\varepsilon$ and 
returns either true of false if the difference is
between $\varepsilon/2$ and $\varepsilon$.
This comparison is deterministic on the level
of operational semantics with real numbers encodings 
but since it is not extensional, it
is non-deterministic at the level of denotational semantics with real numbers.


With this change, we can execute \lstinline{sqrt_Newton}
in exact real arithmetic.  To formally verify its execution,
we first encode a minimal programming language and its exact real operational semantics in a theorem prover.
We base our formalisation on CIDR-F1, the first-order functional fragment of
CIDR, a minimal imperative language for verifiable real number computation,
currently under development with coleagues within CID, EU RISE project 731143.

We wish to express operational semantics as a function
which we can use to extract verified executables from formalised programs.
This means that the operational semantics is a potentially non-terminating
function, which is not (well) supported in theorem provers such as Isabelle and Coq.
To make the semantics a terminating function, we add a call stack size bound parameter: 
\[ 
    \mathrm{opsem} {:{}} (\mathrm{FnName}\to \mathrm{Term}) \to \mathrm{Term} 
    \to (\mathrm{stackBound}:\N) \to \mathrm{Input} \to \mathrm{Val}^\top 
\]
and return $\top$ when a term evaluation makes too many nested calls, \eg due to an infinite recursion.
(In an imperative language we would also need to bound loops.)
We specify that a program terminates by saying that for each valid input there is a stack size limit for which
the operational semantics never returns $\top$.  

The set $\mathrm{Val}$ includes real numbers represented \eg by convergent ``sequences'' \(\Q_+ \to \Q\).
When computing \lstinline{lim ($\lambda \varepsilon$. t $\varepsilon$)},
it is often the case that the closer is $\varepsilon$ to zero, the more resources \lstinline{t $\varepsilon$}
needs.  A fixed bound on call stack size for a limit implies that the operational semantics will
sometimes return ``sequences'' \(\Q_+ \to \Q^\top\) that return $\top$ for very small $\varepsilon$.
These are partial real numbers whose denotational semantics are compact real intervals.  
Our prototype Isabelle formalisation defines an abstract type of (constructive) partial real
numbers, postulating their properties.  For each partial real operation, we postulate that the
operation is safe with regards to the corresponding real number operation and that
the partial real operation has a (non-uniform) modulus of continuity, implying that it converges for
exact inputs.  For example:
\[
    \begin{array}{rclcll}
    x\in X&\land& y \in Y \;\land\; 0\notin Y&\implies& x/y\in X/Y
    &\text{(safety)}
    \\[1ex]
    \mathrm{wd}(X)\leq\omega_{\div,L}(X,Y,\varepsilon)&\land& 
    \mathrm{wd}(Y)\leq\omega_{\div,R}(X,Y,\varepsilon)&\implies& 
    \mathrm{wd}(X/Y)\leq \varepsilon
    &\text{(modulus)}
    \end{array}
\]
% A specification using partial real numbers usually implies
% an analogous specification using $\R$.

We formalise also a non-deterministic denotational semantics of CIDR-F1:
\[ 
    \mathrm{dsem} {:{}} (\mathrm{FnName}\to \P(\mathrm{Input}\times\mathrm{Val}^\top)) \to \mathrm{Term} 
    \to (\mathrm{stackBound}:\N) \to \P(\mathrm{Input}\times\mathrm{Val}^\top) 
\]
Assuming a certain input-output relation for all functions (typically formulated as pre- and post-conditions), 
if we can show that the derived input-output relation for the body of each function implies its own specification,
we have proved that the assumed semantics is a safe approximation of the operational semantics.
Thus we can specify the behaviour of our functions in terms of pre- and post-conditions and verify it
using relatively simple inductive reasoning. The precondition contains a bound on the stack size.

As an example, we will review our formal proof of the following specification of \lstinline{sqrt_Newton}:

\begin{Theorem}
  For any real numbers $1 < x$ and $0<\varepsilon$, the term \lstinline{sqrt_Newton x}
  evaluated with a stack bound at least
  \(
    \max(1,1+\log_2(\nicefrac{2(x-1)}{\varepsilon}))    
  \)
  returns a partial real number (interval) $R$ that contains $\sqrt x$ 
  and whose width is at most $\varepsilon$.
\end{Theorem}

The theorem gives an upper bound on an essential component on the complexity of the program
and shows that the program computes $\sqrt x$ exactly if given unlimited resources. 
% This theorem can be generalised for an arbitrary upper bound on $x$.

The following sketch gives an overview of the components of our verification approach:
\begin{center}
\includegraphics[width=0.6\hsize]{figs/real-proving-elements.pdf}
\end{center}
Our prototype has been implemented in Isabelle \cite{isabellebook2014}, currently without program extraction.

% Unlike Hoare-style specification and verification, our approach facilitates termination proof without
% the need for inline specification of variants.
Our approach could be extended to the imperative part of CIDR,
% using Hoare-style inline specification of loop variants for termination verification, 
but stack bound
alone would not suffice to get a total operational semantics; 
a bound on while-loop executions would be needed too.
We plan to add an iRRAM-style iterative operational semantics to the formalisation.
The iRRAM limit operator\cite{irram2001} has more complex behaviour than the one based on sequences.

\bibliographystyle{unsrt}
\bibliography{cca2018-limits}

\end{document}

