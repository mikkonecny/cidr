theory AReal

imports Complex_Main

begin

(*  Title:      AReal.thy
    Author:     Michal Konecny, Aston University, 2018
*)

text \<open>Specification of a datatype of partially specified (potentially constructive) reals\<close>

text \<open>This is work in progress.  In 2017, this theory was developed as part of a pilot study with 
  a narrow purpose. It is rather incomplete.\<close>

section\<open>Miscellaneous\<close>

abbreviation "q2r \<equiv> real_of_rat"

subsection \<open>Sandbox\<close>

definition oneR :: real where
  "oneR = Real.Real (%n \<Rightarrow> 1)"

value [nbe] "oneR = oneR"

definition badR :: real where
  "badR = Real.Real (%n \<Rightarrow> (-1)^n)"

value [nbe] "oneR"
value [nbe] "badR"

value [nbe] "floorlog 2 (nat \<lfloor> 0::rat \<rfloor>)"

text\<open>Example lemma only for introducing Isabelle to people\<close>

lemma sqrt_increase: 
  assumes xgt1: "1 < (x::real)"
  shows "sqrt x < x"
proof-
  from xgt1 have sxgt1: "1 < sqrt x"
    by auto
  show ?thesis
    using xgt1 sxgt1
    using real_sqrt_mult_self
    using real_sqrt_mult_self [of x]
    using mult_left_le_imp_le
    using mult_left_le_imp_le [of "sqrt x" "sqrt x" 1]
    by linarith
qed

(* class_deps *)


(****************************************************************************************)
section \<open>Type class of partially specified reals\<close>

subsection \<open>Basics\<close>

type_synonym error_bound = real (* TODO: always positive *)
type_synonym error_bound_rat = rat (* TODO: always positive *)

class ar_base =
  fixes
    real_in_ar :: "real \<Rightarrow> 'a \<Rightarrow> bool" (infixl "\<in>ar" 65)
    and ar_err_le :: "'a \<Rightarrow> error_bound \<Rightarrow> bool"  (infixl "e\<le>" 65)
  assumes
    ar_err_le:
      "ar e\<le> e \<Longrightarrow> 0<e \<and> (\<exists> r. r \<in>ar ar) \<and>
          (\<forall> x y.  x \<in>ar ar \<and> y \<in>ar ar \<longrightarrow>  \<bar>x-y\<bar> \<le> e)"
    and ar_err_le_mono:
      "ar e\<le> e \<Longrightarrow> e < e' \<Longrightarrow> ar e\<le> e'"
(*
  The following is undesirable as ar_lim should be applicable also to inconsistent sequences.
    and ar_nonempty:
      "\<And> ar. (\<exists> r . r \<in>ar ar)"
*)
begin

subsubsection\<open>Exact numbers\<close>

fun ar_is_exact :: "'a \<Rightarrow> bool" where
  "ar_is_exact ar = ((\<forall>e > 0 . ar e\<le> e) \<and> (\<exists> r . r \<in>ar ar))"

lemma ar_is_exact_e: 
  "\<And> e . 0 < e \<Longrightarrow> ar_is_exact ar \<Longrightarrow> ar e\<le> e" by simp

lemma ar_exact_unique_real:
  assumes 
    ar: "ar_is_exact ar"
    and x: "x \<in>ar ar"
    and y: "y \<in>ar ar"
  shows "x = y"
  using ar 
  apply (auto)
proof -
  assume "\<forall> e . 0 < e  \<longrightarrow> ar e\<le> e"
  then have
     "\<And> e . 0 < e  \<Longrightarrow> \<bar>x-y\<bar> \<le> q2r e"
    using ar_err_le [of ar]
    using x y by fastforce

  then show "x = y" 
    using of_rat_dense [of 0 "\<bar>x-y\<bar>"]
    by force
qed

subsubsection\<open>Containment\<close>

fun ar_contains :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infixl "\<subseteq>ar" 65) where
  "ar_contains ar1 ar2 = (\<forall>r . r \<in>ar ar1 \<longrightarrow> r \<in>ar ar2)"

subsubsection\<open>Initial accuracy sequences\<close>

fun ar_eI_convergent :: "(error_bound \<Rightarrow> 'a) \<Rightarrow> bool" where
  "ar_eI_convergent eI2ar = 
      (\<forall>eI>0. (\<forall>e > 0. (\<exists>eI'>0 . 
        (\<forall>eI''. 0 < eI'' \<longrightarrow> eI'' \<le> eI' \<longrightarrow> eI2ar eI'' \<subseteq>ar eI2ar eI \<and> eI2ar eI'' e\<le> e))))"

lemma ar_eI_convergent_unique_real:
  assumes 
    eI2ar: "ar_eI_convergent eI2ar"
    and x: "\<forall>eI>0. x \<in>ar eI2ar eI"
    and y: "\<forall>eI>0. y \<in>ar eI2ar eI"
  shows "x = y"
  using eI2ar
  apply (auto)
proof -
  assume all: "\<forall>eI>0.
       \<forall>e>0.
          \<exists>eI'>0.
             \<forall>eI''>0.
                eI'' \<le> eI' \<longrightarrow>
                (\<forall>r. r \<in>ar eI2ar eI'' \<longrightarrow>
                     r \<in>ar eI2ar eI) \<and>
                eI2ar eI'' e\<le> e"
  then have "\<forall>e>0. (\<exists>eI>0. eI2ar eI e\<le> e)"
    by (meson order_refl)
  then have alle: 
     "\<And> e . 0 < e  \<Longrightarrow> \<bar>x-y\<bar> \<le> of_rat e"
    using ar_err_le x y
    by (meson zero_less_of_rat_iff)
  show "x = y" 
    using alle of_rat_dense [of 0 "\<bar>x-y\<bar>"]
    by force
qed

subsubsection\<open>Predicates over all compatible reals\<close>

fun all_r2b ::
  "(real \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> bool)"
  where
  "all_r2b pred xA = (\<forall> x. x \<in>ar xA \<longrightarrow> pred x)"

fun all_rr2b ::
  "(real \<Rightarrow> real \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'a \<Rightarrow> bool)"
  where
  "all_rr2b pred xA yA = (\<forall> x y. x \<in>ar xA \<longrightarrow> y \<in>ar yA \<longrightarrow> pred x y)"

definition ar_sureequal :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infixl "=!=" 65) where
  [simp]: "ar_sureequal = all_rr2b (op =)"


(*
lemma ar_sureequal_exact:
  "(xA =!= yA) \<Longrightarrow> ar_is_exact xA"
  using ar_nonempty [of yA]
  by auto
*)


(*
lemma ar_upto_accuracy_cont:
      "(\<forall>e'. e < e' \<longrightarrow> ar e\<le> e') \<Longrightarrow> ar e\<le> e"
*)

fun is_safe_pre_r2r ::
  "(real \<Rightarrow> bool) \<Rightarrow>
   ('a \<Rightarrow> 'a) \<Rightarrow>
   (real \<Rightarrow> real) \<Rightarrow>
   real \<Rightarrow> 'a \<Rightarrow>
   bool"
  where
  "is_safe_pre_r2r pre f fr x xA =
    (all_r2b pre xA \<longrightarrow> x \<in>ar xA \<longrightarrow> fr x \<in>ar f xA)"

definition is_safe_r2r
  where
  [simp]: "is_safe_r2r = is_safe_pre_r2r (\<lambda>_. True)"

fun is_safe_pre_rr2r :: 
  "(real \<Rightarrow> real \<Rightarrow> bool) \<Rightarrow>
   ('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow>
   (real \<Rightarrow> real \<Rightarrow> real) \<Rightarrow>
   real \<Rightarrow> 'a \<Rightarrow>
   real \<Rightarrow> 'a \<Rightarrow>
   bool"
  where
  "is_safe_pre_rr2r pre f fr x xA y yA =
    (all_rr2b pre xA yA \<longrightarrow> 
      x \<in>ar xA \<longrightarrow> y \<in>ar yA \<longrightarrow> fr x y \<in>ar f xA yA)"

definition is_safe_rr2r
  where
  [simp]: "is_safe_rr2r = is_safe_pre_rr2r (\<lambda>_ _. True)"

fun has_modulus_pre_r2r ::
  "(real \<Rightarrow> bool) \<Rightarrow>
   ('a \<Rightarrow> 'a) \<Rightarrow>
   ('a \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound) \<Rightarrow>
  'a \<Rightarrow> error_bound \<Rightarrow>
   bool"
  where
  "has_modulus_pre_r2r pre f \<omega> xA e =
    (0 < e \<longrightarrow>
     all_r2b pre xA \<longrightarrow> 
     all_r2b (\<lambda> x. 0 < \<omega> xA x e) xA
     \<and>
     (all_r2b (\<lambda> x. xA e\<le> \<omega> xA x e) xA
        \<longrightarrow> (\<exists> x. x \<in>ar xA)  
          \<longrightarrow> f xA e\<le> e))"

fun has_modulus_r2r
  where
  "has_modulus_r2r f = has_modulus_pre_r2r (\<lambda>_. True) f"

fun has_modulus_pre_rr2r :: 
  "(real \<Rightarrow> real \<Rightarrow> bool) \<Rightarrow> 
   ('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 
   ('a \<Rightarrow> real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound) \<Rightarrow> 
   ('a \<Rightarrow> real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound) \<Rightarrow> 
   'a \<Rightarrow> 'a \<Rightarrow> error_bound \<Rightarrow>
   bool" 
  where
  "has_modulus_pre_rr2r pre f \<omega>\<^sub>L \<omega>\<^sub>R xA yA e = 
    (0 < e \<longrightarrow>
     all_rr2b pre xA yA \<longrightarrow>
     (all_rr2b (\<lambda> x y. 0 < \<omega>\<^sub>L xA x y e \<and>  0 < \<omega>\<^sub>R xA x y e) xA yA
     \<and>
     (all_rr2b (\<lambda> x y. xA e\<le> \<omega>\<^sub>L xA x y e \<and> yA e\<le> \<omega>\<^sub>R xA x y e) xA yA
        \<longrightarrow> (\<exists> x. x \<in>ar xA)  
        \<longrightarrow> (\<exists> y. y \<in>ar yA)  
          \<longrightarrow> f xA yA e\<le> e)))"

definition has_modulus_rr2r
  where
  [simp]: "has_modulus_rr2r = has_modulus_pre_rr2r (\<lambda>_ _ . True)"

fun ar_is_exact_pre_r2r ::
  "(real \<Rightarrow> bool) \<Rightarrow>
   ('a \<Rightarrow> 'a) \<Rightarrow>
   'a \<Rightarrow>
   bool"
  where
  "ar_is_exact_pre_r2r pre f xA =
    (all_r2b pre xA \<longrightarrow> ar_is_exact xA \<longrightarrow> ar_is_exact (f xA))"

definition ar_is_exact_r2r
  where
  [simp]: "ar_is_exact_r2r = ar_is_exact_pre_r2r (\<lambda>_. True)"

fun ar_is_exact_pre_rr2r :: 
  "(real \<Rightarrow> real \<Rightarrow> bool) \<Rightarrow>
   ('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow>
   'a \<Rightarrow> 'a \<Rightarrow>
   bool"
  where
  "ar_is_exact_pre_rr2r pre f xA yA =
    (all_rr2b pre xA yA \<longrightarrow> ar_is_exact xA \<longrightarrow> ar_is_exact yA \<longrightarrow> ar_is_exact (f xA yA))"

definition ar_is_exact_rr2r
  where
  [simp]: "ar_is_exact_rr2r = ar_is_exact_pre_rr2r (\<lambda>_ _. True)"

lemma ar_has_modulus_implies_is_exact_pre_r2r:
  "(\<forall> e . has_modulus_pre_r2r pre f \<omega> x e) \<Longrightarrow> ar_is_exact_pre_r2r pre f x"
  apply auto
  using ar_err_le zero_less_one 
  by blast

lemma ar_has_modulus_implies_is_exact_r2r:
  "(\<forall> e . has_modulus_r2r f \<omega> x e) \<Longrightarrow> ar_is_exact_r2r f x"
  apply auto
  using ar_err_le zero_less_one 
  by blast

lemma ar_has_modulus_implies_is_exact_pre_rr2r:
  "(\<forall> e . has_modulus_pre_rr2r pre f \<omega>\<^sub>L \<omega>\<^sub>R x y e) \<Longrightarrow> ar_is_exact_pre_rr2r pre f x y"
  apply auto
  using ar_err_le zero_less_one 
  by blast

lemma ar_has_modulus_implies_is_exact_rr2r:
  "(\<forall> e . has_modulus_rr2r f \<omega>\<^sub>L \<omega>\<^sub>R x y e) \<Longrightarrow> ar_is_exact_rr2r f x y"
  apply auto
  using ar_err_le zero_less_one 
  by blast


end

subsection \<open>Embedding exact numbers\<close>

class ar_rat = ar_base +
  fixes
    ar_of_rat :: "rat \<Rightarrow> 'a"
  assumes
    ar_of_rat_exact: "ar_is_exact (ar_of_rat q)" 
    and ar_of_rat: "of_rat q \<in>ar (ar_of_rat q)"
begin

lemma ar_of_rat_self:
  "x \<in>ar ar_of_rat q \<Longrightarrow> x = of_rat q"
  using ar_exact_unique_real ar_of_rat ar_of_rat_exact 
  by blast

end

text\<open>Rational numbers with an initial accuracy\<close>

class ar_rat_eI = ar_base +
  fixes
    ar_of_rat_eI :: "error_bound \<Rightarrow> rat \<Rightarrow> 'a"
  assumes
    ar_of_rat_eI: "of_rat q \<in>ar (ar_of_rat_eI eI q)"
    and
    ar_of_rat_eI_convergent: "ar_eI_convergent (\<lambda>eI. ar_of_rat_prec eI q)"
begin

lemma ar_of_rat_eI_self:
  "(\<forall> eI>0. x \<in>ar ar_of_rat_eI eI q) \<Longrightarrow> x = of_rat q"
  using ar_eI_convergent_unique_real ar_of_rat_eI ar_of_rat_eI_convergent
  by blast

end

subsection \<open>Addition\<close>

class ar_add = ar_rat + plus +
  fixes
    ar_add_mod1 :: "'a \<Rightarrow> real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound" ("\<omega>\<^sub>+\<^sub>L") and
    ar_add_mod2 :: "'a \<Rightarrow> real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound" ("\<omega>\<^sub>+\<^sub>R")
    (* ar_add_mod1  and ar_add_mod2 give the implementation room for optimisation and rounding *)
  assumes
    ar_add_safe: "\<And> x xA y yA . is_safe_rr2r (op +) (op +) x xA y yA"
    and 
    ar_add_mod: "\<And> xA yA e . has_modulus_rr2r (op +) \<omega>\<^sub>+\<^sub>L \<omega>\<^sub>+\<^sub>R xA yA e"
begin

lemma ar_add_exact:
  "\<And> x y. ar_is_exact_rr2r (op +) x y"
  using ar_has_modulus_implies_is_exact_rr2r
  using ar_add_mod
  by blast

lemma ar_add_rat:
  "ar_of_rat q1 + ar_of_rat q2 =!= ar_of_rat (q1 + q2)"
  using ar_of_rat_self [of "_" "q1+q2"]
  using ar_of_rat_exact
  using ar_add_exact
  using ar_of_rat
  using ar_add_safe [of "of_rat q1" "ar_of_rat q1" "of_rat q2" "ar_of_rat q2"]
  using ar_exact_unique_real [of "ar_of_rat q1 + ar_of_rat q2"]
  by (auto simp add: of_rat_add)

end

class ar_sub = ar_add + minus +
  assumes
    ar_sub_safe: "\<And> x xA y yA . is_safe_rr2r (op -) (op -) x xA y yA"
    and 
    ar_sub_mod: "\<And> xA yA e . has_modulus_rr2r (op -) \<omega>\<^sub>+\<^sub>L \<omega>\<^sub>+\<^sub>R xA yA e"

subsection \<open>Multiplication\<close>

(*
class ar_norm = ar_base +
  fixes
    ar_apxnorm :: "error_bound \<Rightarrow> 'a \<Rightarrow> rat"
  assumes
    ar_apxnorm: "\<And> r ar ear e. r \<in>ar ar \<and> ar e\<le> ear \<and> 0 < e \<Longrightarrow> 
      let 
        arnR = real_of_rat (ar_apxnorm e ar); 
        eR = real_of_rat e;
        earR = real_of_rat ear
      in 
        \<bar>r\<bar> \<le> arnR \<and> arnR \<le> \<bar>r\<bar> + eR + earR"
*)

class ar_mul = ar_rat + times +
  fixes
    ar_mul_mod1 :: "'a \<Rightarrow> real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound" ("\<omega>\<^sub>*\<^sub>L") and
    ar_mul_mod2 :: "'a \<Rightarrow> real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound" ("\<omega>\<^sub>*\<^sub>R")
  assumes
    ar_mul_safe: "\<And> x xA y yA . is_safe_rr2r (op *) (op *) x xA y yA"
    and 
    ar_mul_mod: "\<And> xA yA e . has_modulus_rr2r (op *) \<omega>\<^sub>*\<^sub>L \<omega>\<^sub>*\<^sub>R xA yA e"
begin

lemma ar_mul_exact:
  "\<And> x y. ar_is_exact_rr2r (op *) x y"
  using ar_has_modulus_implies_is_exact_rr2r
  using ar_mul_mod
  by blast

lemma ar_mul_rat:
  "ar_of_rat q1 * ar_of_rat q2 =!= ar_of_rat (q1 * q2)"
  using ar_of_rat_self [of "_" "q1*q2"]
  using ar_of_rat_exact
  using ar_mul_exact
  using ar_of_rat
  using ar_mul_safe [of "of_rat q1" "ar_of_rat q1" "of_rat q2" "ar_of_rat q2"]
  using ar_exact_unique_real [of "ar_of_rat q1 * ar_of_rat q2"]
  by (auto simp add: of_rat_mult)

end

(*
    and
    ar_mul_e1: "\<And>xA yA e. 0 < e \<Longrightarrow> 0 < ar_mul_mod1 xA yA e"
    and
    ar_mul_e2: "\<And>xA yA e. 0 < e \<Longrightarrow> 0 < ar_mul_mod2 xA yA e"
    and
    ar_mul_ee: "\<And>xA yA e. 0 < e \<Longrightarrow> 
      let 
        e1 = ar_mul_mod1 xA yA e; 
        e2 = ar_mul_mod2 xA yA e;
        ar1n = ar_apxnorm e2 xA;
        ar2n = ar_apxnorm e1 yA 
      in
        e1*ar1n + e2*ar1n + e1*e2 \<le> e"
*)

subsection \<open>Division\<close>

class ar_div = ar_rat + ar_mul + inverse +
  fixes
    ar_div_mod1 :: "'a \<Rightarrow> real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound" ("\<omega>\<^sub>\<div>\<^sub>L") and
    ar_div_mod2 :: "'a \<Rightarrow> real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> error_bound" ("\<omega>\<^sub>\<div>\<^sub>R")
  assumes
    ar_div_safe: "\<And> x xA y yA .is_safe_pre_rr2r  (\<lambda> x y . y \<noteq> 0) (op /) (op /) x xA y yA"
    and 
    ar_div_mod: "\<And> xA yA e . has_modulus_pre_rr2r (\<lambda> x y . y \<noteq> 0) (op /) \<omega>\<^sub>\<div>\<^sub>L \<omega>\<^sub>\<div>\<^sub>R xA yA e"
begin

lemma ar_div_exact:
  "\<And> x y. ar_is_exact_pre_rr2r (\<lambda> x y . y \<noteq> 0) (op /) x y"
  using ar_div_mod
  by (meson ar_has_modulus_implies_is_exact_pre_rr2r)

lemma ar_div_rat:
  "q2 \<noteq> 0 \<Longrightarrow> ar_of_rat q1 / ar_of_rat q2 =!= ar_of_rat (q1 / q2)"
  using ar_of_rat_self [of "_" "q1/q2"]
  using ar_of_rat_self [of "_" "q2"]
  using ar_of_rat_exact [of q1]
  using ar_of_rat_exact [of q2]
  using ar_div_exact [of "ar_of_rat q1" "ar_of_rat q2"]
  using ar_of_rat
  using ar_div_safe [of "of_rat q1" "ar_of_rat q1" "of_rat q2" "ar_of_rat q2"]
  using ar_exact_unique_real [of "ar_of_rat q1 / ar_of_rat q2"]
  using of_rat_eq_0_iff [of q2]
  using of_rat_divide [of q1 q2]
  using all_rr2b.elims(3)
  using ar_is_exact_pre_rr2r.elims(2)
  using is_safe_pre_rr2r.simps
  by (smt ar_sureequal_def)

end

(* Can the above be improved using: (a+ea)*b-a*(b+eb) = ea*b-eb*a ? *)

subsection \<open>Approximate comparison\<close>

class ar_approx_eq = ar_add +
  fixes
    (* v = ar_approx_eq e xA yA works something like:

        if \<bar>xA-yA\<bar> > e/2 then 
          v = Some False
        else if \<bar>xA-yA\<bar> \<le> e then
          v = Some True 
        else (* e,e/2 \<in> \<bar>xA-yA\<bar> *)
          v = None
    *)
    ar_approx_eq :: "rat \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool option"
  assumes
    ar_approx_eq_not_false:
       "e > 0 \<Longrightarrow>
        x \<in>ar xA \<Longrightarrow>
        y \<in>ar yA \<Longrightarrow> 
        \<bar>x-y\<bar> \<le> q2r e/2 \<Longrightarrow> 
         (ar_approx_eq e xA yA) \<noteq> Some False"
  and
    ar_approx_eq_not_true:
       "e > 0 \<Longrightarrow>
        x \<in>ar xA \<Longrightarrow>
        y \<in>ar yA \<Longrightarrow> 
        \<bar>x-y\<bar> > q2r e \<Longrightarrow> 
        (ar_approx_eq e xA yA) \<noteq> Some True"
  and
    ar_approx_eq_defined:
       "e > 0 \<Longrightarrow>
        xA e\<le> q2r e/4 \<Longrightarrow>
        yA e\<le> q2r e/4 \<Longrightarrow> 
        \<not> (Option.is_none (ar_approx_eq e xA yA))"
begin

lemma ar_approx_eq_false:
 "e > 0 \<Longrightarrow>
  xA e\<le> q2r e/4 \<Longrightarrow> x \<in>ar xA \<Longrightarrow>
  yA e\<le> q2r e/4 \<Longrightarrow> y \<in>ar yA \<Longrightarrow> 
  \<bar>x-y\<bar> > q2r e \<Longrightarrow> 
  (ar_approx_eq e xA yA) = Some False"
  using ar_approx_eq_defined [of e xA yA]
  using ar_approx_eq_not_true [of e x xA y yA]
  by (metis (full_types) is_none_code(1) option.exhaust)

lemma ar_approx_eq_true:
 "e > 0 \<Longrightarrow>
  xA e\<le> q2r e/4 \<Longrightarrow> x \<in>ar xA \<Longrightarrow>
  yA e\<le> q2r e/4 \<Longrightarrow> y \<in>ar yA \<Longrightarrow> 
  \<bar>x - y\<bar> \<le> q2r e/2 \<Longrightarrow> 
   (ar_approx_eq e xA yA) = Some True"
  using ar_approx_eq_defined [of e xA yA]
  using ar_approx_eq_not_false [of e x xA y yA]
  by (metis (full_types) is_none_code(1) option.exhaust)

end

class ar_approx_leq = ar_add +
  fixes
    (* v = ar_approx_leq e xA yA works something like:

        if xA-yA \<ge> -e then 
          v = Some False
        else if xA-yA \<le> e then
          v = Some True 
        else (* -e,e \<in> xA-yA *)
          v = None
    *)
    ar_approx_leq :: "rat \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool option"
  assumes
    ar_approx_leq_not_false:
       "e > 0 \<Longrightarrow>
        x \<in>ar xA \<Longrightarrow>
        y \<in>ar yA \<Longrightarrow> 
        x-y < of_rat (- e) \<Longrightarrow> 
         (ar_approx_leq e xA yA) \<noteq> Some False"
  and
    ar_approx_leq_not_true:
       "e > 0 \<Longrightarrow>
        x \<in>ar xA \<Longrightarrow>
        y \<in>ar yA \<Longrightarrow> 
        x-y > of_rat e \<Longrightarrow> 
        (ar_approx_leq e xA yA) \<noteq> Some True"
  and
    ar_approx_leq_defined:
       "e > 0 \<Longrightarrow>
        xA e\<le> q2r e \<Longrightarrow>
        yA e\<le> q2r e \<Longrightarrow> 
        \<not> (Option.is_none (ar_approx_leq e xA yA))"
begin

lemma ar_approx_leq_false:
 "e > 0 \<Longrightarrow>
  xA e\<le> q2r e \<Longrightarrow> x \<in>ar xA \<Longrightarrow>
  yA e\<le> q2r e \<Longrightarrow> y \<in>ar yA \<Longrightarrow> 
  x-y > q2r e \<Longrightarrow> 
  (ar_approx_leq e xA yA) = Some False"
  using ar_approx_leq_defined [of e xA yA]
  using ar_approx_leq_not_true [of e x xA y yA]
  by (metis (full_types) is_none_code(1) option.exhaust)

lemma ar_approx_leq_true:
 "e > 0 \<Longrightarrow>
  xA e\<le> q2r e \<Longrightarrow> x \<in>ar xA \<Longrightarrow>
  yA e\<le> q2r e \<Longrightarrow> y \<in>ar yA \<Longrightarrow> 
  x - y < of_rat (- e) \<Longrightarrow> 
   (ar_approx_leq e xA yA) = Some True"
  using ar_approx_leq_defined [of e xA yA]
  using ar_approx_leq_not_false [of e x xA y yA]
  by (metis (full_types) is_none_code(1) option.exhaust)

end


subsection \<open>Limits\<close>

subsubsection\<open>Exact limit\<close>

definition seq_consistent :: "real \<Rightarrow> (error_bound_rat \<Rightarrow> ('a::ar_base) option) \<Rightarrow> bool" where
  [simp]: "seq_consistent x f = 
    (\<forall>e. 0 < e \<longrightarrow> (case f e of None \<Rightarrow> True | Some fe \<Rightarrow> (\<exists>y. y \<in>ar fe \<and> \<bar>x-y\<bar> \<le> q2r e)))"

class ar_lim = ar_base +
  fixes
    ar_lim :: "(error_bound_rat \<Rightarrow> 'a option) \<Rightarrow> ('a :: ar_base)"
    and
    ar_lim_ix :: "'a \<Rightarrow> error_bound_rat \<Rightarrow> error_bound_rat"
  assumes
    ar_lim_safe:
      "\<And>f x. 
        seq_consistent x f
        \<Longrightarrow>
        x \<in>ar (ar_lim f)"
  and
    ar_lim_err:
      "\<And> f e.
        (\<exists> x . seq_consistent x f) \<Longrightarrow>
        0 < e \<Longrightarrow>
        let
          e_ix = ar_lim_ix (ar_lim f) e
        in
        (\<exists> xeA . f e_ix = Some xeA \<and> xeA e\<le> q2r (e - e_ix)) \<Longrightarrow>
        ar_lim f e\<le> q2r e"
  and
    ar_lim_ix:
      "\<And> e xA . 0 < e \<Longrightarrow> 0 < ar_lim_ix xA e \<and> ar_lim_ix xA e < e"
  and
    ar_lim_ix_uniform:
      "\<And> e xA yA . 0 < e \<Longrightarrow> ar_lim_ix xA e = ar_lim_ix yA e"

(*
subsubsection\<open>Limit parameterized by initial accuracy\<close>

definition seq_consistent_eI :: "real \<Rightarrow> (error_bound \<Rightarrow> error_bound \<Rightarrow> ('a::ar_base) option) \<Rightarrow> bool" where
  [simp]: "seq_consistent_eI x f = 
    (\<forall>e eI. 0 < e \<longrightarrow> 0 < eI \<longrightarrow> (case f eI e of None \<Rightarrow> True | Some fe \<Rightarrow> (\<exists>y. y \<in>ar fe \<and> \<bar>x-y\<bar> \<le> of_rat e)))"


class ar_lim_eI = ar_base +
  fixes
    ar_lim_eI :: "(error_bound \<Rightarrow> error_bound \<Rightarrow> 'a option) \<Rightarrow> ('a :: ar_base)"
  assumes
    ar_lim_eI_safe:
      "\<And>f x. 
        seq_consistent_eI x f
        \<Longrightarrow>
        x \<in>ar (ar_lim_eI f)"
(* TODO *)
*)

subsection\<open>Putting it all together\<close>

class areal_noexact_c = ar_sub + ar_div + ar_approx_eq
class areal_c = areal_noexact_c + ar_rat + ar_lim

text\<open>Class @{class areal_noexact_c} fits both exact real numbers and finite intervals. 
    Class @{class areal_c} fits only exact real numbers\<close>

section\<open>Fixed precision arithmetic\<close>

class ar_interval_c = areal_noexact_c + ar_rat_eI
begin

(*
lemma ar_add_eI_converges:
  "ar_eI_convergent eI2x \<Longrightarrow> ar_eI_convergent eI2y \<Longrightarrow> ar_eI_convergent (\<lambda>eI . eI2x eI + eI2y eI)"
  apply auto
proof-
  fix e :: error_bound
  assume ePos: "0<e"
  assume p2x: "\<forall>e>0. \<exists>p. \<forall>p'\<ge>p. p2x p' e\<le> e"
  assume p2y: "\<forall>e>0. \<exists>p. \<forall>p'\<ge>p. p2y p' e\<le> e"

  (* from p2x obtain px where "\<forall>p'\<ge>px. p2x p' e\<le> e" *)

  show " \<exists>p. \<forall>p'\<ge>p. p2x p' + p2y p' e\<le> e"
    using ar_add_mod
    apply auto
    sorry
qed
*)

end

(****************************************************************************************)
section \<open>Some instances for Isabelle reals\<close>

instantiation real :: ar_base
begin

definition
  real_in_ar_real_def[simp]: 
    "x \<in>ar xA = (x = (xA :: real))"
definition
  ar_err_le_real_def[simp]:
    "(x::real) e\<le> e = (e > 0)"

instance by intro_classes simp_all
end

instantiation real :: ar_rat
begin
definition ar_of_rat_real_def[simp]:
  "ar_of_rat q = real_of_rat q"

instance by intro_classes simp_all
end

instantiation real :: ar_add
begin
definition ar_add_mod1_real_def[simp]:
  "ar_add_mod1 (r::real) _ _ e = e/2"
definition ar_add_mod2_real_def[simp]:
  "ar_add_mod2 (r::real) _ _ e = e/2"

instance by intro_classes (auto simp add: of_rat_add)
end

instantiation real :: ar_approx_eq
begin
definition
  ar_approx_eq_real_def[simp]:
    "ar_approx_eq e (x::real) (y::real) = Some (\<bar>x-y\<bar> \<le> of_rat e)"
instance proof
  fix e :: rat
  assume e0: "0 < e"

  fix xA x yA y :: real

  show "\<not> Option.is_none (ar_approx_eq e x y)" by simp
  
  assume x: "x \<in>ar xA"
  assume y: "y \<in>ar yA"

  from e0 have "e/2 < e" by linarith
  then have e2: "real_of_rat e/2 < real_of_rat e"
    by simp

  then show "\<bar>x-y\<bar> \<le> real_of_rat e / 2 \<Longrightarrow> ar_approx_eq e xA yA \<noteq> Some False"
    using x y e0 e2
    by (smt ar_approx_eq_real_def option.inject real_in_ar_real_def)

  then show "real_of_rat e < \<bar>x-y\<bar> \<Longrightarrow> ar_approx_eq e xA yA \<noteq> Some True"
    using x y by auto
qed
end

end