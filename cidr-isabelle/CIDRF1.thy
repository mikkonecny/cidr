theory CIDRF1

(*  Title:      CIDRF1.thy
    Author:     Michal Konecny, Aston University, 2018
*)

imports Main Nat Rat Real Complex_Main Wellfounded AReal

begin

(****************************************************************************************)

section \<open>Syntax\<close>

type_synonym var_name = string
type_synonym fn_name = string

type_synonym 'a per_variable = "(var_name \<Rightarrow> 'a option)"

fun on_variable :: "'a per_variable \<Rightarrow> var_name \<Rightarrow> 'a option" where
  "on_variable f var = f var"

(*
fun zip_rel_all_vars :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow>  'a per_variable \<Rightarrow> 'b per_variable \<Rightarrow> bool"
  where
  "zip_rel_all_vars rel [] [] = True" |
  "zip_rel_all_vars rel ((va, a) # arest) ((vb, b) # brest) = 
      (va = vb \<and> rel a b \<and> zip_rel_all_vars rel arest brest)" |
  "zip_rel_all_vars rel _ _ = False"
*)

datatype term_t =
  Nat_lit nat |
  Rat_lit rat |
  Var var_name |
(*  Bits_accuracy term_t | (* Bits_accuracy n = 2^(-n) *) *)
  Plus term_t term_t |
  Div term_t term_t | (* TODO: add other common operations *)
  MV_approx_eq term_t term_t term_t | (* MV_approx_ex e x y *)
  If_then_else term_t term_t term_t |
  Lim var_name term_t | (* Lim e t_e   e > 0, e \<longrightarrow> 0 *)
  Fn_call fn_name "(var_name \<times> term_t) list"

type_synonym 'a per_fn = "(fn_name \<times> 'a) list"
  
fun on_fn :: "'a per_fn \<Rightarrow> fn_name \<Rightarrow> 'a option" where
  "on_fn [] _ = None" |
  "on_fn  ((x, a) # rest) x' = 
    (if x = x' then Some a else (on_fn rest x'))"

datatype type_t =
  Typ_bool | Typ_nat | Typ_rat | Typ_real

type_synonym param_types_t = 
  "(var_name \<times> type_t) list"

type_synonym program_t = "(term_t \<times> param_types_t) per_fn"
  
definition prg_empty :: program_t where
  "prg_empty = []"


(****************************************************************************************)

section \<open>Values\<close>

datatype 'ar value_t =
  Val_bool bool |
  Val_nat nat |
  Val_rat rat |
  Val_areal 'ar

fun map_value :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a value_t \<Rightarrow> 'b value_t" where
  "map_value f (Val_areal a) = Val_areal (f a)" |
  "map_value f (Val_nat n) = Val_nat n" |
  "map_value f (Val_rat q) = Val_rat q" |
  "map_value f (Val_bool b) = Val_bool b"

fun check_type :: "type_t \<Rightarrow> ('ar value_t) option \<Rightarrow> ('ar value_t) option" where
  "check_type Typ_bool (Some (Val_bool b)) = Some (Val_bool b)" |
  "check_type Typ_nat (Some (Val_nat n)) = Some (Val_nat n)" |
  "check_type Typ_rat (Some (Val_rat q)) = Some (Val_rat q)" |
  "check_type Typ_real (Some (Val_areal ar)) = Some (Val_areal ar)" |
  "check_type Typ_bool _ = None" |
  "check_type Typ_nat _ = None" |
  "check_type Typ_rat _ = None" |
  "check_type Typ_real _ = None"

fun val_has_type :: "'ar value_t \<Rightarrow> type_t \<Rightarrow> bool" where
  "val_has_type  (Val_bool _) Typ_bool = True" |
  "val_has_type  (Val_nat _) Typ_nat = True" |
  "val_has_type  (Val_rat _) Typ_rat = True" |
  "val_has_type  (Val_areal _) Typ_real = True" |
  "val_has_type _ Typ_bool = False" |
  "val_has_type _ Typ_nat = False" |
  "val_has_type _ Typ_rat = False" |
  "val_has_type _ Typ_real = False"

fun voo_has_type :: "('ar value_t) option option \<Rightarrow> type_t \<Rightarrow> bool" where
  "voo_has_type (Some (Some v)) typ = val_has_type v typ" |
  "voo_has_type _ _ = False"

lemma voo_has_type_real: "voo_has_type voo Typ_real = (\<exists>ar. voo = Some (Some (Val_areal ar)))"
  by (metis type_t.distinct(11) type_t.distinct(5) type_t.distinct(9) val_has_type.elims(2) val_has_type.simps(4) voo_has_type.elims(2) voo_has_type.simps(1))

lemma voo_has_type_rat: "voo_has_type voo Typ_rat = (\<exists>q. voo = Some (Some (Val_rat q)))"
  by (metis type_t.distinct(11) type_t.distinct(3) type_t.distinct(7) val_has_type.elims(2) val_has_type.simps(3) voo_has_type.elims(2) voo_has_type.simps(1))

fun vo_to_aro :: "('ar value_t) option \<Rightarrow> 'ar option"
  where
  "vo_to_aro vo = (case vo of (Some (Val_areal a)) \<Rightarrow> Some a | _ \<Rightarrow> None)"

lemma vo_to_aro [simp]: "(vo_to_aro vo = Some ar) = (vo = Some (Val_areal ar))"
  by (auto split:option.splits value_t.splits)

lemma vo_to_aro_case [simp]: 
  "(case vo_to_aro vo of None \<Rightarrow> P | Some ar \<Rightarrow> Q ar) =
   (case vo of Some (Val_areal ar) \<Rightarrow> Q ar | _ \<Rightarrow> P)"
  by (auto split: option.splits value_t.splits)

(*
definition valuation_of_real :: "real valuation_t \<Rightarrow> ('ar :: ar_real) valuation_t" where
  [simp]:
  "valuation_of_real =
    list.map (\<lambda> (var,optval) . (var, option.map_option (map_value ar_of_real) optval))"
*)

(*
fun realval_in_val :: "real value_t \<Rightarrow> ('ar :: ar_base) value_t \<Rightarrow> bool" (infix "\<in>arval" 65) where
  "realval_in_val (Val_areal r) (Val_areal ar) = (r \<in>ar ar)" |
  "realval_in_val (Val_nat n1) (Val_nat n2) = (n1 = n2)" |
  "realval_in_val (Val_rat q1) (Val_rat q2) = (q1 = q2)" |
  "realval_in_val (Val_bool b1) (Val_bool b2) = (b1 = b2)" |
  "realval_in_val _ _ = False"
*)

type_synonym 'ar valuation_t = 
  "(('ar value_t) option) per_variable"
(* The option above is a hack which makes it easier to define semantics of function call. *)

fun vn_has_types :: "param_types_t \<Rightarrow> ('ar valuation_t) \<Rightarrow> bool" 
  where
  "vn_has_types [] vnf = True" |
  "vn_has_types ((prm_name, prm_type) # param_types) vnf = 
      (voo_has_type (vnf prm_name) prm_type
       \<and> vn_has_types param_types vnf)"

definition vn_empty :: "'ar valuation_t" where
  [simp]: "vn_empty \<equiv> \<lambda>_.None"

fun vn_of_list :: "(var_name \<times> ('ar value_t) option) list \<Rightarrow> 'ar valuation_t" where
  "vn_of_list [] = vn_empty" |
  "vn_of_list ((var, vo)#rest) = vn_of_list rest (var \<mapsto> vo)"
  
(*
fun realvn_in_arvn :: "real valuation_t \<Rightarrow> ('ar :: ar_base) valuation_t \<Rightarrow> bool" (infix "\<in>arvn" 65)
  where
  "realvn_in_arvn vn1 vn2 = 
    (\<forall> var.   
      case vn2 var of 
        Some (Some arv) \<Rightarrow> (\<exists> rv. rv \<in>arval arv \<and> vn1 var = Some (Some rv)) |
        None \<Rightarrow> vn1 var = None |
        Some None \<Rightarrow> vn1 var = Some None
    )"

lemma realvn_in_arvn_on_var: 
  "rvn \<in>arvn arvn \<Longrightarrow> 
    on_variable arvn var = Some arvo \<Longrightarrow> 
    (\<exists> rvo. on_variable rvn var = Some rvo)"
  by (smt on_variable.elims option.case_eq_if option.simps(5) realvn_in_arvn.simps)

fun on_var_ar :: "'ar valuation_t \<Rightarrow> var_name \<Rightarrow> 'ar option" where
  "on_var_ar vn var = 
    (case on_variable vn var of 
      Some (Some (Val_areal r)) \<Rightarrow> Some r | 
      _ \<Rightarrow> None)"

lemma on_var_ar:
  "on_var_ar vn var = Some x \<Longrightarrow> on_variable vn var = Some (Some(Val_areal x))"
  by (auto split: option.splits value_t.splits)
*)

(****************************************************************************************)

section \<open>Operational semantics\<close>

type_synonym stack_limit_t = nat

type_synonym 'ar paramvals_t = "'ar valuation_t"

type_synonym 'ar opsem_term_t = "'ar valuation_t \<Rightarrow> stack_limit_t \<Rightarrow> ('ar value_t) option"
type_synonym 'ar opsem_params_t = "'ar valuation_t \<Rightarrow> stack_limit_t \<Rightarrow> 'ar paramvals_t"

fun combine_vals :: 
  "(nat \<Rightarrow> nat \<Rightarrow> 'ar value_t option) \<Rightarrow> 
   (rat \<Rightarrow> rat \<Rightarrow> rat option) \<Rightarrow>
   (('ar::ar_rat) \<Rightarrow> 'ar \<Rightarrow> 'ar option) \<Rightarrow>
   (('ar value_t) option \<Rightarrow> ('ar value_t) option \<Rightarrow> ('ar value_t) option)"
  where
  "combine_vals fnat frat far v1 v2 =
    (case (v1,v2) of 
      (Some (Val_nat n1), Some (Val_nat n2)) \<Rightarrow> fnat n1 n2 |
      (Some (Val_nat n1), Some (Val_rat q2)) \<Rightarrow> map_option Val_rat (frat (of_nat n1) q2) |
      (Some (Val_nat n1), Some (Val_areal ar2)) \<Rightarrow> map_option Val_areal (far (ar_of_rat (of_nat n1)) ar2) |
      (Some (Val_rat q1), Some (Val_nat n2)) \<Rightarrow> map_option Val_rat (frat q1 (of_nat n2)) |
      (Some (Val_rat q1), Some (Val_rat q2)) \<Rightarrow> map_option Val_rat (frat q1 q2) |
      (Some (Val_rat q1), Some (Val_areal ar2)) \<Rightarrow> map_option Val_areal (far (ar_of_rat q1) ar2) |
      (Some (Val_areal ar1), Some (Val_nat n2)) \<Rightarrow> map_option Val_areal (far ar1 (ar_of_rat (of_nat n2))) |
      (Some (Val_areal ar1), Some (Val_rat q2)) \<Rightarrow> map_option Val_areal (far ar1 (ar_of_rat q2)) |
      (Some (Val_areal ar1), Some (Val_areal ar2)) \<Rightarrow> map_option Val_areal (far ar1 ar2) |
      _ \<Rightarrow> None)"

fun addSome2 :: "('a \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> ('a \<Rightarrow> 'b \<Rightarrow> 'c option)" where
  "addSome2 f a b = Some (f a b)"

fun divTestZ :: "(('a :: inverse) \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a option" where
  "divTestZ testZ a b = (if testZ b then None else Some (a/b))"

function opsem_term :: "program_t \<Rightarrow> term_t \<Rightarrow> ('ar :: areal_c) opsem_term_t"
  and
  opsem_params :: "program_t \<Rightarrow> (var_name \<times> term_t) list \<Rightarrow> 'ar opsem_params_t"
  where
  "opsem_term p (Nat_lit n) vn sb = Some (Val_nat n)" |
  "opsem_term p (Rat_lit q) vn sb = Some (Val_rat q)" |
  "opsem_term p (Var varN) vn sb = (case on_variable vn varN of Some v \<Rightarrow> v | _ \<Rightarrow> None)" |
(*
  "opsem_term p (Bits_accuracy t) vn sb =
    (case (opsem_term p t vn sb) of
      (Some (Val_nat n)) \<Rightarrow> Some (Val_rat (1/(2^n))) |
      _ \<Rightarrow> None)" |
*)
  "opsem_term p (Plus t1 t2) vn sb =
    (combine_vals (\<lambda> n1 n2 . Some (Val_nat (n1 + n2))) (addSome2 (op +)) (addSome2 (op +))
      (opsem_term p t1 vn sb) (opsem_term p t2 vn sb))" |
  "opsem_term p (Div t1 t2) vn sb =     
    (combine_vals 
      (\<lambda> n1 n2 . map_option Val_rat (divTestZ (op = 0) (of_nat n1) (of_nat n2)))
      (divTestZ (op = 0)) 
      (divTestZ (op \<in>ar 0))
      (opsem_term p t1 vn sb) (opsem_term p t2 vn sb))" |
  "opsem_term p (MV_approx_eq te t1 t2) vn sb = 
      (case (opsem_term p te vn sb, opsem_term p t1 vn sb, opsem_term p t2 vn sb) of
        (Some (Val_rat e), Some (Val_areal ar1), Some (Val_areal ar2))
          \<Rightarrow> (case ar_approx_eq e ar1 ar2 of Some bb \<Rightarrow> Some (Val_bool bb) | _ \<Rightarrow> None) |
        _ \<Rightarrow> None)" |
  "opsem_term p (If_then_else tc t1 t2) vn sb = 
      (case opsem_term p tc vn sb of
        Some (Val_bool True) \<Rightarrow> opsem_term p t1 vn sb |
        Some (Val_bool False) \<Rightarrow> opsem_term p t2 vn sb |
        _ \<Rightarrow> None)" |
  "opsem_term p (Lim var_name t) vn sb = 
    (Some (Val_areal (ar_lim (\<lambda>e. 
          (let vne = vn ( var_name \<mapsto> Some (Val_rat e) ) in
          case opsem_term p t vne sb of 
            Some (Val_areal ar) \<Rightarrow> Some ar |
            _ \<Rightarrow> None)))))" |
  "opsem_term p (Fn_call fn_name param_terms) vn 0 = None" |
  "opsem_term p (Fn_call fn_name param_terms) vn (Suc sb) =
      (case (on_fn p fn_name) of 
          Some (fn_term, param_types) \<Rightarrow> 
              (let vnf = opsem_params p param_terms vn (Suc sb) in
                if vn_has_types param_types vnf 
                  then opsem_term p fn_term vnf sb 
                  else None)
         | _           \<Rightarrow> None)" |
  "opsem_params p [] vn sb = (\<lambda>_.None)" |
  "opsem_params p ((prm_name,t) # ts) vn sb =
    (let prm_val = opsem_term p t vn sb
      in
    (opsem_params p ts vn sb) ( prm_name \<mapsto> prm_val))" 
by pat_completeness auto
termination
  apply (relation
    "((\<lambda>x. case x of 
      Inl (p, t, vn, sb) \<Rightarrow> sb | 
      Inr (p, ts, vn, sb) \<Rightarrow> sb))
    <*mlex*> 
    (measure (\<lambda>x. case x of 
      Inl (p, t, vn, sb) \<Rightarrow> size t | 
      Inr (p, ts, vn, sb) \<Rightarrow> size (size_list (\<lambda> (_,t) . size t) ts)))")
  apply (simp add: wf_mlex)
  apply (simp_all add: mlex_prod_def)
  proof -
    fix param_terms :: "(char list \<times> term_t) list"
    show "size_list (\<lambda>(_, y). size y) param_terms < Suc (size_list (size_prod (\<lambda>x. 0) size) param_terms)" 
      by (induct param_terms) auto
  qed

subsection \<open>Examples\<close>

definition ex_clterm :: term_t where
  [simp]: "ex_clterm \<equiv> Plus (Nat_lit 1) (Rat_lit (1/3))"

lemma ex_clterm_opsem: 
  "opsem_term prg_empty ex_clterm vn_empty 10 = Some (Val_rat (4/3))"
  by simp

definition ex_cltermBug :: term_t where
  [simp]: "ex_cltermBug \<equiv> If_then_else (Nat_lit 1) (Nat_lit 1) (Nat_lit 1)"

lemma ex_cltermBug_opsem: 
  "opsem_term prg_empty ex_cltermBug vn_empty 10 = None"
  by simp

subsubsection \<open>Semantics of a term with a specific valuation_t\<close>

definition ex_openterm :: term_t where
  [simp]: "ex_openterm = Plus (Var ''x'') (Rat_lit (1/3))"

fun vn_xq :: "rat \<Rightarrow> ('ar :: areal_c) valuation_t" where
  "vn_xq q = [ ''x'' \<mapsto> Some (Val_areal (ar_of_rat q))]"

lemma ex_openterm_x1_opsem: 
  "\<exists>ar . opsem_term prg_empty ex_openterm (vn_xq 1) 10 = Some (Val_areal ar) \<and> ar =!= ar_of_rat (4/3)"
  using ar_add_rat [of 1 "1/3"]
  by simp

subsubsection \<open>Semantics of a term with a non-specific valuation_t\<close>

lemma ex_openterm_xq_opsem:
  "\<exists>ar . opsem_term prg_empty ex_openterm (vn_xq q) 10 = Some (Val_areal ar) \<and> ar =!= ar_of_rat (q+1/3)"
  using ar_add_rat
  by simp

subsubsection \<open>Semantics of a term with function call\<close>

definition ex_fncallterm :: term_t where
  [simp]: "ex_fncallterm = Fn_call ''f1'' [(''x'', Var ''x'')]"

definition prg_f1 :: program_t where
  [simp]: "prg_f1 = [(''f1'', (Plus (Rat_lit (1/3)) (Var ''x''), [ (''x'', Typ_real) ]))]"
    
lemma ex_fncallterm_x1_opsem: 
  "\<exists> ar . opsem_term prg_f1 ex_fncallterm (vn_xq q) 1 = Some (Val_areal ar) 
      \<and> ar =!= ar_of_rat (q + 1/3)"
  using ar_add_rat [of "1/3" q]
  by  (auto simp add: add.commute)

(****************************************************************************************)
    
section \<open>Denotational semantics\<close>

type_synonym 'ar dsem_term_t = "'ar valuation_t \<Rightarrow> stack_limit_t \<Rightarrow> ('ar value_t) option \<Rightarrow> bool"
type_synonym 'ar dsem_params_t = "'ar valuation_t \<Rightarrow> stack_limit_t \<Rightarrow> (var_name \<times> ('ar value_t) option) list => bool"

(*
type_synonym 'ar dsem_term_precond_t = "'ar valuation_t \<Rightarrow> stack_limit_t \<Rightarrow> bool"
type_synonym 'ar dsem_term_postcond_t = "'ar valuation_t \<Rightarrow> stack_limit_t \<Rightarrow> ('ar value_t) option \<Rightarrow> bool"

fun dsem_pre_post :: "'ar dsem_term_precond_t \<Rightarrow> 'ar dsem_term_postcond_t \<Rightarrow> 'ar dsem_term_t"
  where
  "dsem_pre_post pre post vn sb v = (pre vn sb \<longrightarrow> post vn sb v)"
*)

type_synonym 'ar dsem_prg_t = "('ar dsem_term_t) per_fn"

definition dsem_prg_empty :: "'ar dsem_prg_t" where
  [simp]: "dsem_prg_empty = []"

function dsem_term :: "('ar :: areal_c) dsem_prg_t \<Rightarrow> term_t \<Rightarrow> 'ar dsem_term_t"
  and
  dsem_params :: "'ar dsem_prg_t \<Rightarrow> (var_name \<times> term_t) list \<Rightarrow> 'ar dsem_params_t"
  where
  "dsem_term p (Nat_lit n) vn sb v = (v = Some (Val_nat n))" |
  "dsem_term p (Rat_lit q) vn sb v = (v = Some (Val_rat q))" |
  "dsem_term p (Var varN) vn sb v = 
    (case on_variable vn varN of Some vv \<Rightarrow> v = vv | _ \<Rightarrow> v = None)" |
(*
  "dsem_term p (Bits_accuracy t1) vn sb v =
    (\<exists> v1 . (dsem_term p t1 vn sb v1) \<and>
    (case (v1) of
      (Some (Val_nat n1)) \<Rightarrow> v = Some (Val_rat (1/(2^n1))) |
      _ \<Rightarrow> v = None))" |
*)
  "dsem_term p (Plus t1 t2) vn sb v =
    (\<exists> v1 v2. (dsem_term p t1 vn sb v1 \<and> dsem_term p t2 vn sb v2) \<and>
      v = combine_vals (\<lambda> n1 n2 . Some (Val_nat (n1 + n2)))
            (addSome2 (op +)) (addSome2 (op +)) v1 v2)" |
  "dsem_term p (Div t1 t2) vn sb v =     
    (\<exists> v1 v2. (dsem_term p t1 vn sb v1 \<and> dsem_term p t2 vn sb v2) \<and>
      v = combine_vals 
            (\<lambda> n1 n2 . map_option Val_rat (divTestZ (op = 0) (of_nat n1) (of_nat n2)))
            (divTestZ (op = 0)) 
            (divTestZ (op \<in>ar 0))
            v1 v2)" |
  "dsem_term p (MV_approx_eq te t1 t2) vn sb v = 
    (\<exists> ve v1 v2. 
      (dsem_term p te vn sb ve \<and> dsem_term p t1 vn sb v1 \<and> dsem_term p t2 vn sb v2) \<and>
      (case (ve, v1, v2) of
        (Some (Val_rat e), Some (Val_areal ar1), Some (Val_areal ar2))
          \<Rightarrow> (case ar_approx_eq e ar1 ar2 of Some bb \<Rightarrow> v = Some (Val_bool bb) | _ \<Rightarrow> v = None) |
        _ \<Rightarrow> v = None))" |
  "dsem_term p (If_then_else tc t1 t2) vn sb v = 
    (\<exists> vc.
      (dsem_term p tc vn sb vc) \<and>
      (case vc of
        (Some (Val_bool True))  \<Rightarrow> dsem_term p t1 vn sb v |
        (Some (Val_bool False)) \<Rightarrow> dsem_term p t2 vn sb v |
        _                       \<Rightarrow> v = None))" |
  "dsem_term p (Lim var_name t) vn sb v =
    (\<exists> f. v = Some (Val_areal (ar_lim (\<lambda> e. vo_to_aro (f e))))
          \<and> (\<forall> e > 0. 
              let vne = vn (var_name \<mapsto> Some (Val_rat e)) in
              dsem_term p t vne sb (f e))
    )" |
  "dsem_term p (Fn_call fn_name param_terms) vn 0 v = (v = None) " |
  "dsem_term p (Fn_call fn_name param_terms) vn (Suc sb) v = 
    (case (on_fn p fn_name) of
          None \<Rightarrow> v = None
        | Some fn_dsem \<Rightarrow> 
            (\<exists> param_vals. 
              dsem_params p param_terms vn (Suc sb) param_vals
              \<and> fn_dsem (vn_of_list param_vals) sb v)
    )" |
  "dsem_params p [] vn sb vs = (vs=[])" |
  "dsem_params p ((prm_name, t) # ts) vn sb vvs =
    (\<exists> vs v. vvs = (prm_name,v) # vs \<and> dsem_params p ts vn sb vs \<and>
      dsem_term p t vn sb v)" 
by pat_completeness auto
termination
  apply (relation "measure (\<lambda>x. case x of 
    Inl (p, t, vn, sb, v) \<Rightarrow> size t | 
    Inr (p, ts, vn, sb, vs) \<Rightarrow> size_list (\<lambda> (_,t) . size t) ts)")
  apply auto
  proof -
    fix param_terms :: "(char list \<times> term_t) list"
    show "size_list (\<lambda>(_, y). size y) param_terms < Suc (size_list (size_prod (\<lambda>x. 0) size) param_terms)" 
      by (induct param_terms) auto
  qed
  
(*
theorem dsem_correct:
  "(\<forall> vn sb v. dsem_term p_sem f_term vn sb v \<Longrightarrow> f_sem vn sb v) \<Longrightarrow>
  opsem_term p t vn sb v \<Longrightarrow> dsem_term p_sem t vn sb v"
*)  

subsection \<open>Examples\<close>

definition ex_term_plus :: term_t where
  [simp]: "ex_term_plus \<equiv> Plus (Var ''x'') (Rat_lit (1/3))"

lemma ex_term1_xq_pasem:
  "dsem_term dsem_prg_empty ex_term_plus (vn_xq q) 0 r \<Longrightarrow> 
      (\<exists>ar . r = (Some (Val_areal ar)) \<and> ar =!= (ar_of_rat (q+1/3)))"
  using ar_add_rat [of q "1/3"]
  by  auto

text \<open>The above lemma implies that any opsem execution of this term with these parameters 
  (A) terminates and makes no function calls (sb=0)
  (B) raises no exception
  (C) returns the result is q+1/3.\<close>

subsubsection \<open>Division\<close>

definition ex_term_div :: term_t where
  [simp]: "ex_term_div \<equiv> Div (Rat_lit 1) (Var ''x'')"

lemma ex_term_div_non0_xq_dsem:
  "(q \<noteq> 0) \<Longrightarrow> 
   dsem_term dsem_prg_empty ex_term_div (vn_xq q) 0 r \<Longrightarrow> 
   (\<exists> ar. r = Some (Val_areal ar) \<and> ar =!= ar_of_rat (1/q))"
  using ar_of_rat ar_of_rat_exact [of q]
  using ar_exact_unique_real [of "ar_of_rat q" "0" "of_rat q"] 
  using ar_div_rat [of q 1]
  by auto

lemma ex_term_div_0_xq_pasem:
  "(q = 0) \<Longrightarrow> dsem_term dsem_prg_empty ex_term_div (vn_xq q) 0 r \<Longrightarrow> r = None"
  using ar_of_rat [of 0]
  by auto

subsubsection \<open>Function call\<close>

definition ex_fncallterm1 :: term_t where
  [simp]: "ex_fncallterm1 = Fn_call ''f1'' [(''x'', Var ''x'')]"

definition dsem_prg_f1 :: "('ar::areal_c) dsem_prg_t" where
  [simp]: "dsem_prg_f1 =
    [(''f1'', \<lambda> vn sb v . 
        case (on_variable vn ''x'') of 
          (Some (Some (Val_areal x))) \<Rightarrow> v = Some (Val_areal (x + ar_of_rat (1/3)))
          | _ \<Rightarrow> v = None)]"
    
lemma ex_fncallterm_x1_pasem:
  "dsem_term dsem_prg_f1 ex_fncallterm1 (vn_xq q) 1 r \<Longrightarrow> 
      (\<exists>ar . r = (Some (Val_areal ar)) \<and> ar =!= (ar_of_rat (q+1/3)))"
  using ar_add_rat [of q "1/3"]
  by auto

subsection \<open>Safety\<close>

theorem opsem_in_dsem_sb0:
  "\<And>vn .(dsem_term prg_dsem t vn 0 (opsem_term prg t vn 0))"
proof (induct t)
    case (Nat_lit x)
    then show ?case by simp
  next
    case (Rat_lit x)
    then show ?case by simp
  next
    case (Var x)
    then show ?case by (auto split: option.split)
  next
    case (Plus t1 t2)
    then show ?case by auto
  next
    case (Div t1 t2)
    then show ?case by auto
  next
    case (MV_approx_eq t1 t2 t3)
    then show ?case
      apply (auto split: option.split)
      apply (auto split: value_t.split)
      apply fastforce
      apply fastforce
      apply fastforce
      apply fastforce
      by fastforce
  next
    case (If_then_else t1 t2 t3)
    then show ?case 
    proof (cases "opsem_term prg t1 vn 0")
      assume t1: "\<And>vn . dsem_term prg_dsem t1 vn 0 (opsem_term prg t1 vn 0)"
      case None
      then show ?thesis
        using t1 by fastforce
    next
      assume t1: "\<And>vn. dsem_term prg_dsem t1 vn 0 (opsem_term prg t1 vn 0)"
      assume t2: "\<And>vn. dsem_term prg_dsem t2 vn 0 (opsem_term prg t2 vn 0)"
      assume t3: "\<And>vn. dsem_term prg_dsem t3 vn 0 (opsem_term prg t3 vn 0)"
      case a: (Some a)
      then show ?thesis
        using t1 t2 t3
        apply (auto split: value_t.split bool.split)
        apply fastforce
        apply fastforce
        apply fastforce
        apply fastforce
        by fastforce  
    qed
  next
    case (Lim var_name t)
    then show ?case by auto
  next
    case (Fn_call fn_name param_terms)
    then show ?case by simp
qed


theorem opsem_in_dsem:
  "(\<And> f tf pt. on_fn prg f = Some (tf, pt) \<Longrightarrow> 
      (\<exists> df. on_fn prg_dsem f = Some df \<and>
        (\<forall> vnf sbf vof . 
          if vn_has_types pt vnf 
            then
                (dsem_term prg_dsem tf vnf sbf vof) \<longrightarrow> (df vnf sbf vof)
            else (df vnf sbf None))
      )
   )
   \<Longrightarrow>
   (\<And> f df. on_fn prg_dsem f = Some df \<Longrightarrow> (\<exists> tf_pt . on_fn prg f = Some tf_pt))
   \<Longrightarrow>
   (\<And> t vn . dsem_term prg_dsem t vn sb (opsem_term prg t vn sb))"
proof (induct sb)
  case 0
  then show ?case
    by (simp add: opsem_in_dsem_sb0)
next
  case (Suc sbp)
  then have sbp_ih_t_vn: "\<And> t vn . dsem_term prg_dsem t vn sbp (opsem_term prg t vn sbp)"
    by auto
  assume prg: "(\<And> f df. 
      on_fn prg_dsem f = Some df \<Longrightarrow> 
      (\<exists> tf . on_fn prg f = Some tf))"
  assume prg_dsem: "(\<And>f tf pt.
      on_fn prg f = Some (tf, pt) \<Longrightarrow>
      \<exists>df. on_fn prg_dsem f = Some df \<and>
           (\<forall>vnf sbf vof.
               if vn_has_types pt vnf 
               then (dsem_term prg_dsem tf vnf sbf vof \<longrightarrow> df vnf sbf vof) else (df vnf sbf None)))"
  show "\<And> vn . dsem_term prg_dsem t vn (Suc sbp) (opsem_term prg t vn (Suc sbp))"
    proof (induct t)
      case (Nat_lit x)
      then show ?case by simp
    next
      case (Rat_lit x)
    then show ?case by simp
    next
      case (Var x)
      then show ?case 
        by (auto split: option.split)
    next
      case (Plus t1 t2)
      then show ?case by auto
    next
      case (Div t1 t2)
      then show ?case by auto
    next
      case (MV_approx_eq t1 t2 t3)
      then show ?case
        apply (auto split: option.split)
        apply (auto split: value_t.split)
        apply fastforce
        apply fastforce
        apply fastforce
        apply fastforce
        by fastforce
    next
      case (If_then_else t1 t2 t3)
      then show ?case
      proof (cases "opsem_term prg t1 vn (Suc sbp)")
        assume t1: "\<And> vn . dsem_term prg_dsem t1 vn (Suc sbp) (opsem_term prg t1 vn (Suc sbp))"
        case None  
        then show ?thesis
          using t1 by fastforce
      next
        assume t1: "\<And>vn. dsem_term prg_dsem t1 vn (Suc sbp) (opsem_term prg t1 vn (Suc sbp))"
        assume t2: "\<And>vn. dsem_term prg_dsem t2 vn (Suc sbp) (opsem_term prg t2 vn (Suc sbp))"
        assume t3: "\<And>vn. dsem_term prg_dsem t3 vn (Suc sbp) (opsem_term prg t3 vn (Suc sbp))"
        case a: (Some a)
        then show ?thesis
          using t1 t2 t3
          apply (auto split: value_t.split bool.split)
          apply fastforce
          apply fastforce
          apply fastforce
          apply fastforce
          by fastforce  
      qed
    next
      case (Lim var_name t)
      then show ?case by auto
    next

      (* function call, the interesting case *)

      case t:(Fn_call fn_name param_terms)
      then show ?case 
      proof (cases "on_fn prg fn_name")
        case None
        then show ?thesis 
          using prg [of fn_name]
          by (auto split: option.split)
      next
        case tf_pt1:(Some tf_pt)
        then show ?thesis using t
        proof (cases tf_pt)
          case tf_pt2:(Pair tf param_types)

          let ?param_vals =
              "list.map 
                (\<lambda> (p,t) . (p, (opsem_term prg t vn (Suc sbp))))
               param_terms"
          let ?param_vn = "opsem_params prg param_terms vn (Suc sbp)"
          have param_vals_vn: "?param_vn = vn_of_list ?param_vals"
          proof (induct param_terms)
            case Nil
            then show ?case by simp
          next
            case c:(Cons p_t param_terms)
            then show ?case
            proof (cases p_t)
              case (Pair param_name t)
              then show ?thesis using c by auto
            qed
          qed
  
          assume "\<And>p_t t vn.
            p_t \<in> set param_terms \<Longrightarrow>
            t \<in> Basic_BNFs.snds p_t \<Longrightarrow>
            dsem_term prg_dsem t vn (Suc sbp) (opsem_term prg t vn (Suc sbp))"
          then have params: "dsem_params prg_dsem param_terms vn (Suc sbp) ?param_vals"
          proof (induct param_terms)
            case Nil
            then show ?case by simp
          next
            case (Cons p_t param_terms)
            then show ?case
            proof (cases p_t)
              case (Pair param_name t)
              then show ?thesis
                using Cons.hyps Cons.prems
                by auto fastforce
            qed
          qed
          
          show ?thesis using tf_pt1 tf_pt2
              using params
              using prg_dsem [of fn_name tf]
              using sbp_ih_t_vn [of tf ?param_vn]
              apply (auto split: option.split)
              apply fastforce
            proof-
              fix df
              assume df_pre: "(\<And>pt. param_types = pt \<Longrightarrow>
                  (\<forall>vnf sbf vof.
                    if vn_has_types pt vnf
                    then dsem_term prg_dsem tf vnf sbf vof \<longrightarrow>
                         df vnf sbf vof
                    else df vnf sbf None))"
              then have df: "\<And>vnf sbf vof.
                   if vn_has_types param_types vnf
                    then dsem_term prg_dsem tf vnf sbf vof \<longrightarrow>
                         df vnf sbf vof
                    else df vnf sbf None" by auto

              let ?vof = "let vnf = ?param_vn in
                         if vn_has_types param_types vnf
                         then opsem_term prg tf vnf sbp else None"
              have "df (?param_vn) sbp ?vof"
                  using df [of ?param_vn sbp ?vof]
                  using sbp_ih_t_vn [of tf ?param_vn]
                  by presburger
              then show "\<exists>param_vals.
                 dsem_params prg_dsem param_terms vn (Suc sbp) param_vals 
                 \<and>
                 df (vn_of_list param_vals) sbp
                    (let vnf = ?param_vn
                     in if vn_has_types param_types vnf
                        then opsem_term prg tf vnf sbp else None)"
                using params
                using param_vals_vn
                by auto
            qed
          qed
      qed
    qed
qed

end