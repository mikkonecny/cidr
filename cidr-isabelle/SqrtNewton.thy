theory SqrtNewton

imports Complex_Main AReal CIDRF1
   
begin

(****************************************************************************************)

section\<open>Newton Iteration\<close>

subsection \<open>Terms\<close>

definition 
  x_over_y_term :: term_t
  where [simp]:
  "x_over_y_term = Div (Var ''x'') (Var ''y'')"

definition 
  y_next_term :: term_t
  where [simp]:
  "y_next_term = Div (Plus (Var ''y'') x_over_y_term) (Nat_lit 2)"

definition
  rec_call_sqrt_Newton_Iter :: term_t
  where [simp]:
  "rec_call_sqrt_Newton_Iter =
      (Fn_call ''sqrt_Newton_Iter''
        [(''x'', (Var ''x'')),
         (''y'', y_next_term),
         (''e'', (Var ''e''))])"

definition 
  sqrt_Newton_Iter_term :: term_t
  where [simp]:
(* 
sqrt_Newton_Iter(x, y, e) = 
  if y =e= x/y
    then y
    else sqrt_Newton_Iter(x, (y+x/y)/2, e)
*)
  "sqrt_Newton_Iter_term =
    If_then_else 
      (* y =e= x/y *)
      (MV_approx_eq (Var ''e'')
        (Var ''y'')
        x_over_y_term)
      (Var ''y'')
      (* sqrt_Newton_Iter(x, (y+x/y)/2, e) *)
      rec_call_sqrt_Newton_Iter"

fun vn_xye :: "'ar \<Rightarrow> 'ar \<Rightarrow> rat \<Rightarrow> 'ar valuation_t" where
  "vn_xye x y e =
    vn_empty
    (''x'' \<mapsto> Some (Val_areal x),
     ''y'' \<mapsto> Some (Val_areal y),
     ''e'' \<mapsto> Some (Val_rat e))"

definition sqrt_Newton_Iter_param_types where
  [simp]:
  "sqrt_Newton_Iter_param_types = 
      [(''x'', Typ_real), (''y'', Typ_real), (''e'', Typ_rat)]"

definition sqrt_Newton_Iter_prg :: program_t where
  [simp]:
  "sqrt_Newton_Iter_prg = 
    [(''sqrt_Newton_Iter'', 
        (sqrt_Newton_Iter_term, sqrt_Newton_Iter_param_types))]"

subsection\<open>Specification\<close>

fun sqrt_Newton_Iter_pre :: "real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> bool" 
  where
  "sqrt_Newton_Iter_pre x y e = 
        (1 \<le> x
        \<and> sqrt x \<le> y 
        \<and> y \<le> x
        \<and> 0 < e)"

fun sqrt_Newton_Iter_stackpre :: "real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> stack_limit_t \<Rightarrow> bool" 
  where
  "sqrt_Newton_Iter_stackpre x y e sb = 
    (sqrt_Newton_Iter_pre x y e
     \<and> 2^sb \<ge> 2*(y^2-x)/(e*y))"

fun sqrt_Newton_Iter_post :: "real \<Rightarrow> real \<Rightarrow> error_bound \<Rightarrow> real \<Rightarrow> bool" where
  "sqrt_Newton_Iter_post x y e res = (sqrt x \<le> res \<and> \<bar>res - x/res\<bar> \<le> e)"
  

definition sqrt_Newton_Iter_dsem :: "('ar::areal_c) dsem_term_t" where
  [simp]: "sqrt_Newton_Iter_dsem vn sb v = 
    (case (vn ''x'', vn ''y'', vn ''e'') of
      (Some(Some (Val_areal xAR)), 
       Some(Some (Val_areal yAR)),
       Some (Some (Val_rat e))) \<Rightarrow>
        (* Stack use and accuracy *)
        (\<forall> eRes . 0 < eRes \<and> 
            ar_is_exact xAR \<and> ar_is_exact yAR \<longrightarrow> 
          (* TODO: replace the above line with a precondition based on a modulus of continuity *)
              (\<forall> x y. x \<in>ar xAR \<longrightarrow> y \<in>ar yAR \<longrightarrow> 
                sqrt_Newton_Iter_stackpre x y (q2r e) sb \<longrightarrow>
                (\<exists> resAR . v = Some (Val_areal resAR) \<and> resAR e\<le> eRes)))
        \<and>
        (* Safety *)
        (\<forall> x y resAR. 
          v = Some (Val_areal resAR) \<longrightarrow>
          x \<in>ar xAR \<longrightarrow> y \<in>ar yAR \<longrightarrow> 
          sqrt_Newton_Iter_pre x y (q2r e) \<longrightarrow>
          (\<exists> res. res \<in>ar resAR \<and>
          sqrt_Newton_Iter_post x y (q2r e) res)) |
      _ \<Rightarrow> v = None
    )"

definition sqrt_Newton_Iter_prg_dsem :: "('ar::areal_c) dsem_prg_t" where
  [simp]:
  "sqrt_Newton_Iter_prg_dsem = 
    [(''sqrt_Newton_Iter'', sqrt_Newton_Iter_dsem)]"


subsection\<open>Correctness\<close>


text \<open>The following abbreviation makes Isabelle's reporting of remaining goals shorter and more readable\<close>
  
abbreviation q2ar :: "rat \<Rightarrow> ('ar :: ar_rat)" where "q2ar \<equiv> ar_of_rat"

lemma [simp]: "(y::real) \<ge> 1 \<Longrightarrow> y-x/y = (y^2-x)/y"
  by (simp add: diff_divide_distrib power2_eq_square)

lemma [simp]: "(y::real) \<ge> 1 \<Longrightarrow> y+x/y = (y^2+x)/y"
  by (simp add: add_divide_distrib power2_eq_square)

theorem sqrt_Newton_Iter_dsem_correct:
  "vn_has_types sqrt_Newton_Iter_param_types vn \<Longrightarrow>
   dsem_term sqrt_Newton_Iter_prg_dsem sqrt_Newton_Iter_term vn sb v \<Longrightarrow>
   sqrt_Newton_Iter_dsem vn sb v"
proof -
  (****)
  text \<open>Obtain parameters from the valuation\<close>

  assume "vn_has_types sqrt_Newton_Iter_param_types vn"
  then obtain xA yA e where 
      vnx: "vn ''x'' = Some (Some (Val_areal xA))"
      and
      vny: "vn ''y'' = Some (Some (Val_areal yA))"
      and
      vne: "vn ''e'' = Some (Some (Val_rat e))"
    using voo_has_type_real [of "vn ''x''"]
    using voo_has_type_real [of "vn ''y''"]
    using voo_has_type_rat [of "vn ''e''"]
    by auto

  (****)
  text \<open>General numerical facts required in the proof\<close>

  have ypos: "\<And>x y . 1 \<le> x \<Longrightarrow> sqrt x \<le> y \<Longrightarrow> 1 \<le> y"
    using real_sqrt_ge_one order_trans 
    by blast

  have ynext_ge_sqrtx: "\<And>x y . 1 \<le> x \<longrightarrow> sqrt x \<le> y \<longrightarrow> sqrt x \<le> (y + x / y)/2"
  proof-
    fix x y
    have "1 \<le> x \<longrightarrow> sqrt x \<le> y \<longrightarrow> sqrt x * 2 \<le> (y + x / y)"
      using ypos sum_squares_bound [of "sqrt x" "y"]
      using \<open>2 * sqrt x * y \<le> (sqrt x)\<^sup>2 + y\<^sup>2\<close> le_divide_eq by fastforce
    then show "?thesis x y" by auto
  qed

  have ynext_le_x: "\<And>x y . 1 \<le> x \<longrightarrow> sqrt x \<le> y \<longrightarrow> y \<le> x \<longrightarrow> y + (x/y) \<le> x*2"
  proof-
    fix x y
    show "?thesis x y"
    using ypos mult_le_cancel_left_pos [of y "(x/y)" "y"]
    by (smt nonzero_mult_div_cancel_left one_le_of_rat_iff power2_eq_square times_divide_eq_right sqrt_le_D)
  qed

  have not02: "\<not> 0 \<in>ar q2ar 2"
    using ar_exact_unique_real ar_of_rat ar_of_rat_exact by fastforce

  have sbprec_ge_1: "\<And> x y e . 
    1 \<le> x \<longrightarrow> 
    sqrt x \<le> y \<longrightarrow> 
    0 < e \<longrightarrow>
    \<bar>y - x/y\<bar> > q2r e/2 \<longrightarrow>
    1 < (2 * y\<^sup>2 - 2 * x) / (q2r e * y)"
  proof-
    fix x y e
    have "sqrt x \<le> y \<longrightarrow> x \<le> y^2" by (simp add: sqrt_le_D)
    then have dpos: "1 \<le> x \<longrightarrow> sqrt x \<le> y \<longrightarrow>  0 \<le> y - x/y"
      using ypos
      by (smt divide_le_eq power2_eq_square)
    then have "1 \<le> x \<longrightarrow> sqrt x \<le> y \<longrightarrow>  
        \<bar>y - x/y\<bar> > q2r e/2 \<longrightarrow> y - x/y > q2r e/2" using dpos by auto
    then show "?thesis x y e"
      using less_divide_eq_1_pos [of "q2r e * y" "2 * y\<^sup>2 - 2 * x"]
      using ypos
      apply (auto simp add: pos_less_divide_eq mult.commute)
      by (metis divide_divide_eq_left less_divide_eq_1_pos zero_less_of_rat_iff)
  qed

  have ynext_exact: 
    "ar_is_exact xA \<Longrightarrow> ar_is_exact yA \<Longrightarrow> \<not> 0 \<in>ar yA \<Longrightarrow> 
     ar_is_exact ((yA + xA / yA)/(q2ar 2))"
    using ar_add_exact [of yA "xA/yA"]
    apply auto
    using ar_div_exact [of xA yA]
    using ar_div_exact [of "yA + xA/yA" "q2ar 2"]
    using ar_of_rat_exact [of 2]
    using not02
    by auto

  have ynext_safe: 
    "\<And>x y . x \<in>ar xA \<longrightarrow> y \<in>ar yA \<longrightarrow> \<not> 0 \<in>ar yA \<longrightarrow>
    (y+x/y)/2 \<in>ar ((yA + xA / yA)/(q2ar 2))"
  proof-
    fix x y
    show "?thesis x y"
      using ar_add_safe [of y yA "x/y" "xA/yA"]
      using ar_div_safe [of x xA y yA]
      using ar_div_safe [of "y+x/y" "yA + xA/yA" 2 "q2ar 2"]
      using not02 ar_of_rat [of 2]
      by auto
  qed

  (****)
  text\<open>The main proof\<close>

  assume dsem: "dsem_term sqrt_Newton_Iter_prg_dsem sqrt_Newton_Iter_term vn sb v"

  show ?thesis
  proof (cases "0 \<in>ar yA")
    (* Rule out y=0. *)
    case True
    then show ?thesis 
      using vnx vny vne dsem
      apply auto
    proof-
      fix x y :: real
      assume y: "y \<in>ar yA"
      assume sqrtX_Y: "sqrt x \<le> y"
      assume xpos: "1 \<le> x"
      assume yAexact: "\<forall>e>0. yA e\<le> e"
      then show "0 \<in>ar yA \<Longrightarrow> False"
        using y ypos xpos sqrtX_Y ar_exact_unique_real 
        by fastforce
    qed
  next
    case not0: False (* ie y\<noteq>0 *)
    show ?thesis
      using not0 vnx vny vne dsem
      proof (cases "ar_approx_eq e yA (xA / yA)")
        (* Rule out an exception during the comparison. *)
        case None
        then show ?thesis
          using vnx vny vne dsem not0
          using ar_approx_eq_defined [of e yA "xA/yA"]
          using ar_div_mod [of xA yA "q2r e/4"]
        by auto
      next
        case compRes: (Some compRes)
        show ?thesis 
        proof (cases compRes)
          case True (* equality established, y is returned *)
          then show ?thesis
            using vnx vny vne dsem not0 compRes
            using ar_approx_eq_not_true [of e _ yA _ "xA/yA"]
            using ar_div_mod [of xA yA "q2r e/4"]
            using ar_div_safe [of _ xA _ yA]
            apply (auto split: option.split simp del:rec_call_sqrt_Newton_Iter_def sqrt_Newton_Iter_prg_dsem_def)
            apply fastforce
            apply fastforce
            by fastforce
        next
          case rec:False (* inequality established, recursive call with yNext *)
          show ?thesis
          proof (cases "sb = 0")
            (* Rule out an exception due to insufficient stack space. *)
            case True
            then show ?thesis
            using vnx vny vne dsem not0 compRes rec
            using ar_approx_eq_not_false [of e _ yA _ "xA/yA"]
            using ar_div_safe [of _ xA _ yA]
            using sbprec_ge_1
            apply (auto split: option.split)
            by (metis not_le)
          next
            case sbnot0: False (* ie 1\<le>sb *)
            then obtain sbPred where sbPred: "sb = Suc sbPred"
              using gr0_conv_Suc by blast

            then show ?thesis
              using vnx vny vne dsem not0 compRes rec 
              apply (auto simp add:not02)
              using ynext_ge_sqrtx ynext_le_x
              using ynext_safe
          proof-
              fix x y
              assume x: "x \<in>ar xA"
              assume y: "y \<in>ar yA"
            
              assume xPos: "1 \<le> x"
              assume sqrtX_Y: "sqrt x \<le> y"
              assume x_y: "y \<le> x"

              have yPos: "1 \<le> y" using xPos sqrtX_Y ypos by auto

              { (* stack use and accuracy *)
                fix eRes :: error_bound
                assume eRes: "0 < eRes"
                assume xAexact: "\<forall>e>0. xA e\<le> e"
                assume yAexact: "\<forall>e>0. yA e\<le> e"
                assume ePos: "0 < e"
                assume sb: "(2*y^2 - 2*x) / (q2r e*y) \<le> 2*2 ^ sbPred"

                let ?yNext = "(y+x/y)/2"
                let ?e = "q2r e"
                have sbPred: "(2 * ?yNext\<^sup>2 - 2 * x) / (?e * ?yNext) \<le> 2 ^ sbPred"
                  apply auto
                  proof -
                    have dPos: "0 \<le> y^2-x" using sqrtX_Y by (simp add: sqrt_le_D)
                    then have "(y^2-x)/(y^2+x)\<le>1" using xPos by auto
                    then have "(y\<^sup>2 - x) / (y * ?e) \<ge> ((y\<^sup>2 - x) / (y * ?e))*(y\<^sup>2 - x) / (x + y\<^sup>2)"
                      using yPos dPos ePos xPos
                      using mult_right_le_one_le [of "(y\<^sup>2 - x) / (y * ?e)" "(y\<^sup>2 - x) / (x + y\<^sup>2)"]
                      using divide_nonneg_pos times_divide_eq_right zero_less_mult_iff zero_less_of_rat_iff
                      by auto
                    then have c: "(y\<^sup>2 - x) / (y * ?e) \<ge> y * ((y\<^sup>2 - x) / y)\<^sup>2 / (x * q2r e + q2r e * y\<^sup>2)"
                      using yPos power2_eq_square [of "(y\<^sup>2 - x) / y"]
                      apply auto
                      by (metis comm_semiring_class.distrib mult.commute mult.left_commute)
                    have s4: "\<And> a. 4 * (a / (y * 2))\<^sup>2 = a^2 / y^2"
                      using ypos
                      by (auto simp add: algebra_simps power2_eq_square)
                    have "(y\<^sup>2 + x)\<^sup>2 - 4 * x * y\<^sup>2 = (y\<^sup>2 - x)\<^sup>2"
                      by (auto simp add: algebra_simps power2_eq_square)
                    then have b: "4 * ((y\<^sup>2 + x) / (y * 2))\<^sup>2 - 4 * x = ((y^2 - x)/y)\<^sup>2"
                      using s4 [of "(y\<^sup>2 + x)"]
                      using yPos xPos
                      apply (auto simp add: algebra_simps nonzero_divide_eq_eq)
                      by (simp add: power_divide)
                    from sb have sb1a: "(y\<^sup>2 - x) / (?e * y) \<le> 2 ^ sbPred"
                      by (simp add: diff_divide_distrib)
                    from sb1a b c have d:
                      "(4 * ((y\<^sup>2 + x) / (y * 2))\<^sup>2 - 4 * x) * y / (q2r e * (y\<^sup>2 + x)) \<le> 2 ^ sbPred"
                    using add_divide_distrib
                    by (auto simp add: algebra_simps)
                    have e: "y + x/y = (y^2+x)/y"
                      by (simp add: add_divide_distrib power2_eq_square)
                    from d e show "(4 * ((y + x / y) / 2)\<^sup>2 - 4 * x) / (q2r e * (y + x / y)) \<le> 2 ^ sbPred"
                      by (auto simp add: algebra_simps)
                  qed
  
                assume "\<forall>eRes.
                  0 < eRes \<and> 
                  (\<exists>r. r \<in>ar xA) \<and> 
                  (\<forall>e>0. (yA + xA / yA) / q2ar 2 e\<le> e) \<and>
                  (\<exists>r. r \<in>ar (yA + xA / yA) / q2ar 2) \<longrightarrow>
                  (\<forall>x. x \<in>ar xA \<longrightarrow>
                       (\<forall>y. y \<in>ar (yA + xA / yA) / q2ar 2 \<longrightarrow>
                            1 \<le> x \<and>
                            sqrt x \<le> y \<and>
                            y \<le> x \<and>
                            (2 * y\<^sup>2 - 2 * x) / (q2r e * y) \<le> 2 ^ sbPred \<longrightarrow>
                            (\<exists>resAR.
                                v = Some (Val_areal resAR) \<and>
                                resAR e\<le> eRes)))"
                then show "\<exists>resAR. v = Some (Val_areal resAR) \<and> resAR e\<le> eRes"
                  using not0 eRes
                  using x y x_y sqrtX_Y xPos
                  using xAexact yAexact sb
                  using ynext_exact
                  using ynext_safe [of x y]
                  using ynext_ge_sqrtx [of x y]
                  using ynext_le_x [of x y]
                  using sbPred
                  by fastforce
              } (* end of stack use and accuracy *)
            
              (* safety *)
              fix resAR
              have "[xA,resAR] = [xA,resAR]".. (* this removes a type warning *)
              assume resAR: "\<forall>x. x \<in>ar xA \<longrightarrow>
                 (\<forall>y. y \<in>ar (yA + xA / yA) / q2ar 2 \<longrightarrow>
                      1 \<le> x \<and> sqrt x \<le> y \<and> y \<le> x \<longrightarrow>
                      (\<exists>res. res \<in>ar resAR \<and>
                             sqrt x \<le> res \<and> \<bar>res - x / res\<bar> \<le> q2r e))"

              then show "\<exists>res. res \<in>ar resAR \<and> sqrt x \<le> res \<and> \<bar>res - x / res\<bar> \<le> q2r e"
                using not0
                using x y x_y sqrtX_Y xPos
                using ynext_safe [of x y]
                using ynext_ge_sqrtx [of x y]
                using ynext_le_x [of x y]
                by auto
            qed
          qed
        qed
      qed
    qed
  qed

section\<open>Limit\<close>

subsection\<open>Terms\<close>

definition sqrt_Newton_term :: term_t where
  [simp]: "sqrt_Newton_term =
    Lim ''e''
      (Fn_call ''sqrt_Newton_Iter''
        [(''x'', Var ''x''),
         (''y'', Var ''x''),
         (''e'', Var ''e'')])"

definition sqrt_Newton_param_types where
  [simp]:
  "sqrt_Newton_param_types = 
      [(''x'', Typ_real)]"

fun vn_x :: "'ar \<Rightarrow> ('ar::areal_c) valuation_t" where
  "vn_x x = 
    vn_empty (''x'' \<mapsto> Some (Val_areal x))"

subsection\<open>Specification\<close>

fun sqrt_Newton_pre :: "real \<Rightarrow> bool" where
  "sqrt_Newton_pre x = (1 \<le> x)"

fun sqrt_Newton_stackpre :: "real \<Rightarrow> stack_limit_t \<Rightarrow> error_bound_rat \<Rightarrow> bool" where
  "sqrt_Newton_stackpre x sb eLimTerm = 
    (sqrt_Newton_pre x
     \<and> 1 \<le> sb 
     \<and> 2^(sb-1) \<ge> 2*(x-1)/(of_rat eLimTerm))"

fun sqrt_Newton_post :: "real \<Rightarrow> real \<Rightarrow> bool" where
  "sqrt_Newton_post x res = (res = sqrt x)"

definition sqrt_Newton_dsem :: "('ar::areal_c) dsem_term_t" where
  [simp]: "sqrt_Newton_dsem vn sb v = 
    (\<exists> xAR . 
      vn ''x'' = Some(Some (Val_areal xAR)) \<and>
        (* Stack use and accuracy *)
        (\<forall> eRes . 0 < eRes \<and> 
            ar_is_exact xAR \<longrightarrow> 
          (* TODO: replace the above line with a precondition based on a modulus of continuity *)
              (\<forall> x. x \<in>ar xAR \<longrightarrow> 
                sqrt_Newton_stackpre x sb (ar_lim_ix xAR eRes) \<longrightarrow>
                (\<exists> resAR . v = Some (Val_areal resAR) \<and> resAR e\<le> q2r eRes)))
        \<and>
        (* Safety *)
        (\<forall> x resAR. 
          v = Some (Val_areal resAR) \<longrightarrow>
          x \<in>ar xAR \<longrightarrow>  
          sqrt_Newton_pre x \<longrightarrow>
          (\<exists> res. res \<in>ar resAR \<and>
          sqrt_Newton_post x res)))"

subsection\<open>Correctness\<close>

lemma sbsuc: "1 \<le> sb \<Longrightarrow> \<exists>sbp. sb = Suc sbp"
  by (simp add: not0_implies_Suc)

theorem sqrt_Newton_dsem_correct:
  "dsem_term sqrt_Newton_Iter_prg_dsem sqrt_Newton_term (vn_x xA) sb v \<longrightarrow>
   sqrt_Newton_dsem (vn_x xA) sb v"
proof cases
  assume sb0: "sb = 0"
  then show ?thesis
    apply (auto simp del: vo_to_aro.simps)
  proof-
    fix f :: "error_bound_rat \<Rightarrow> ('ar::areal_c value_t) option"
    fix x :: real
    assume "\<forall>e>0. f e = None"
    then show "sqrt x \<in>ar ar_lim (\<lambda>e. vo_to_aro (f e))"
      using ar_lim_safe [of "sqrt x" "(\<lambda>e. vo_to_aro (f e))"]
      by simp
  qed
next
  assume sbnot0: "\<not> sb = 0"
  then show ?thesis 
    using sbsuc [of sb]
    apply (auto simp del: vo_to_aro.simps)
  proof-
    fix x
    assume x: "x \<in>ar xA"
    assume xPos: "1 \<le> x"
  
    from xPos have sqrtx: "sqrt x \<le> x"
      using less_eq_real_def sqrt_increase by auto

    have postcond_sqrt: "\<And> r. sqrt x \<le> r \<Longrightarrow> \<bar>sqrt x - r\<bar> \<le> \<bar>r-x/r\<bar>"
      using xPos
      using abs_of_pos [of "r - sqrt x"]
      using abs_of_pos [of "r - x/r"]
      by (smt divide_inverse divide_less_eq_1_pos real_div_sqrt real_sqrt_ge_one times_divide_eq_left)

    fix f 
    assume vf: "v = Some (Val_areal (ar_lim (\<lambda>e. vo_to_aro (f e))))"

    let ?iter_safe = 
        "\<forall>e > 0. (\<forall>x y resAR.
           vo_to_aro (f e) = Some resAR \<longrightarrow>
           x \<in>ar xA \<longrightarrow>
           y \<in>ar xA \<longrightarrow>
           1 \<le> x \<and> sqrt x \<le> y \<and> y \<le> x \<longrightarrow>
           (\<exists>res. res \<in>ar resAR \<and>
                  sqrt x \<le> res \<and> \<bar>res - x / res\<bar> \<le> q2r e))"

    have contains_sqrtx: "\<And>e. 0 < e \<Longrightarrow> ?iter_safe \<Longrightarrow>
                  (case vo_to_aro (f e) of None \<Rightarrow> True
                   | Some fe \<Rightarrow>
                       \<exists>r'. r' \<in>ar fe \<and> \<bar>sqrt x - r'\<bar> \<le> q2r e)"
    proof-
      fix e :: error_bound_rat
      assume e: "0 < e"
      assume ?iter_safe
      then have iter_safe: "\<And> e x y resAR . 0 < e \<Longrightarrow> 
           vo_to_aro (f e) = Some resAR \<longrightarrow>
           x \<in>ar xA \<longrightarrow>
           y \<in>ar xA \<longrightarrow>
           1 \<le> x \<and> sqrt x \<le> y \<and> y \<le> x \<longrightarrow>
           (\<exists>res. res \<in>ar resAR \<and>
                  sqrt x \<le> res \<and> \<bar>res - x / res\<bar> \<le> q2r e)" by auto
      show "?thesis e"
        using e x xPos sqrtx
        using iter_safe [of e _ x x]
        using postcond_sqrt
        by (auto split: option.split) force
    qed

    fix sbp
    assume sb: "sb = Suc sbp"

    { (* stack use and result accuracy *)
  
      fix eRes :: error_bound_rat
      assume eResPos: "0 < eRes"
    
      assume xAexact: "\<forall>e>0. xA e\<le> e"
      assume sbpBound: "(2 * x - 2) / q2r (ar_lim_ix xA eRes) \<le> 2 ^ sbp"
      
      from sbpBound xPos eResPos have sbpBoundXX:
        "(2 * x\<^sup>2 - 2 * x) / (q2r (ar_lim_ix xA eRes) * x)
           \<le> 2 ^ sbp"
        using ar_lim_ix [of eRes xA]
        by (metis (no_types, hide_lams) distrib_left mult.commute mult.left_commute mult_divide_mult_cancel_left_if mult_minus_right not_one_le_zero power2_eq_square uminus_add_conv_diff)
  
      assume itersem_pre:
        "\<forall>e>0.
          (\<forall>eRes.
              0 < eRes \<and> (\<exists>r. r \<in>ar xA) \<longrightarrow>
              (\<forall>x. x \<in>ar xA \<longrightarrow>
                   (\<forall>y. y \<in>ar xA \<longrightarrow>
                        1 \<le> x \<and>
                        sqrt x \<le> y \<and>
                        y \<le> x \<and>
                        (2 * y\<^sup>2 - 2 * x) / (q2r e * y)
                        \<le> 2 ^ sbp \<longrightarrow>
                        (\<exists>resAR. f e = Some (Val_areal resAR) \<and> resAR e\<le> eRes)))) \<and>
          (\<forall>x y resAR.
              f e = Some (Val_areal resAR) \<longrightarrow>
              x \<in>ar xA \<longrightarrow>
              y \<in>ar xA \<longrightarrow>
              1 \<le> x \<and> sqrt x \<le> y \<and> y \<le> x \<longrightarrow>
              (\<exists>res.
                  res \<in>ar resAR \<and>
                  sqrt x \<le> res \<and>
                  \<bar>res - x / res\<bar> \<le> q2r e))"
      then have itersem_pre1:
        "\<And> e eRes . e >0 \<and> 0 < eRes \<and> (\<exists>r. r \<in>ar xA) \<longrightarrow>
              (\<forall>x. x \<in>ar xA \<longrightarrow>
                   (\<forall>y. y \<in>ar xA \<longrightarrow>
                        1 \<le> x \<and>
                        sqrt x \<le> y \<and>
                        y \<le> x \<and>
                        (2 * y\<^sup>2 - 2 * x) / (q2r e * y)
                        \<le> 2 ^ sbp \<longrightarrow>
                        (\<exists>resAR. f e = Some (Val_areal resAR) \<and> resAR e\<le> q2r eRes)))"
        by auto
      have consistent: "\<exists>r. \<forall>e>0. case f e of
                        Some (Val_areal fe) \<Rightarrow>
                          \<exists>r'. r' \<in>ar fe \<and> \<bar>r - r'\<bar> \<le> q2r e | _ \<Rightarrow> True"
        using contains_sqrtx itersem_pre
        by (auto simp del:vo_to_aro.simps)
      have conv: "let e_ix = ar_lim_ix xA eRes
          in \<exists>xeA.
                f e_ix = Some (Val_areal xeA) \<and>
                xeA e\<le> q2r (eRes - e_ix)"
        using ar_lim_err [of "(\<lambda>e. vo_to_aro (f e))" eRes]
        using ar_lim_ix [of eRes "ar_lim (\<lambda>e. vo_to_aro (f e))"]
        using eResPos
        using x xPos sqrtx
        using sbpBoundXX ar_lim_ix_uniform [of eRes "ar_lim (\<lambda>e. vo_to_aro (f e))" "xA"]
        using contains_sqrtx itersem_pre1 [of "ar_lim_ix xA eRes" "eRes - ar_lim_ix xA eRes"]
        by (auto simp del:vo_to_aro.simps)
      show "ar_lim (\<lambda>e. vo_to_aro (f e)) e\<le> q2r eRes"
        using ar_lim_err [of "(\<lambda>e. vo_to_aro (f e))" eRes]
        using ar_lim_ix [of eRes "ar_lim (\<lambda>e. vo_to_aro (f e))"]
        using eResPos
        using x xPos sqrtx
        using sbpBoundXX ar_lim_ix_uniform [of eRes "ar_lim (\<lambda>e. vo_to_aro (f e))" "xA"]
        using consistent conv
        by (auto simp del:vo_to_aro.simps)
    }   (* end of: stack use and result accuracy *)

    (* safety *)

      assume itersem_pre:
        "\<forall>e>0.
          (\<forall>eRes.
              0 < eRes \<and>
              (\<forall>e>0. xA e\<le> e) \<and> (\<exists>r. r \<in>ar xA) \<longrightarrow>
              (\<forall>x. x \<in>ar xA \<longrightarrow>
                   (\<forall>y. y \<in>ar xA \<longrightarrow>
                        1 \<le> x \<and>
                        sqrt x \<le> y \<and>
                        y \<le> x \<and>
                        (2 * y\<^sup>2 - 2 * x) / (q2r e * y)
                        \<le> 2 ^ sbp \<longrightarrow>
                        (\<exists>resAR. f e = Some (Val_areal resAR) \<and> resAR e\<le> eRes)))) \<and>
          (\<forall>x y resAR.
              f e = Some (Val_areal resAR) \<longrightarrow>
              x \<in>ar xA \<longrightarrow>
              y \<in>ar xA \<longrightarrow>
              1 \<le> x \<and> sqrt x \<le> y \<and> y \<le> x \<longrightarrow>
              (\<exists>res.
                  res \<in>ar resAR \<and>
                  sqrt x \<le> res \<and>
                  \<bar>res - x / res\<bar> \<le> q2r e))"

    then show "sqrt x \<in>ar ar_lim (\<lambda>e. vo_to_aro (f e))"
      using contains_sqrtx
      using ar_lim_safe [of "sqrt x" "(\<lambda>e. vo_to_aro (f e))"]
      by (auto simp del:vo_to_aro.simps)
  qed
qed

lemma sqrt_Newton_Iter_prg_dsem_prg:
  "\<And> f df . on_fn sqrt_Newton_Iter_prg_dsem f = Some df \<Longrightarrow>
   \<exists>tf_pt. on_fn sqrt_Newton_Iter_prg f = Some tf_pt"
  apply auto
  by (metis on_fn.simps(1) option.distinct(1))

theorem sqrt_Newton_Iter_prg_prg_dsem:
  "\<And>f tf pt.
    on_fn sqrt_Newton_Iter_prg f = Some (tf, pt) \<Longrightarrow>
    \<exists>df. on_fn sqrt_Newton_Iter_prg_dsem f = Some df  \<and>
             (\<forall>vnf sbf vof.
                 if vn_has_types pt vnf
                 then dsem_term sqrt_Newton_Iter_prg_dsem tf vnf sbf
                       vof \<longrightarrow>
                      df vnf sbf vof
                 else df vnf sbf None)"
  using sqrt_Newton_Iter_dsem_correct
  apply (auto
          simp del: opsem_term.simps dsem_term.simps 
          sqrt_Newton_Iter_term_def 
          sqrt_Newton_Iter_dsem_def)
  apply blast
  apply (auto split:option.splits value_t.splits)
  by (metis on_fn.simps(1) option.distinct(1))

theorem sqrt_Newton_opsem_in_dsem:
  "sqrt_Newton_dsem (vn_x x) sb 
    (opsem_term sqrt_Newton_Iter_prg sqrt_Newton_term (vn_x x) sb)"
  using sqrt_Newton_Iter_prg_dsem_prg
  using sqrt_Newton_Iter_prg_prg_dsem
  using opsem_in_dsem [of sqrt_Newton_Iter_prg sqrt_Newton_Iter_prg_dsem sqrt_Newton_term "vn_x x" sb]
  using sqrt_Newton_dsem_correct [of x sb "opsem_term sqrt_Newton_Iter_prg sqrt_Newton_term (vn_x x) sb"]
  by (auto simp del: opsem_term.simps dsem_term.simps 
          sqrt_Newton_term_def sqrt_Newton_Iter_term_def 
          sqrt_Newton_dsem_def sqrt_Newton_Iter_dsem_def)

end
