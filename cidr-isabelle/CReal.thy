theory CReal

imports Complex_Main Log_Nat Code_Target_Int AReal

begin

(*  Title:      CReal.thy
    Author:     Michal Konecny, Aston University, 2018
*)

text \<open>Constructive real numbers via fast-converging Cauchy sequences\<close>

text \<open>This is work in progress.  In 2017-2018, this theory was developed as part of a pilot study with 
  a narrow purpose. Thus it is incomplete and generated code is not very efficient.\<close>

section \<open>Misc\<close>

lemma of_rat_abs: "q2r \<bar>a\<bar> = \<bar>q2r a\<bar>"
  by (simp add: abs_rat_def of_rat_minus)

lemma of_rat_min: "\<And> a b. q2r (min a b) = min (q2r a) (q2r b)"
  by (simp add: min_def of_rat_less_eq)


(****************************************************************************************)
section \<open>A datatype of constructive reals\<close>

subsection \<open>creal type\<close>

type_synonym radius_t = rat

type_synonym
  cros_t = "error_bound_rat \<Rightarrow> (rat \<times> radius_t) option"
  (* cros ~ center-radius option sequence *)

fun cros_goodrad_e :: "error_bound_rat \<Rightarrow> cros_t \<Rightarrow> bool" where
  "cros_goodrad_e e cros = 
      (case cros e of Some (_,r) \<Rightarrow> 0 \<le> r \<and> 2*r \<le> e | _ \<Rightarrow> True)"

fun cros_goodrad :: "cros_t \<Rightarrow> bool" where
  "cros_goodrad cr = (\<forall> e > 0 . cros_goodrad_e e cr)"

datatype creal_t = CReal (unCReal: cros_t)

subsection \<open>Consistency\<close>

fun
  real_in_cros_e :: "real \<Rightarrow> cros_t \<times> error_bound_rat \<Rightarrow> bool" (infixl "\<in>cros,e" 65)
  where
  "real_in_cros_e x (xS, e) = 
    (case xS e of None \<Rightarrow> True | Some (q, r) \<Rightarrow> \<bar>x - q2r q\<bar> \<le> q2r r)"

fun real_in_cros :: "real \<Rightarrow> cros_t \<Rightarrow> bool" (infixl "\<in>cros" 65)
  where
  "real_in_cros x xS = (\<forall> e . 0 < e \<longrightarrow> x \<in>cros,e (xS, e))"

fun cros_consistent :: "cros_t \<Rightarrow> bool" where
  "cros_consistent xS = (\<exists> x . x \<in>cros xS)"

lemma cros_consistent_overlap:
  "\<bar>c1-c2\<bar> \<le> r1 + r2" 
  if  "cros_consistent xS"   and "xS e1 = Some (c1,r1)" 
  and "xS e2 = Some (c2,r2)" and "0 < e1" and "0 < e2"
proof-
  from that have "\<bar>q2r c1-q2r c2\<bar> \<le> q2r r1 + q2r r2" 
    apply auto
    by (smt case_prod_conv option.simps(5))
  then show ?thesis
    by (smt abs_rat_def add_uminus_conv_diff of_rat_add of_rat_less_eq of_rat_minus)
qed

subsection \<open>Base\<close>

instantiation creal_t :: ar_base
begin
definition
  real_in_ar_creal_def[simp]: 
    "r \<in>ar cr = (r \<in>cros unCReal cr \<and> cros_goodrad (unCReal cr))"
definition
  ar_err_le_creal_def[simp]:
    "cr e\<le> e = (e > 0 \<and> cros_consistent (unCReal cr) \<and> cros_goodrad (unCReal cr) \<and>
      (\<forall> e'. q2r e' \<ge> e \<longrightarrow> \<not> Option.is_none (unCReal cr e')))"
instance proof
  fix cr :: creal_t
  fix e
  assume e: "cr e\<le> e"

  have rad: "\<And>q r e'. q2r e' \<ge> e \<Longrightarrow> unCReal cr e' = Some (q, r) \<Longrightarrow> 2 * r \<le> e'"
    using e
    by auto (smt case_prod_conv option.simps(5) zero_less_of_rat_iff)

  have "\<And> x y. x \<in>ar cr \<Longrightarrow> y \<in>ar cr \<Longrightarrow> \<bar>x-y\<bar> \<le> e"
  proof-
    fix x y :: real
    assume x: "x \<in>ar cr"
    assume y: "y \<in>ar cr"
    let ?cros = "unCReal cr"
    from x y have "\<And>e'. q2r e' \<ge> e \<Longrightarrow> \<bar>x-y\<bar> \<le> q2r e'"
    proof-
      fix e'
      assume e':"q2r e' \<ge> e"
      from e e' x have xe: "x \<in>cros,e (?cros, e')" 
        by (smt ar_err_le_creal_def real_in_ar_creal_def real_in_cros.simps zero_less_of_rat_iff)
      from e e' y have ye: "y \<in>cros,e (?cros, e')" 
        by (smt ar_err_le_creal_def real_in_ar_creal_def real_in_cros.simps zero_less_of_rat_iff)
      from e e' have cre: "\<not> Option.is_none (?cros e')" by auto
      from e xe ye cre show "\<bar>x-y\<bar> \<le> q2r e'"
        apply (auto split:option.split simp add: Option.is_none_def of_rat_divide)
        by (smt Groups.mult_ac(2) e' mult_2_right of_rat_add of_rat_less_eq rad)
    qed
    then show "\<bar>x-y\<bar> \<le> e"
      by (meson less_eq_real_def not_le of_rat_dense)
  qed

  from this e show 
      "0 < e \<and> (\<exists>x. x \<in>ar cr) \<and> 
       (\<forall>r1 r2. r1 \<in>ar cr \<and> r2 \<in>ar cr \<longrightarrow> \<bar>r1 - r2\<bar> \<le>  e)"
    by auto

  fix e'
  from e show "e < e' \<Longrightarrow> cr e\<le> e'" by auto
qed
end

subsection \<open>Rationals\<close>

instantiation creal_t :: ar_rat 
begin

definition ar_of_rat_creal_def[simp]:
  "ar_of_rat q = CReal (\<lambda>e. Some (q, 0))"

instance proof
  fix q
  show "ar_is_exact (ar_of_rat q :: creal_t)"
    by simp

  show "real_of_rat q \<in>ar (ar_of_rat q :: creal_t)"
    by simp
qed
end

subsection \<open>Addition\<close>

fun plus_cros :: "cros_t \<Rightarrow> cros_t \<Rightarrow> cros_t" where
  "plus_cros xS yS e = (case (xS (e/2), yS (e/2)) of 
      (Some (xc,xr), Some (yc,yr)) \<Rightarrow> Some (xc+yc, xr+yr) | _ \<Rightarrow> None)"

lemma plus_goodrad: 
  "cros_goodrad xS \<Longrightarrow> 
   cros_goodrad yS \<Longrightarrow>
   cros_goodrad (plus_cros xS yS)"
  apply (auto split:option.split)
  apply (smt add_nonneg_nonneg case_prod_conv half_gt_zero option.case_eq_if option.distinct(1) option.sel)
  by (smt Option.is_none_def half_gt_zero is_none_code(2) of_rat_1 of_rat_add of_rat_divide of_rat_less_eq old.prod.case one_add_one option.case_eq_if option.sel real_sum_of_halves)

instantiation creal_t :: plus
begin

definition plus_creal_def[simp]:
  "cr1 + cr2 = CReal (plus_cros (unCReal cr1) (unCReal cr2))"

instance ..
end

instantiation creal_t :: ar_add
begin

definition ar_add_mod1_creal_def[simp]:
  "ar_add_mod1 (cr :: creal_t) _ _ e = e/2"

definition ar_add_mod2_creal_def[simp]:
  "ar_add_mod2 (cr :: creal_t) _ _ e = e/2"

instance 
  apply intro_classes
  apply (auto simp del:real_in_ar_creal_def ar_of_rat_creal_def plus_creal_def ar_err_le_creal_def)
  proof -
    fix xA yA :: creal_t
    fix x y
    assume x: "x \<in>ar xA"
    assume y: "y \<in>ar yA"

    let ?xS = "unCReal xA"
    let ?yS = "unCReal yA"

    from x have xA_goodrad: "\<And>e . 0 < e \<Longrightarrow> cros_goodrad_e e ?xS"
      by simp
    from y have yA_goodrad: "\<And>e . 0 < e \<Longrightarrow> cros_goodrad_e e ?yS"
      by simp
  
    have consistent: "\<And>e xc xr yc yr.
           unCReal yA (e / 2) = Some (yc, yr) \<Longrightarrow>
           unCReal xA (e / 2) = Some (xc, xr) \<Longrightarrow>
           0 < e \<Longrightarrow> \<bar>x + y - q2r (xc + yc)\<bar> \<le> q2r (xr + yr)"
      using x y
      by auto (smt case_prod_conv half_gt_zero of_rat_add option.simps(5))

    show "x + y \<in>ar (xA + yA)"
      using plus_goodrad xA_goodrad yA_goodrad consistent
      by (auto split: option.split)

    fix e :: error_bound
    assume ePos: "0 < e"
  
    assume xe2: "xA e\<le> e/2" 
    assume ye2: "yA e\<le> e/2"

    from xe2 have xe2some:"\<And> e'. e \<le> q2r e' \<Longrightarrow> unCReal xA (e' / 2) = None \<Longrightarrow> False"
      by (smt ar_err_le_creal_def divide_right_mono is_none_simps(1) of_rat_1 of_rat_add of_rat_divide one_add_one)
    from ye2 have ye2some:"\<And> e'. e \<le> q2r e' \<Longrightarrow> unCReal yA (e' / 2) = None \<Longrightarrow> False"
      by (smt ar_err_le_creal_def divide_right_mono is_none_simps(1) of_rat_1 of_rat_add of_rat_divide one_add_one)
  
    show "xA + yA e\<le> e"
      using xe2 ye2 xe2some ye2some consistent
      using plus_goodrad xA_goodrad yA_goodrad
      apply (auto split: option.split)
      by blast

qed

end

subsection \<open>Multiplication\<close>

fun norm_cros :: "cros_t \<Rightarrow> error_bound_rat \<Rightarrow> rat option" where
  "norm_cros xS e = (case xS e of Some (xc,xr) \<Rightarrow> Some (\<bar>xc\<bar>+xr) | _ \<Rightarrow> None)"

lemma abs_le_norm: 
  "0 < e \<Longrightarrow> case norm_cros xS e of 
    None \<Rightarrow> True | 
    Some b \<Rightarrow> (x \<in>cros xS \<longrightarrow> \<bar>x\<bar> \<le> q2r b)"
  apply (auto split:option.split)
  by (smt abs_rat_def of_rat_add of_rat_less_0_iff of_rat_minus)

lemma abs_e_ge_norm: 
  "0 < e \<Longrightarrow> cros_goodrad xS \<Longrightarrow>
  case norm_cros xS e of 
    None \<Rightarrow> True | 
    Some b \<Rightarrow> (x \<in>cros xS \<longrightarrow> q2r b \<le> \<bar>x\<bar>+(2 * (q2r e)))"
  apply (auto split:option.split)
  by (smt Groups.mult_ac(2) abs_rat_def case_prod_conv mult_2_right of_rat_add of_rat_less_eq of_rat_minus option.simps(5))

fun times_cros_pre :: "cros_t \<Rightarrow> cros_t \<Rightarrow> cros_t" where
  "times_cros_pre xS yS e = 
    (case (norm_cros xS e, norm_cros yS e) of
      (Some nx, Some ny) \<Rightarrow>
         let 
           ex = 2*(min e (e/(4*ny+6*e)));
           ey = 2*(min e (e/(4*nx+6*e)))
         in
            (case (xS ex, yS ey) of
                  (Some (cx,rx), Some (cy,ry)) \<Rightarrow> 
                    Some (cx*cy, \<bar>cx\<bar>*ry + \<bar>cy\<bar>*rx + rx*ry)
                  | _ \<Rightarrow> None)
      | _ \<Rightarrow> None)"

fun times_cros :: "cros_t \<Rightarrow> cros_t \<Rightarrow> cros_t" where
  "times_cros xS yS e = times_cros_pre xS yS (min 1 e)"

lemma times_goodrad_pre_safe:
  "cros_goodrad (times_cros_pre xS yS) \<and> x*y \<in>cros (times_cros_pre xS yS)"
  if  "cros_goodrad xS" and "cros_goodrad yS" 
  and "x \<in>cros xS" and "y \<in>cros yS"
  apply (auto split:option.split)
proof -
  from that have xg: "\<And>e. 0<e \<Longrightarrow>
          case xS e of None \<Rightarrow> True
          | Some (xa, r) \<Rightarrow> 0 \<le> r \<and> 2 * r \<le> e" by auto

  from that have yg: "\<And>e. 0<e \<Longrightarrow>
          case yS e of None \<Rightarrow> True
          | Some (xa, r) \<Rightarrow> 0 \<le> r \<and> 2 * r \<le> e" by auto

  from that have xCT: "cros_consistent xS" by auto
  from that have yCT: "cros_consistent yS" by auto
  from that have x: "\<And>e . e>0 \<Longrightarrow>
       case xS e of None \<Rightarrow> True
       | Some (q, r) \<Rightarrow> \<bar>x - q2r q\<bar> \<le> q2r r" by auto
  from that have y: "\<And>e . e>0 \<Longrightarrow>
       case yS e of None \<Rightarrow> True
       | Some (q, r) \<Rightarrow> \<bar>y - q2r q\<bar> \<le> q2r r" by auto

  fix e :: error_bound_rat
  assume e: "0<e"
  fix yc1 yr1
  assume ye2: "yS e = Some (yc1, yr1)"
  fix xc1 xr1
  assume xe2: "xS e = Some (xc1, xr1)"

  from ye2 have yr1: "0 \<le> yr1 \<and> 2*yr1 \<le> e"
    using yg [of "e"] e by simp
  from xe2 have xr1: "0 \<le> xr1 \<and> 2*xr1 \<le> e"
    using xg [of "e"] e by simp

  fix yc yr
  assume yey: "yS (2 * min e (e / (4 * \<bar>xc1\<bar> + 4 * xr1 + 6*e))) = Some (yc, yr)"
  fix xc xr
  assume xex: "xS (2 * min e (e / (4 * \<bar>yc1\<bar> + 4 * yr1 + 6*e))) = Some (xc, xr)"

  from yey have yr: "0 \<le> yr \<and> 2*yr \<le> 2 * min e (e / (4 * \<bar>xc1\<bar> + 4 * xr1 + 6*e))" 
    using e xr1 yg [of "2 * min e (e / (4 * \<bar>xc1\<bar> + 4 * xr1 + 6*e))"]
    by auto

  from xex have xr: "0 \<le> xr \<and> 2*xr \<le> 2 * min e (e / (4 * \<bar>yc1\<bar> + 4 * yr1 + 6*e))" 
    using e yr1 xg [of "2 * min e (e / (4 * \<bar>yc1\<bar> + 4 * yr1 + 6*e))"]
    by auto

  from xr yr show "0 \<le> \<bar>xc\<bar> * yr + \<bar>yc\<bar> * xr + xr * yr" by simp

  from xr have xr_e: "xr \<le> e" by auto
  from yr have yr_e: "yr \<le> e" by auto

  have "\<bar>xc - xc1\<bar> \<le> xr + xr1"
    using that e xex xe2 yr1 xCT
    using cros_consistent_overlap [of xS _ xc xr e xc1 xr1]
    by auto

  then have xc:"\<bar>xc\<bar> \<le> \<bar>xc1\<bar> + xr1 + xr"
    by auto

  have "\<bar>yc - yc1\<bar> \<le> yr + yr1"
    using that e yey ye2 xr1 yCT
    using cros_consistent_overlap [of yS _ yc yr e yc1 yr1]
    by auto

  then have yc:"\<bar>yc\<bar> \<le> \<bar>yc1\<bar> + yr1 + yr"
    by auto

  then have "2* (\<bar>xc\<bar> * yr) + xr * yr \<le> e/2"
  proof-
    have "yr * (4 * \<bar>xc1\<bar> + 4 * xr1 + 6 * e) \<le> e"
      using yr xr1 e
      using pos_le_divide_eq [of "(4 * \<bar>xc1\<bar> + 4 * xr1 + 6 * e)"]
      by auto
    then have "4 * ((\<bar>xc1\<bar> + xr1 + xr) * yr) + xr * yr * 2 \<le> e"
      using xr_e yr_e yr
      using mult_left_mono [of xr e "yr*6"]
      by (auto simp add: algebra_simps)
    then show ?thesis  
      using xc yr
      using mult_right_mono [of "\<bar>xc\<bar>" "\<bar>xc1\<bar> + xr1 + xr" yr] 
      by auto
  qed

  moreover have  "2* (\<bar>yc\<bar> * xr) + xr * yr \<le> e/2"
  proof-
    have "xr * (4 * \<bar>yc1\<bar> + 4 * yr1 + 6 * e) \<le> e"
      using xr yr1 e
      using pos_le_divide_eq [of "(4 * \<bar>yc1\<bar> + 4 * yr1 + 6 * e)"]
      by auto
    then have "4 * ((\<bar>yc1\<bar> + yr1 + yr) * xr) + yr * xr * 2 \<le> e"
      using xr_e yr_e xr
      using mult_left_mono [of yr e "xr*6"]
      by (auto simp add: algebra_simps)
    then show ?thesis  
      using yc xr
      using mult_right_mono [of "\<bar>yc\<bar>" "\<bar>yc1\<bar> + yr1 + yr" xr] 
      by (auto simp add: algebra_simps)
  qed

  ultimately show "2* (\<bar>xc\<bar> * yr) + 2* (\<bar>yc\<bar> * xr) + 2 * (xr * yr) \<le> e"
    by auto


  have xxr: "\<bar>x - q2r xc\<bar> \<le> q2r xr"
    using xex e yr1 x [of "(2 * min e (e / (4 * \<bar>yc1\<bar> + 4 * yr1 + 6 * e)))"]
    by auto

  have yyr: "\<bar>y - q2r yc\<bar> \<le> q2r yr"
    using yey e xr1 y [of "(2 * min e (e / (4 * \<bar>xc1\<bar> + 4 * xr1 + 6 * e)))"]
    by auto

  have "x * y - q2r (xc * yc) = (q2r xc)*(y-(q2r yc)) + (q2r yc)*(x-(q2r xc)) + (x-(q2r xc))*(y-(q2r yc))"
    by (auto simp add: algebra_simps of_rat_mult)

  then have "\<bar>x * y - q2r (xc * yc)\<bar> \<le> q2r \<bar>xc\<bar>*\<bar>y-(q2r yc)\<bar> + q2r \<bar>yc\<bar>*\<bar>x-(q2r xc)\<bar> + \<bar>x-(q2r xc)\<bar>*\<bar>y-(q2r yc)\<bar>"
    using abs_triangle_ineq [of "q2r xc * (y - q2r yc) + q2r yc * (x - q2r xc)" "(x - q2r xc) * (y - q2r yc)"]
    using abs_triangle_ineq [of "q2r xc * (y - q2r yc)" "q2r yc * (x - q2r xc)"]
    by (auto simp add: abs_mult le_trans of_rat_abs)

  then show "\<bar>x * y - q2r (xc * yc)\<bar> \<le> q2r (\<bar>xc\<bar> * yr + \<bar>yc\<bar> * xr + xr * yr)"
    using xxr yyr
    using mult_left_mono [of "\<bar>x - q2r xc\<bar>" "q2r xr" "q2r \<bar>yc\<bar>"]
    using mult_left_mono [of "\<bar>y - q2r yc\<bar>" "q2r yr" "q2r \<bar>xc\<bar>"]
    apply (auto simp add: of_rat_add of_rat_mult mult.commute)
    by (smt mult_mono')    
qed

lemma times_goodrad_safe:
  "cros_goodrad (times_cros xS yS) \<and> x*y \<in>cros (times_cros xS yS)"
  if  "cros_goodrad xS" and "cros_goodrad yS" 
  and "x \<in>cros xS" and "y \<in>cros yS"
  using times_goodrad_pre_safe [of xS yS x y]
  apply (auto simp del: times_cros_pre.simps cros_goodrad_e.simps real_in_cros_e.simps)
proof -
  fix e
  assume ""
  assume "e\<le>1"
  then show ?thesis sorry
next
  case False
  then show ?thesis sorry
qed
  sorry


instantiation creal_t :: times
begin

definition times_creal_def[simp]:
  "cr1 * cr2 = CReal (times_cros (unCReal cr1) (unCReal cr2))"

instance ..
end

instantiation creal_t :: ar_mul
begin

definition ar_mul_mod1_creal_def[simp]:
  "ar_mul_mod1 (cr :: creal_t) _ y e = min e (e/(2*\<bar>y\<bar>+5*(min 1 e)))"

definition ar_mul_mod2_creal_def[simp]:
  "ar_mul_mod2 (cr :: creal_t) x _ e = min e (e/(2*\<bar>x\<bar>+5*(min 1 e)))"

instance 
  apply intro_classes
   apply (auto simp del:real_in_ar_creal_def ar_of_rat_creal_def times_creal_def ar_err_le_creal_def)
proof-
  fix x y
  fix xA yA :: creal_t
  assume x: "x \<in>ar xA"
  assume y: "y \<in>ar yA"

  show "x * y \<in>ar xA * yA"
    using x y times_goodrad_safe [of "unCReal xA" "unCReal yA" x y]
    by auto

  fix e :: error_bound
  assume ePos: "0 < e"

  assume xye: "\<forall>x. x \<in>ar xA \<longrightarrow> (\<forall>y. y \<in>ar yA \<longrightarrow>
                xA e\<le> min e (e / (2* \<bar>y\<bar>+ 5*(min 1 e))) \<and>
                yA e\<le> min e (e / (2* \<bar>x\<bar>+ 5*(min 1 e))))"

  from xye x have xe: "\<And>y. y \<in>ar yA \<Longrightarrow> xA e\<le> min e (e / (2* \<bar>y\<bar>+ 5*(min 1 e)))" by auto
  from xye y have ye: "\<And>x. x \<in>ar xA \<Longrightarrow> yA e\<le> min e (e / (2* \<bar>x\<bar>+ 5*(min 1 e)))" by auto

  have xAe: "\<And>e'. q2r e' \<ge> e  \<longrightarrow> \<not> Option.is_none (unCReal xA (e'))" 
    using xe y by auto
  have yAe: "\<And>e'. q2r e' \<ge> e  \<longrightarrow> \<not> Option.is_none (unCReal yA (e'))" 
    using ye x by auto

  have mono: "\<And> e' a . e \<le> e' \<Longrightarrow> 0 \<le> a  \<Longrightarrow> 
        min e (e / (2 * a + 5 * (min 1 e))) \<le> 2*(min e' (e' / (2 * a + 5 * (min 1 e'))))"
  proof -
    fix e' a b :: real
    assume e': "e \<le> e'"
    assume a: "0 \<le> a"
    assume b: "a \<le> b"
    from ePos a b have d1: "2 * b + 5 * e > 0" by auto
    from ePos e' a have d2: "4 * (a + e' -e) + 6 * e' > 0" by auto

    have t1: "(4 * (a + e' -e) + 6 * e') * e \<le> (4 * b + 6 * e) * e'"
      using e' a b ePos
      using mult_left_mono 
      using mult_right_mono
      apply (auto simp add: algebra_simps mult_mono')
      by auto

    have "e / (4 * b + 6 * e) \<le> e' / (4 * (a + e' - e) + 6 * e')"
      using ePos e' b d1 d2 t1
      using mult_le_cancel_left_pos [of "(4 * (a+e'-e) + 6 * e') * (4 * b + 6 * e)" "e / (4 * b + 6 * e)" "e' / (4 * (a+e'-e) + 6 * e')"]
      by auto
    then show "?thesis e' a b"
      using ePos e'
      by auto
  qed

  have xAex:
    "\<And>e' ny. q2r e' \<ge> e \<longrightarrow> e \<le> 1 \<longrightarrow>  0 \<le> ny \<longrightarrow> q2r ny \<le> \<bar>y\<bar> + (q2r e') \<longrightarrow> 
        \<not> Option.is_none (unCReal xA (2*(min e' (e' / (4 * ny + 6 * e')))))"
  proof-
    fix e' ny
    have of_rat_exy: "q2r (2 * min e' (e' / (e' * 6 + ny * 4))) 
        = 2 * min (q2r e') (q2r e' / (q2r e' * 6 + q2r ny * 4))"
    by (simp add: of_rat_add of_rat_divide of_rat_mult of_rat_min)

    then show "?thesis e' ny"
      using xe x y ePos
      using mono [of "q2r e'" "q2r ny+(q2r e')-e" "\<bar>y\<bar> + e"]
      by (auto split:option.split simp add: algebra_simps)
  qed

  have yAey:
    "\<And>e' nx. q2r e' \<ge> e  \<longrightarrow>  0 \<le> nx \<longrightarrow> q2r nx \<le> \<bar>x\<bar> + e \<longrightarrow> 
        \<not> Option.is_none (unCReal yA (2*(min e' (e' / (4 * nx + 6 * e')))))"
  proof-
    fix e' nx
    have of_rat_exy: "q2r (2 * min e' (e' / (e' * 6 + nx * 4))) 
        = 2 * min (q2r e') (q2r e' / (q2r e' * 6 + q2r nx * 4))"
    by (simp add: of_rat_add of_rat_divide of_rat_mult of_rat_min)

    then show "?thesis e' nx"
      using ye x y ePos
      using mono [of "q2r e'" "q2r nx" "\<bar>x\<bar> + e"]
      by (auto split:option.split simp add: algebra_simps)
  qed

  show "xA * yA e\<le> e"
    using ePos
    using x y times_goodrad_safe [of "unCReal xA" "unCReal yA" x y]
    apply (auto simp del:times_cros.simps)
  proof-
    fix e'
    assume e': "e \<le> q2r e'"
    then show "Option.is_none (times_cros (unCReal xA) (unCReal yA) e') \<Longrightarrow> False"
      using xAe [of e']
      using yAe [of e']
      apply (auto simp add: Option.is_none_def)
      using ePos e' xAex [of e' "the (norm_cros (unCReal yA) e')"]
      using x y xe [of y] abs_le_norm [of e' y "unCReal yA"]
      using abs_e_ge_norm [of e' "unCReal yA" y]
      using zero_le_of_rat_iff [of "the (norm_cros (unCReal yA) e')"]
      apply auto
      
      apply (smt abs_ge_zero add_nonneg_nonneg case_prod_conv option.simps(5) zero_less_of_rat_iff)
      
      using yAey
      using abs_le_norm [of _ _ "unCReal yA"]
      using abs_e_ge_norm [of _ "unCReal yA"]
      apply (auto simp add: Option.is_none_def)
      sorry
  qed
    using xAe yAe xAex yAey
    using abs_le_norm [of _ _ "unCReal xA"]
    using abs_le_norm [of _ _ "unCReal yA"]
    using abs_e_ge_norm [of _ "unCReal xA"]
    using abs_e_ge_norm [of _ "unCReal yA"]
    apply (auto simp del:real_in_ar_creal_def ar_of_rat_creal_def times_creal_def ar_err_le_creal_def)

    (* TODO:  *)
    
    
  
    sorry


qed


end

section\<open>Code generation test\<close>

definition oneAR :: creal_t where "oneAR = (ar_of_rat 1)"
definition twoAR :: creal_t where "twoAR = oneAR + oneAR"
definition threeAR :: creal_t where "threeAR = twoAR + oneAR"
definition sixAR :: creal_t where "sixAR = twoAR * threeAR"

export_code sixAR in Haskell
  module_name CReal file "/t/isabelle"

(*

lemma times_cros [code]:
  "Rep_creal (xA * yA) = times_cros (Rep_creal xA) (Rep_creal yA)"
  using times_goodrad Rep_creal Abs_creal_inverse mem_Collect_eq
  by (smt times_creal_def cros_goodrad.elims(2))

*)

(*

abbreviation rat_close_n :: "rat \<Rightarrow> rat \<Rightarrow> nat \<Rightarrow> bool" where
  "rat_close_n q1 q2 n \<equiv> \<bar>q1 - q2\<bar> \<le> 1/(2^n)"

abbreviation creal_close_n_m_ifdef :: "creal \<Rightarrow> accuracy_t \<Rightarrow> accuracy_t \<Rightarrow> bool" where
  "creal_close_n_m_ifdef cr n m \<equiv> 
      cr n = None \<or> cr m = None \<or>
        (n < m \<longrightarrow> rat_close_n (the (cr n)) (the (cr m)) n)"

abbreviation creal_close_n_m :: "creal \<Rightarrow> accuracy_t \<Rightarrow> accuracy_t \<Rightarrow> bool" where
  "creal_close_n_m cr n m \<equiv> 
      \<not> cr n = None \<and> \<not> cr m = None \<and>
        (n < m \<longrightarrow> rat_close_n (the (cr n)) (the (cr m)) n)"

abbreviation creal_wheredef_convp :: "creal \<Rightarrow> bool" where
  "creal_wheredef_convp cr \<equiv> \<forall> n m. creal_close_n_m_ifdef cr n m"

abbreviation creal_alldef :: "creal \<Rightarrow> bool" where
  "creal_alldef cr \<equiv> \<forall> n. \<not> (cr n = None)"

abbreviation creal_convp :: "creal \<Rightarrow> bool" where
  "creal_convp cr \<equiv> creal_wheredef_convp cr \<and> creal_alldef cr"

lemma creal_conv_close: 
  "\<And>n m. creal_convp cr \<Longrightarrow> creal_close_n_m cr n m"
  by blast


(****************************************************************************************)
section \<open>Equality comparison\<close>

fun creal_approx_eq :: "nat \<Rightarrow> creal \<Rightarrow> creal \<Rightarrow> bool option"
  where
  "creal_approx_eq n r1 r2 =
    (case (r1 (n+3), r2 (n+3)) of
      (Some r1q, Some r2q) \<Rightarrow> 
        Some (\<bar>r1q - r2q\<bar> < 3/(2^(n+2))) |
      _ \<Rightarrow> None)"

lemma creal_approx_eq_partialinfo:
  assumes 
   aconv: "creal_wheredef_convp a" and
   adef: "a ac\<ge> (n+3)" and
   bconv: "creal_wheredef_convp b" and
   bdef: "b ac\<ge> (n+3)" and
   ar: "ar \<in>cr a" and
   br: "br \<in>cr b"
 shows
    "\<bar>ar - br\<bar> < of_rat (1/2^(n+1)) \<Longrightarrow> 
     (creal_approx_eq n a b) = Some True"
  and
   "\<bar>ar - br\<bar> > of_rat (1/2^(n)) \<Longrightarrow> 
    (creal_approx_eq n a b) = Some False"
proof-
  from aconv ar adef have arclose: "\<bar>ar - of_rat (the (a (n+3)))\<bar> \<le> of_rat (1/2^(n+3))"
    by (smt creal_upto_accuracy.elims(2) diff_le_self real_in_creal.elims(2))
  from bconv br bdef have brclose: "\<bar>br - of_rat (the (b (n+3)))\<bar> \<le> of_rat (1/2^(n+3))"
    by (smt creal_upto_accuracy.elims(2) diff_le_self real_in_creal.elims(2))

  have of_rat_dist_less: "\<And> x y z. \<bar>of_rat x - of_rat y\<bar> < real_of_rat z \<Longrightarrow> \<bar>x-y\<bar> < z"
    by (smt abs_rat_def of_rat_diff of_rat_less of_rat_minus)
  have dist3_less: "\<And>y2 y1 d1 y3 d2 y4 d3. 
      \<bar>y2-y1\<bar>\<le>(of_rat d1) \<Longrightarrow> \<bar>y2-y3\<bar><(of_rat d2) \<Longrightarrow> \<bar>y3-y4\<bar>\<le>(of_rat d3) \<Longrightarrow> 
        \<bar>y1-y4\<bar> < real_of_rat(d1+d2+d3)"
    by (simp add: of_rat_add)
  have *<: "(3::rat) / (4 * 2 ^ n) = 2 / 2 ^ (n + 3) + 1 / (2 * 2 ^ n)"
    proof -
      have "((1::rat) + 1) / (2 ^ n * 2 ^ 3) = 2 / 2 ^ (n + 3)"
        by (metis numeral_Bit0 numeral_One power_add)
      then show ?thesis by force
    qed
  from arclose brclose adef bdef
  show "\<bar>ar - br\<bar> < of_rat (1/2^(n+1)) \<Longrightarrow> 
          (creal_approx_eq n a b) = Some True"
    using dist3_less [of "ar" "of_rat(the (a (n+3)))" "1/2^(n+3)"
                    "br" "1/(2*2^n)"
                    "of_rat(the (b (n+3)))"  "1/2^(n+3)"]
    using *< of_rat_dist_less [of "the (a (n+3))"  "the (b (n+3))"]
    by (auto simp: diff_Real option.case_eq_if)

  have of_rat_dist_more: "\<And> z x y. real_of_rat z < \<bar>of_rat x - of_rat y\<bar>  \<Longrightarrow> z < \<bar>x-y\<bar>"
    by (smt abs_rat_def less_iff_diff_less_0 of_rat_diff of_rat_less of_rat_minus)
  have dist3_more: "\<And>y1 y2 d1 d2 y3 y4 d3. 
      \<bar>y2-y1\<bar>\<le>of_rat d1 \<Longrightarrow> of_rat d2<\<bar>y2-y3\<bar> \<Longrightarrow> \<bar>y3-y4\<bar>\<le>of_rat d3 \<Longrightarrow> 
        \<bar>y1-y4\<bar> > real_of_rat(d2-d1-d3)"
    by (simp add: of_rat_diff)
  have *>: "(3::rat) / (4 * 2 ^ n) = 1 / 2 ^ n - 2 / 2 ^ (n + 3)"
    proof -
      have "((1::rat) + 1) / (2 ^ n * 2 ^ 3) = 2 / 2 ^ (n + 3)"
        by (metis numeral_Bit0 numeral_One power_add)
      then show ?thesis by force
    qed
  from arclose brclose adef bdef
  show "\<bar>ar - br\<bar> > of_rat (1/2^(n)) \<Longrightarrow> 
          (creal_approx_eq n a b) = Some False"
    using dist3_more [of "ar" "of_rat(the (a (n+3)))" "1/2^(n+3)"
                    "1/(2^n)" "br"
                    "of_rat(the (b (n+3)))"  "1/2^(n+3)"]
    using *> of_rat_dist_more [of "3/(4*2^n)" "the (a (n+3))"  "the (b (n+3))"]
    by (auto simp: diff_Real option.case_eq_if)
qed

lemma creal_approx_eq:
  assumes 
   aconv: "creal_convp a" and
   bconv: "creal_convp b"
  shows 
   "\<bar>real_of_creal a - real_of_creal b\<bar> < real_of_rat (1/2^(n+1)) \<Longrightarrow> 
    (creal_approx_eq n a b) = Some True"
  and
   "\<bar>real_of_creal a - real_of_creal b\<bar> > real_of_rat (1/2^(n)) \<Longrightarrow> 
    (creal_approx_eq n a b) = Some False"

  using creal_approx_eq_partialinfo [of "a" "n" "b" "cr2r a" "cr2r b"]
  using real_of_creal_in_creal [of a]
  using real_of_creal_in_creal [of b]
  apply auto
  using aconv bconv apply force
  using aconv bconv by force


(****************************************************************************************)

section \<open>Division\<close>

fun creal_div :: "creal \<Rightarrow> creal \<Rightarrow> creal" where
  "creal_div r1 r2 n = 
    (* TODO: define CR division, eg as in AERN2 *)
    (case (r1 (n+1), r2 (n+1)) of (Some c1, Some c2) \<Rightarrow> Some (c1+c2) | _ \<Rightarrow> None)"

(*
fun creal_div :: "creal \<Rightarrow> creal \<Rightarrow> creal"
  where
  "creal_div r1 r2 n = 
    (case (r1 n, r2 n) of (Some (m1,e1), Some (m2,e2)) \<Rightarrow> 
      if m2-e2 \<le> 0 \<and> 0 \<le> m2+e2 
        then None 
        else Some (m1/m2, (e1 + e1*e2*\<bar>m2\<bar> + \<bar>m1/m2\<bar>*e2 ) / (\<bar>m2\<bar> - e2))
     | _ \<Rightarrow> None)"
*)

*)

(*
lemma "\<And>r e. 0 < e \<Longrightarrow> \<exists>q. \<bar>q2r q-r\<bar> \<le> q2r e"
proof -
  fix r :: real
  fix e :: error_bound
  assume e:"0 < e"
  let ?R = "rep_real r"
  have "Real.cauchy ?R"
    by (metis Quotient_def Quotient_real realrel_def)
  then obtain k where "\<forall>m\<ge>k. \<forall>n\<ge>k. \<bar>?R m - ?R n\<bar> < e"
    using e cauchyD by blast
  then have "\<forall>n\<ge>k. \<bar>?R k - ?R n\<bar> < e" by auto
  then have "\<forall>n\<ge>k. \<bar>q2r (?R k) - q2r (?R n)\<bar> < q2r e"
    by (metis of_rat_abs of_rat_diff of_rat_less)
  then have "\<bar>q2r (?R k)-r\<bar> \<le> q2r e"
    sorry
  then show "\<exists>q. \<bar>q2r q-r\<bar> \<le> q2r e"
    by auto
qed
*)

(*
lemma twoeps: "\<And>m. 2 * real_of_rat (1 / (2 * 2 ^ m)) = of_rat (1/2^m)"
proof-
  fix m
  show "2 * real_of_rat (1 / (2 * 2 ^ m)) = of_rat (1/2^m)"
  by (smt Groups.mult_ac(2) add_divide_distrib div_by_1 inverse_divide mult.right_neutral mult_2_right nonzero_mult_div_cancel_left of_rat_add zero_neq_numeral)
qed


lemma shift_realrep:
  assumes xcauchy: "Real.cauchy X"
  shows "realrel X (\<lambda>n . X (Suc n))"
proof-
  from xcauchy have "Real.cauchy (\<lambda>n . X (Suc n))"
    using cauchyI [of "(\<lambda>n . X (Suc n))"] cauchyD [of X]
    by (meson le_SucI)
  then show ?thesis using xcauchy
    by (auto simp:realrel_def cauchy_def vanishes_def) (meson le_SucI)
qed

lemma shift_realrep_Real:
  "Real.cauchy X \<Longrightarrow> Real.Real X = Real.Real (\<lambda>n . X (Suc n))"
  using shift_realrep [of X]
  by (simp add: eq_Real realrel_def)

lemma abs_Real_cauchy:
  "Real.cauchy (\<lambda>n. \<bar>X n\<bar>)"
  if xcauchy: "Real.cauchy X"
  using that cauchyI [of "(\<lambda>n. \<bar>X n\<bar>)"] cauchyD [of X]
    by (meson abs_triangle_ineq3 le_less_trans)

lemma abs_Real: 
  "Real.Real (\<lambda>n. \<bar>X n\<bar>) = \<bar>Real.Real (\<lambda>n. X n)\<bar>"
  if xcauchy: "Real.cauchy X"
proof (cases "Real.Real X < 0")
  case Xneg: True
    then have "Real.vanishes (\<lambda>n. \<bar>X n\<bar> + (X n))"
      using xcauchy positive_Real [of "(\<lambda>n. - (X n))"]
      by (auto simp: vanishes_def minus_Real less_real_def) fastforce
    then have XnegConc: "Real.Real (\<lambda>n. \<bar>X n\<bar>) = - Real.Real X"
      using xcauchy abs_Real_cauchy eq_Real minus_Real [of X]
      by (auto simp: add_Real)
    then show ?thesis using Xneg
      by (auto simp: abs_real_def)
  next
  case Xnotneg: False
  show ?thesis using Xnotneg
  proof (cases "Real.Real X > 0")
    case Xpos: True
    then have "Real.vanishes (\<lambda>n. \<bar>X n\<bar> - (X n))"
      using xcauchy positive_Real [of "(\<lambda>n. (X n))"]
      by (auto simp: vanishes_def add_Real less_real_def) fastforce
    then have XposConc: "Real.Real (\<lambda>n. \<bar>X n\<bar>) = Real.Real X"
      using xcauchy abs_Real_cauchy eq_Real minus_Real [of X]
      by (auto simp: add_Real)
    then show ?thesis using Xpos
      by (auto simp: abs_real_def)
  next
    case Xnotpos: False
      (* Real.Real X = 0 *)
    show ?thesis using Xnotneg Xnotpos
      using eq_Real [of X "\<lambda>n.0"] xcauchy
      using eq_Real [of "\<lambda>n.\<bar>X n\<bar>" "\<lambda>n.0"] abs_Real_cauchy [of X]
      using zero_real_def
      by (auto simp: abs_real_def eq_Real vanishes_def)
  qed
qed

*)


(*
fun eps2n :: "rat \<Rightarrow> nat" where
  "eps2n \<epsilon> = nat (ceiling (1/\<epsilon>))"

lemma arbitrary_acc: 
  fixes \<epsilon>::rat
  assumes epos: "\<epsilon> > 0"
  shows "1/2^(eps2n \<epsilon>) < \<epsilon>"
  using le_less_trans 
  using of_nat_ceiling [of "1/\<epsilon>"] 
  using of_nat_less_two_power [of "eps2n \<epsilon>"]
  by (smt div_by_1 epos eps2n.elims inverse_divide inverse_less_imp_less)

fun creal_of_real :: "real \<Rightarrow> creal" where
  "creal_of_real x = 
    Abs_creal (\<lambda>e. if 0 < e then Some (rep_real x (eps2n (e/2)), e/2) else None)"

lemma creal_of_real: "let cr = creal_of_real x in x \<in>ar cr"
  using Abs_creal_inverse [of "\<lambda>e. if 0 < e then Some (rep_real x (eps2n (e/2)),e/2) else None"]
  using Rep_real [of x]
  apply (auto simp add: rep_real_def realrel_def Real.cauchy_def)
  by auto
*)

end

