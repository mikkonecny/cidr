\documentclass[parskip=half]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[x11names]{xcolor}
\usepackage{hyperref}
\usepackage[textsize=scriptsize]{todonotes}
\usepackage{amsmath,amssymb,amsthm,mathtools,mathabx,listings}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}[theorem]{Conjecture}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}

\newcommand*\mto{\rightrightarrows}
\newcommand*\pto{\rightharpoonup}
\newcommand*\pmto{\rightrightharpoons}

\newcommand*\mv{\mathrm{mv}}
\newcommand*\sv{\mathrm{sv}}

\newcommand*\sign{\operatorname{sign}}
\newcommand*\kif{\operatorname{kif}}
\newcommand*\apprx{\operatorname{approx}}
\newcommand*\meq{\operatorname{eq}\nolimits_\mv}
\newcommand*\dom{\operatorname{dom}}

\newcommand*\lang[1]{\text`\textcolor{blue}{\text{\ttfamily{#1}}}\text'{}}


\newcommand{\true}{\mathsf{T}}\newcommand{\false}{\mathsf{F}}\newcommand{\indeterminate}{\mathsf{I}}
\newcommand{\tpN}{\mathbb{N}}\newcommand{\tpZ}{\mathbb{Z}}\newcommand{\tpQ}{\mathbb{Q}}
\newcommand{\tpK}{\mathbb{K}}\newcommand{\tpR}{\mathbb{R}}
\newcommand{\tpX}{\mathbb{X}}\newcommand{\tpY}{\mathbb{Y}}
\newcommand{\tpOp}{\mathcal{O}}\newcommand{\tpCl}{\mathcal{A}}\newcommand{\tpRg}{\mathcal{R}}
\newcommand{\tpOv}{\mathcal{V}}\newcommand{\tpCp}{\mathcal{K}}\newcommand{\tpLc}{\mathcal{L}}
\newcommand{\tpPwCts}{\mathcal{PC}}\newcommand{\tpCts}{\mathcal{C}}
\newcommand{\tpMeas}{\mathcal{M}}

\theoremstyle{definition}
\newtheorem{defn}{Definition}
\bibliographystyle{alphaurl}

\lstdefinestyle{ttstyle}{
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=[1]{\bfseries\color{blue}},
  keywordstyle=[2]{\bfseries\color{red!90!black}},
  commentstyle=\itshape\color{gray},
  identifierstyle=\color{green!40!black},
  stringstyle=\color{orange},
  numbers=left,
  numberstyle=\scriptsize,
}

\lstdefinelanguage{cidr}{
  sensitive=true,
  % [1] single-valued constructs
  keywords=[1]{while,if,then,else,return},
  % [2] multi-valued constructs
  keywords=[2]{approx,kif,eq_mv},
  % [3] primitive types
  %keywords=[3]{Boolean,Kleenean,Nat,Rational,Real},
  otherkeywords={:=,!=,==,<,<=,>,>=},
  comment=[s]{/*}{*/},
}

\lstset{language=cidr,style=ttstyle}

\begin{document}

\title{\texttt{CIDR} -- a realistic language for verifiable programs on real numbers}
\author{F.B., P.C., M.K, N.M., E.N, ...?}
\maketitle

\section{Goals}


\subsection{Example problems}

\begin{itemize}
\item
	Approximating square root with Newtons method,
	$\mathbb Q_+\times \mathbb{R} \to  \mathbb{R}$
	on $z\geq 0$.
	\begin{quote}\begin{lstlisting}
Real sqrt_approx(Rational p, Real z) {
   Real a := 1;
   while !eq_mv(a, z/a, p) do
    /* or |approx(a - z/a, p/2)| > p/2 */
    /* or a !=_p z/a */
   {
      a := (a + z/a) / 2;
   }
   return a;
}
	\end{lstlisting}\end{quote}

\item Binary logarithm $\log_2: \mathbb{R} \pmto  \mathbb{Z}$
\item Modulo function $\bmod: \mathbb{R}^2 \pmto  \mathbb{R}$
\item Inversion by Gaussian elimination $\mathbb{R}^{n\times n} \pto  \mathbb{R}^{n\times n}$
\item Trisection for root finding (including an application of a limit operator)
\end{itemize}



\section{Syntax and Semantics (all mixed up...)}

Notation:
For sets $A,B$ we denote multi-valued functions from $A$ to $B$ by $A\mto B$,
partial functions by $A\pto B$ and partial multi-valued functions by $A\pmto B$.

We refer to the sets
$\mathbb B=\{T,F\}$ and $\mathbb K=\mathbb B\cup\{\bot\}$ as
boolean and kleenean respectively.

For a set $A$, the set of finite words over $A$ is denoted by $A^*$ and
$A^+$ means non-empty words over $A$. In contrast, for ordered sets $B$ and
$0\in B$, the notation $B_+$ refers to $\{b\in B:0<b\}$.
For a set $A$ and multi-index
$n=(n_1\times\cdots\times n_k)=(n_1,\ldots,n_k)\in\mathbb N^k$,
$k\geq 0$, let $A^n$ refer to $\bigtimes_{i=1}^k A^{n_i}$.
\todo{do we want it \emph{typed} (e.g.\ overloading)?}
The empty word is denoted by $\epsilon$.

In some cases, the behaviour is explicitely
\todo{explicate meaning of \emph{undefined} for a program and a run of a program}
\emph{undefined}, e.g.\ for $\kif(\bot,1,2)$.

Requirements:
\todo[inline]{These should be untangled and moved to the appropriate sections
	about syntax and/or semantics}
\begin{itemize}
\item
	\emph{Primitive-type}s are
	\lang{Boolean}, \lang{Kleenean}, \lang{Nat}, \lang{Rational}, \lang{Real}
	(for $\mathbb B,\mathbb K,\mathbb N,\mathbb Q,\mathbb R$, respectively).
	\todo[inline]{%
		Important subtypes:
		$\mathbb{D}$ for fast arithmetic,
		$\mathbb{Q}_+$ for error bounds,
		intervals?}
\item
	\emph{Compound-type}s include the \emph{primitive-type}s.
	If $A,B$ are \emph{compound-type}s and $n\in\mathbb N^+$ such that
	$|n|>1$ and $n>1$ component-wise,
	then $A^n$ and $A\times B$
	are \emph{compound-type}s.
\item
	If $A,B$ are
	\emph{compound-type}s, then
	\todo{Syntax for $\to,\mto,\pto,\pmto$}
	$A\to_1 B$, $A\mto_1 B$, $A\pto_1 B$ and $A\pmto_1 B$
	are \emph{function type}s \emph{of order $1$}.
	\todo[inline]{Allow first-order objects
		(at least, see trisection, as parameters)}
\item
	Named constants \lang{True}, \lang{False} and \lang{Bottom} for
	$T,F,\bot$, respectively.
\item
	Literals of some syntax.
\item
	Type annotations for literals and constants
	(to disambiguate expressions).
	\begin{itemize}
	\item Type relations? Type casts?
	\end{itemize}
\item
	Unary/Binary operators:
	\begin{itemize}
	\item \lang +, \lang -, \lang *, \lang /, \lang <, \lang{<=}, \lang{>=}, \lang > on arithmetic types
	\item \lang\&, \lang |, \lang ! on logic types
	\item \lang{!=}, \lang{==} on all types?
	\item how about composite or restricted types?
	\item \emph{(partial)-function-application}.

		Let $f$ be an expression of \emph{function-type}
		$A\mathbin\theta_1 B$ where
		$\theta\in\{{\to},{\mto},{\pto},{\pmto}\}$,
		$B$ is of \emph{compound-type} and
		$A$ is either of \emph{compound-type}
		\todo{Treat void-fn as constant? Would prohibit taking
			$\lim_2$ of constant functions.}
		or $\epsilon$.
		\todo[inline]{Type of $\epsilon$ or don't allow overloaded
			constants? (latter probably more intuitive)}
		\begin{itemize}
		\item
			If $a$ is $\epsilon$ or an expression of
			\emph{compound-type} $A$ then
			\lang{$f$($a$)} is a
			\emph{function-call-expression} of type $B$.
			\todo[inline]{specify semantics of
				\emph{function-call-expressions} taking bound
				out-of-domain values into account}
		\item
			If $A=(A_1,\ldots,A_n)$ for
			\emph{compound-type}s $A_i$,
			$i\in\{1,\ldots,n\}$, $n\geq 1$,
			and if $a_i$ is an expression of type $A_i$ for an
			$i\in\{1,\ldots,n\}$, then
			\lang{bind($f$,$i$,$a_i$)} is an
			expression of type
			$(A_1,\ldots,A_{i-1},A_{i+1},\ldots,A_n)\mathbin\theta_1 B$.
		\end{itemize}
		\begin{itemize}
		\item call-by-value semantics, no side-effects.
		\end{itemize}
	\end{itemize}
%\item
%	The $\sign:\mathbb R\to\mathbb K$ function \lang{sign}
%	defined as $x\mapsto\begin{cases}
%		F & x < 0 \\
%		T & x > 0 \\
%		\bot &x=0
%	\end{cases}$.
\item
	\todo{maybe defined in terms of the $\sign$ function by
	e.g.\ $x\mathbin\theta y\coloneqq\sign(x-y)\mathbin\theta 0$}
	comparisons $\theta\in\{\neq,=,<,\leq,>,\geq\}:\mathbb R\times\mathbb R\to\mathbb K$.
\item
	Imperative constructs define programs $P$:
	\begin{itemize}
	\item
		For an expression $b$ of type \lang{Boolean} and a program $p$,
		the \emph{loop} \lang{while $b$ do $p$} is a program.
	\item
		For programs $p_1,p_2,\ldots,p_n$, $n\geq 0$,
		the \emph{sequence} \lang{\{$p_1p_2\ldots p_n$\}} is a program.
	\item
		For an expression $b$ of type \lang{Boolean} and programs $p_1,p_2$
		the \emph{option}
		\lang{if $b$ then $p_1$}
		\todo{A dangling \lang{else} [reference...] is attached to the
			inner-most \lang{if}.}
		and the \emph{choice} \lang{if $b$ then $p_1$ else $p_2$}
		is a program, respectively.
	\end{itemize}
	\todo[inline]{Definition and association/assignment of programs to
		function(-name)s and assignment of references(?)\ thereof to
		variables of \emph{function-type}.}
\item
	\emph{Variable-declaration-assignment}s \lang{$T$ $v$:=$e$;} of
	the value of an expression $e$ of type $T$ to a variable $v$ of type $T$,
	\emph{assignments} \lang{$v$:=$e$;} of the value of an expressions $e$
	of type $T$ to a variables $v$ of type $T$, and \emph{return-statement}s
	\lang{return $e$;} of expressions $e$ are programs.
\item
	\emph{expressions} include
	\emph{(partial)-function-application}.
\item
	Identifiers of variables, functions and types follow
	\todo{define \emph{naming-scheme}} some \emph{naming-scheme}.
\item
	$\kif:\mathbb K\times\mathbb R^2\to\mathbb R$,
	$(k,x,y)\mapsto\begin{cases}
		x &\text{if}~k=\lang{True} \\
		y &\text{if}~k=\lang{False} \\
		x\,(=y) &\text{if}~k=\lang{Bottom}~\text{and}~x=y \\
		\text{undefined} &\text{otherwise}
	\end{cases}$
\item
	$\apprx:\mathbb R\times\mathbb Q_+\mto\mathbb Q$,
	$q\in\apprx(x,p)\implies|x-q|\leq p$.
\item
	$\meq:\mathbb R^2\times\mathbb Q_+\mto\mathbb B$%,
	%$\lang{True}\in\meq(x,y,p)\implies|x-y|\leq p$ and
	%$\forall x\exists p:\lang{True}\in\meq(x,x,p)$
	, $\meq(x,y,p)\in\begin{cases}
		\lang{False} & \text{if}~|x-y|>2^{p-1} \\
		\lang{True}  & \text{if}~|x-y|<2^p
	\end{cases}$
\item
	Variants of limits of sequences of
	\todo{In light of $\Pi^0_1$ shouldn't these be rational (approximations)?}
	real numbers.
	\begin{itemize}
	\item
		$\lim\nolimits_1:(x_n)\mapsto x$ for Cauchy-sequences $(x_n)$
		converging to $x$ expressed by
		\begin{itemize}
		\item
			a function of type $\lang{Nat}\mathbin\theta A$,
			where $A$ is a \emph{compound-type} and
			$\theta\in\{\to,\mto\}$,
		\item other expressions (e.g.\ inline recursively defined)?
		\end{itemize}
	\item
		$\lim\nolimits'_1:y\mapsto x$ for expressions $y$ of
		\emph{function-type} $\mathbb Q_+\mathbin\theta\lang{Real}$ for
		$\theta\in\{\to,\mto\}$.
	\end{itemize}
\item
	Variants of limits of continuous functions.

	Let $A,B$ \todo{define \emph{metric-type}} metric \emph{compound-type}s.
	Let $(f_n)$ be a sequence of functions
	$f_n:A\pto B$, for $n\in\mathbb N$, converging to an $f:A\pto B$
	locally uniformly with modulus $\mu:\mathbb Q_+\times A\pmto\mathbb N$,
	that is $\forall x\in\dom(f)$ there is a neighbourhood $B$ of $x$
	such that $\forall q\in\mathbb Q_+$ there is $N\in\mu(q,x)$ such that
	$\forall n\geq N:\sup_{y\in B\cap\dom(f)}\Vert f_n(y)-f(y)\Vert\leq q$.
	Additionally, let $g:\mathbb Q_+\times A\pmto B$ such that
	$\forall x\in\dom(f)\,\forall q\in\mathbb Q_+
	:\Vert g(q,x)-f(x)\Vert\leq q$.

	Then one or both of these limit operators on functions could be useful
	(plus variations for the multi-valued function types):
	\begin{itemize}
	\item
		$\lim\nolimits_2:(\mu,(f_n))\mapsto f$ for $(f_n)$
		expressed by a function of type $\lang{Nat}\times A\pmto B$
		and a modulus bounded by the function $\mu$ of type
		$\mathbb Q_+\times A\pmto\lang{Nat}$.
	\item
		$\lim\nolimits'_2:g\mapsto f$ for $g$ expressed by a function of
		type $\mathbb Q_+\times A\pmto B$.
	\end{itemize}
	\todo[inline,author=FB]{$\lim\nolimits_2$ has $\mu$, which is probably
		required for verification, stated explicitely}
\end{itemize}

Specify semantics by defining how a virtual machine interprets programs.



\section{Function types}

The function calculus part of CIDR will support different types of functions.
Each type has a defining property, equivalent properties,and supported operations.

We use as fundamental types the real numbers $\tpR$, and types $\tpX$, $\tpY$, $\tpZ$ which are based on quotients of countably-based (QCB) spaces.
We also use the \emph{Kleenean} type $\tpK$ with values $\true$ (true), $\false$ (false) and $\indeterminate$ (indeterminate), and the \emph{Sierpinskian} or \emph{upper Kleenean} subtype $\tpK_>$, with values $\{\true,\indeterminate\}$.
For a type $\tpX$, we also consider types of its \emph{compact} sets $\tpCp(\tpX)$, \emph{open} sets $\tpOp(\tpX)$, \emph{closed} sets $\tpCl(\tpX)$ and \emph{regular} sets $\tpRg(\tpX)$

\begin{description}
 \item[Continuous functions] $\tpCts(\tpX;\tpY) := (\tpX \to \tpY)$. Defined by \emph{evaluation}.
   \par Support \emph{composition} $\tpCts(\tpY;\tpZ) \times \tpCts(\tpX;\tpY) \to \tpCts(\tpX;\tpZ):g,f\mapsto g\circ f$ defined by $[g\circ f](x) = g(f(x))$.
   \par On a compact subset of $\tpR^n$, equivalent to the completion of the polynomials under the uniform norm.
 \item[Differentiable functions] $\tpCts^1(\tpR^n;\tpR)$ defined by evaluation of the function itself, and its \emph{(partial) derivatives} $\partial_k f$ or $f_{,k}$.
   \par Note that differentiation is an uncomputable operation, so the derivative must be given explicitly.
   \par Combining evaluation of the function and its derivatives is usually efficient. This extends to an action on \emph{differentials}: $f(x_0+x_1 dt) := f(x_0) + \sum_{k} x_{1,k} f_{,k}(x_0) dt$.
 \item[Piecewise-continuous functions] $\tpPwCts(\tpX;\tpY)$. Defined by evaluation on \emph{pieces} $f_i:X_i \to \tpY$, where $X_i : \tpRg(\tpX)$ form a (locally-finite) \emph{topological cover} $X_{i_1}^\circ\cap X_{i_2}^\circ=\emptyset$ ($i_1\neq i_2$), and $\bigcup_{i} X_i = \tpX$.
  \par The pieces may be conveniently defined by separating functions e.g. $X_i=\{ x \mid g_1(x) \lesssim 0\}$, $X_2=\{ x \mid g_1(x) \gtrsim 0 \wedge g_2(x) \lesssim 0\}$,  $X_3=\{ x \mid g_1(x) \gtrsim 0 \wedge g_2(x) \gtrsim 0\}$.
  \par In Euclidean space, the pieces may be defined by boxes, or on the real line by intervals. The endpoints may be real numbers, or be restricted to lie in some decidable subsets, such as the dyadics or algebraic numbers.
 \item[Measurable functions] If $(\tpX,\mu)$ is a measure space, a type of \emph{measurable functions} $\tpMeas(\tpX;\tpY)$ (alternatively denoted $\tpX \rightsquigarrow \tpY$ defined by the measures $\mu\bigl( U\cap f^{-1}(V)\bigr)$ for $U:\tpOp(\tpX)$, $V:\tpOp(\tpY)$, such that $f(x)$ can be ``defined almost everywhere''.
  \par If $\tpY$ is a metric space, may be defined as the completion of the full-measure piecewise-continuous functions under the Fan metric
  \[ \begin{aligned} d(f_1,f_2) &= \inf\{ \epsilon \in \tpQ^+ \mid \mu(\{x\in \tpX \mid d(f_1(x),f_2(x)) > \epsilon\} < \epsilon\} \\
                                &= \sup\{ \epsilon \in \tpQ^+ \mid \mu(\{x\in \tpX \mid d(f_1(x),f_2(x)) > \epsilon\} > \epsilon\} . \end{aligned}  \]
  Alternatively, full-measure piecewise constant functions may be used, and/or piecewise functions on a topological partition into boxes.
 \item[Integrable functions] Measurable functions $\tpMeas(\tpX;\tpR)$ with finite Lebesgue norm $\|f\|_p = \int_\tpX |f(x)|^p\,d\mu(x)$.
  Alternatively, the completion under the Lebesgue norm of the piecewise continuous functions.
\end{description}

\begin{definition}\todo{Check this definition is correct}
Let $(\tpX,\mu)$ be an effective measure space. A function $\tpOp(\tpX) \times \tpOp(\tpY) \to \tpR^+<$ \emph{defines almost everywhere} a measurable function $f:\tpX \rightsquigarrow \tpY$ if for almost every $x:tpX$, there exists $y:\tpY$, such that for all open $V\ni y$ and $\epsilon>0$, there exists open $W \ni x$ such that whenever $U$ is open and $x\in U\subset W$, then $\mu(U)>0$ and $\mu(U\cap f^{-1}(V))/\mu(U)>1-\epsilon$.
\end{definition}
\begin{conjecture}
Let $(\tpX,\mu)$ be an effective measure space and $\tpY$ an effective metric space.
Then given a function $\tpOp(\tpX) \times \tpOp(\tpY) \to \tpR^+<$ which defines almost everywhere a (noncomputable) function $f$ from $\tpX$ to $\tpY$, then it is possible to compute a sequence of piecewise-continuous functions $f_n: \tpX \pto \tpY$ defined on full-measure open sets, such that $f_n$ converges in the Fan metric, with limit almost everywhere to $f$.
\end{conjecture}


\appendix
\section{Grammar}

\end{document}
