# CIDR - Computing with Infinite Data - Reals 

Integrated and Verified Exact Real Computation with support for Function calculi

## Folders:

* `lang-imperative`: A document defining the CIDR language and giving some example programs
    * an early draft, mainly by Franz Brausse
* `cidr-aern2`: A mini prototype shallow embedding of a functional fragment of CIDR to Haskell/AERN2
    * by Michal Konečný
* `cidr-agda`: A mini prototype formalisation of a functional fragment of CIDR in Agda
    * currently inactive, by Michal Konečný
* `cidr-isabelle`: A prototype formalisation of a functional fragment of CIDR in Isabelle/HOL
    * work in progress, by Michal Konečný
    * formalisation down to the level of operational semantics suitable for automatic code generation
    * verifying pre- and post-conditions as well as termination
