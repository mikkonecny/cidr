module Coercible where

open import Relation.Nullary.Decidable using (fromWitness)

open import Data.Nat using (ℕ)
import Data.Nat.Coprimality as ℕ-Coprim

open import Data.Integer using (ℤ)
import Data.Integer as ℤ

open import Data.Rational using (ℚ)
import Data.Rational as ℚ

ℤ→ℚ : ℤ → ℚ
ℤ→ℚ z = (z ℚ.÷ 1)  {fromWitness (λ {i} → proof {i})}
  where
  open ℕ-Coprim
  proof : Coprime (ℤ.∣ z ∣) 1
  proof = sym (1-coprimeTo (ℤ.∣ z ∣))

ℕ→ℚ : ℕ → ℚ
ℕ→ℚ n = ℤ→ℚ (ℤ.+ n)

record Coercible {a} (A B : Set a) : Set a where
  field
    coerce : A → B

open Coercible {{...}} public

instance
  coercible-ℕ-ℤ : Coercible ℕ ℤ
  coerce {{coercible-ℕ-ℤ}} = ℤ.+_

instance
  coercible-ℕ-ℚ : Coercible ℕ ℚ
  coerce {{coercible-ℕ-ℚ}} = ℕ→ℚ

instance
  coercible-ℤ-ℚ : Coercible ℤ ℚ
  coerce {{coercible-ℤ-ℚ}} = ℤ→ℚ

instance
  coercible-ℕ-ℕ : Coercible ℕ ℕ
  coerce {{coercible-ℕ-ℕ}} n = n

instance
  coercible-ℤ-ℤ : Coercible ℤ ℤ
  coerce {{coercible-ℤ-ℤ}} n = n

instance
  coercible-ℚ-ℚ : Coercible ℚ ℚ
  coerce {{coercible-ℚ-ℚ}} n = n

Hasℕ : Set → Set
Hasℕ A = Coercible ℕ A
fromℕ : {A : Set} → {{_ : Hasℕ A}} → ℕ → A
fromℕ = coerce

Hasℤ : Set → Set
Hasℤ A = Coercible ℤ A
fromℤ : {A : Set} → {{_ : Hasℤ A}} → ℤ → A
fromℤ = coerce

Hasℚ : Set → Set
Hasℚ A = Coercible ℚ A
fromℚ : {A : Set} → {{_ : Hasℚ A}} → ℚ → A
fromℚ = coerce

CanBeℤ : Set -> Set
CanBeℤ A = Coercible A ℤ
toℤ : {A : Set} → {{_ : CanBeℤ A}} → A → ℤ
toℤ = coerce

CanBeℚ : Set -> Set
CanBeℚ A = Coercible A ℚ
toℚ : {A : Set} → {{_ : CanBeℚ A}} → A → ℚ
toℚ = coerce
