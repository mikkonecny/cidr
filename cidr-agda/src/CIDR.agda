module CIDR where

{-
  USER STORIES:

    Numerical analysts & engineers:

    - can write and read CIDR programs easily

    - can translate the programs to equivalent and fast iRRAM/Ariadne/AERN2

    - can specify the meaning of the program

    - can prove that the programs' axiomatic semantics satisfies the specification

    - can translate CIDR programs to equivalent vanilla Haskell

      - this translation is proved correct, reducing the trusted base

      - (?) the generated code is not too much slower than the generated iRRAM/Ariadne/AERN2 code

  ROUTES TO VERIFICATION:

  - Give the CIDR terms executable Agda semantics,
    specify and prove properties of the semantics.

    - This semantics is based on CauchyReal.

    - Advantage: this semantics can be compiled to vanilla Haskell.

  - Give the CIDR terms logical semantics
    and specify and prove a Hoare triple.

    - This semantics is based on AxiomaticReal.

  - One can do both of the above and prove equivalence of the two semantics.

    - This gives axiomatic proofs + provably correct vanilla Haskell implementation.

  INTERESTING POINTS

  - Multi-valued functions

    - The postcondition can be a predicate satisfied by multiple results.

  - Partial functions

    - Logical semantics specifies the domain in the pre-condition.

  - Executing "while" programs that are not terminating or where termination is hard to prove.

    - Agda allows one to overrride the restriction to execute only provably terminating programs.

-}

module Examples where

  open import Data.Product
  open import Data.Integer using (+_)
  open import Data.Rational
  open import Data.String

  open import CIDR.Syntax

  e1Name : String
  e1Name = "1 + (1/3)"

  e1 : Term Context.empty ℝ
  e1 =
    (realℕ 1) + (realℚ ((+ 1) ÷ 3))

  -- sqrtApprox :
  --   Term
  --     (ContextAVL.fromList ( ( varP , ℕ ) ∷ ( varX , ℝ ) ∷ [] ) )
  --     ℝ
  -- sqrtApprox = _

module Verification where
  open import Data.Integer using (+_)
  open import Data.Rational

  open import Coercible
  open import AxiomaticReal using (IsReal)
  open import CIDR.LogicalSemantics
  open Examples

  module _ (R : Set) { isRealR : IsReal R } (result : R) where
    open IsReal isRealR

    e1Spec : Set
    e1Spec =
      ⟦ e1 ⟧ R {isRealR} emptyValuation result → (result ≅ (ℚ→ℝ ((+ 4) ÷ 3)))

    -- -- a lemma that could be useful in proving e1Spec:
    -- e1sum : (result ≅ (ℚ→ℝ (ℕ→ℚ 1)) + (ℚ→ℝ ((+ 1) ÷ 3))) → (result ≅ (ℚ→ℝ ((+ 4) ÷ 3)))
    -- e1sum = _ -- TODO: need more IsReal axioms first!
    --
    -- e1SpecCorrect : e1Spec
    -- e1SpecCorrect =
    --   e1sum
{-
The above proof attempt gives the following error message, which gives us hints
on what kind of lemmas we need to complete the proof:

Type mismatch:
expected: result ≅ ℚ→ℝ (ℕ→ℚ 1) + ℚ→ℝ (+ 1 ÷ 3)
  actual: (r₁ r₂ : R) →
⟦ .CIDR.Syntax.Terms.Term.realℕ 1 ⟧ R emptyValuation r₁ →
⟦ .CIDR.Syntax.Terms.Term.realℚ (+ 1 ÷ 3) ⟧ R emptyValuation r₂ →
result ≅ r₁ + r₂
when checking that the expression e1sum
has type e1Spec
-}


module Execute where
  open import Data.Unit using (⊤)

  open import IO
  open import Coinduction
  open import Data.String

  import CauchyReal as CR using (show_atP_)

  open import CIDR.Syntax
  open import CIDR.OperationalSemantics

  open Examples

  mainIO : IO ⊤
  mainIO =
    ♯(putStr (e1Name ++ " ∈ "))
    >>
    ♯(putStrLn (CR.show (⟦ e1 ⟧ emptyValuation) atP 999999999999999999999999999999))

  main : _
  main = run mainIO

open Execute using (main)
