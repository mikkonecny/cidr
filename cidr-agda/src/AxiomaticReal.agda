module AxiomaticReal where

import Level using (zero)

open import Relation.Binary

open import Data.Maybe

open import Data.Nat using (ℕ)
import Data.Nat as ℕ

open import Data.Integer using (ℤ)
import Data.Integer as ℤ

open import Data.Rational using (ℚ)
import Data.Rational as ℚ

open import Coercible

record IsReal (R : Set) : Set₁ where
  field
    -- equality:
    _≅_ : Rel R Level.zero
    ≅-equivalence : IsEquivalence _≅_
    -- not IsDecEquivalence

    -- order:
    _≲_ : Rel R Level.zero
    ≲-total-order : IsTotalOrder _≅_ _≲_
    -- not IsDecTotalOrder

    -- conversions:
    ℚ→ℝ : ℚ → R

    looseCeilingℕ : R → ℕ -- many-valued

    fromℚ-looseCeilingℕ : ∀ r → r ≲ ℚ→ℝ (ℕ→ℚ (looseCeilingℕ r))

    -- archimedean : (r l : R) → (l ≤ l + l ∧ ¬(l + l ≅ l)) → ∃ (n : ℕ) → r ≤ l * (fromℚ (ℕ-to-ℚ n))

    _+_ : R → R → R
    _-_ : R → R → R
    _*_ : R → R → R
    _/_ : R → R → R
    _^_ : R → R → R

    -- TODO: add proof that R is an ordered Field

    -- lim : (seq : ℕ → R) → (Convergent (λ r₁ r₂ → (r₁ + (neg r₂)) ) seq) → R

  infix  4 _≅_ _≲_
  infixl 6 _+_ _-_
  infixl 7 _*_ _/_
  infixl 8 _^_

  carrier : Set
  carrier = R

  CanBeℝ : Set -> Set
  CanBeℝ A = Coercible A R
  toℝ : {A : Set} → {{_ : CanBeℝ A}} → A → R
  toℝ = coerce

  -- coercion of rational numbers to real numbers:
  instance
    coercible-A-viaℚ-ℝ : {A : Set} → {{_ : Coercible A ℚ}} → Coercible A R
    coerce {{ coercible-A-viaℚ-ℝ {A} {{hasℚ}} }} a = ℚ→ℝ (toℚ a)

open IsReal public
