module IntegerInterval where

open import Data.Nat using (ℕ)
import Data.Nat as ℕ
open import Data.Integer using (ℤ)

module _ where
  open import Data.Product
  open import Data.Integer
  open import Coercible

  record 𝕀ℤ (p : ℕ) : Set where -- indexed by precision p
    constructor [_,_]
    field
      l : ℤ
      r : ℤ

  _ConsistentWith_ : {p₁ p₂ : ℕ} → 𝕀ℤ p₁ → 𝕀ℤ p₂ → Set
  _ConsistentWith_ {p₁} {p₂} [ l₁ , r₁ ] [ l₂ , r₂ ] =
    (denom₁ * l₂ ≤ denom₂ * r₁) × (denom₁ * l₁ ≤ denom₂ * r₂)
    where
    import Data.Nat as ℕ
    denom₁ = + (1 ℕ.+ p₁)
    denom₂ = + (1 ℕ.+ p₂)


module _ {p : ℕ} where -- for some fixed precision p
  Valid : 𝕀ℤ p → Set
  Valid [ l , r ] = l ≤ r
    where open Data.Integer

  _±1 : ℤ → 𝕀ℤ p
  _±1 x = [ (- (+ 1)) + x  , (+ 1) + x ]
    where open Data.Integer

  width : 𝕀ℤ p -> ℕ
  width [ l , r ] = ∣ r - l ∣
    where open Data.Integer

  module _ where
    open import Relation.Binary.PropositionalEquality
    open ≡-Reasoning
    import Data.Nat.Properties.Simple as ℕ
    open Data.Integer
    import Data.Integer.Properties as ℤ

    width-±1 : (x : ℤ) → (width (x ±1) ≡ 2)
    width-±1 x = aux x
      where
      aux2 : (n : ℕ) → ∣ (2 ℕ.+ n) ⊖ n ∣ ≡ 2
      aux2 n =
        begin
          ∣ (2 ℕ.+ n) ⊖ n ∣
              ≡⟨ cong (λ m -> ∣ m ⊖ n ∣ ) (ℕ.+-comm 2 n) ⟩
          ∣ (n ℕ.+ 2) ⊖ n ∣
              ≡⟨ cong (λ m -> ∣ (n ℕ.+ 2) ⊖ m ∣ ) (ℕ.+-comm 0 n) ⟩
          ∣ (n ℕ.+ 2) ⊖ (n ℕ.+ 0) ∣
              ≡⟨ cong (∣_∣) (ℤ.+-⊖-left-cancel n 2 0 ) ⟩
          ∣ + 2 ∣
              ∎

      aux : (x : ℤ) → ∣ ((+ 1) + x) - ((- (+ 1)) + x) ∣ ≡ 2
      aux (+ ℕ.zero) = refl
      aux (+ (ℕ.suc ℕ.zero)) = refl
      aux (+ (ℕ.suc (ℕ.suc n))) = aux2 n
      aux (-[1+ ℕ.zero ]) = refl
      aux (-[1+ (ℕ.suc n) ]) = aux2 n

  _WidthBoundedBy_ : 𝕀ℤ p → ℕ -> Set
  i WidthBoundedBy k =
    -- |r-l|/(p+1) ≤ 1/(k+1)
    (width i) * (1 + k) ≤ (1 + p)
    where open import Data.Nat

-- enclose a rational number by an interval
module _ where

  open import Data.Rational using (ℚ)
  import Data.Rational as ℚ

  approxℚ-by-ℤ : ℚ → ℕ → ℤ
  approxℚ-by-ℤ q p =
    (sign (numerator q))
    ◃
    (∣ numerator q ∣ * p) div (suc (denominator-1 q))
    where
    open Data.Nat; open import Data.Nat.DivMod; open Data.Integer using (_◃_;sign;∣_∣); open ℚ.ℚ

  approxℚ : ℚ → (p : ℕ) → 𝕀ℤ p
  approxℚ q p = (approxℚ-by-ℤ q p) ±1

  module _ where
    open import Relation.Nullary.Decidable
    open import Relation.Binary using (DecTotalOrder)
    open import Relation.Binary.PropositionalEquality
    open ≡-Reasoning

    open Data.Nat
    open import Data.Nat.Properties.Simple

    approxℚ-widthBounded : (q : ℚ) → (k : ℕ) → ((approxℚ q (1 + 2 * k)) WidthBoundedBy k)
    approxℚ-widthBounded q k =
      DecTotalOrder.reflexive decTotalOrder aux
        where
        aux2 : k + (1 + (k + 0)) ≡ 1 + (k + (k + 0))
        aux2 = begin
          k + (1 + (k + 0))
              ≡⟨ +-comm k (1 + (k + 0)) ⟩
          1 + ((k + 0) + k)
              ≡⟨ cong (_+_ 1) (+-comm (k + 0) k) ⟩
          1 + (k + (k + 0))
              ∎

        p : ℕ
        p = 1 + 2 * k

        aux : width (approxℚ q p) * (1 + k) ≡ 2 + (2 * k)
        aux = begin
          width (approxℚ q p) * (1 + k)
              ≡⟨ cong (λ m -> m * (1 + k) ) (width-±1 {p} (approxℚ-by-ℤ q p)) ⟩
          1 + k + (1 + (k + 0))
              ≡⟨ cong suc aux2 ⟩
          2  + (2 * k)
              ∎

    -- -- TODO
    -- approxℚ-consistent : (q : ℚ) → (p₁ p₂ : ℕ) → ((approxℚ q p₁) ConsistentWith (approxℚ q p₂))
    -- approxℚ-consistent q p₁ p₂ = _

module _ {p : ℕ} where

  _+_ : 𝕀ℤ p → 𝕀ℤ p → 𝕀ℤ p
  [ l1 , r1 ] + [ l2 , r2 ] = [ l1 ℤ.+ l2 , r1 ℤ.+ r2 ]
    where import Data.Integer as ℤ
