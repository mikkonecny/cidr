module CauchyReal where

open import Data.Nat using (ℕ)
import Data.Nat as ℕ

module _ where
  open import IntegerInterval

  record ℝ : Set where
    constructor ℝseq_
    field
      seq : (p : ℕ) → 𝕀ℤ p
      -- seq p = [ l , r ]  encodes the rational interval [l,r]/(1+p)

-- formatting a real number
module _ where
  open import Data.String hiding (show)
  open import IntegerInterval

  show_atP_ : ℝ → ℕ → String
  show (ℝseq seq) atP p
      with seq p | 1 ℕ.+ p
  ...| [ numerL , numerR ] | denom =
    "[ " ++ (show numerL) ++ " .. " ++ (show numerR) ++ " ] / "
    ++ (show (+ denom))
    where
    open import Data.Integer

-- is a real number well defined?
module _ where
  open import IntegerInterval

  record Converges (r : ℝ) : Set where
    field
      Consistent : (p₁ p₂ : ℕ) → ((ℝ.seq r p₁) ConsistentWith (ℝ.seq r p₂))
      rate : (k : ℕ) → ℕ
      WidthShrinks : (k : ℕ) → (ℝ.seq r (rate k)) WidthBoundedBy k

  open Converges {{...}} public

-- conversion from discrete numbers
module _ where
  open import Coercible
  open import Data.Rational
  open import IntegerInterval hiding (_+_)

  CanBeℝ : Set -> Set
  CanBeℝ A = Coercible A ℝ
  toℝ : {A : Set} → {{_ : CanBeℝ A}} → A → ℝ
  toℝ = coerce

  -- coercion of rational numbers to real numbers:
  instance
    coercible-A-viaℚ-ℝ : {A : Set} → {{_ : Coercible A ℚ}} → Coercible A ℝ
    coerce {{ coercible-A-viaℚ-ℝ {A} {{hasℚ}} }} a =
      ℝseq (approxℚ (toℚ a))

  -- -- TODO
  -- converges-ℚ-coerce : (q : ℚ) → Converges (toℝ q)
  -- Consistent {{converges-ℚ-coerce q}} p₁ p₂ = _
  -- rate {{converges-ℚ-coerce q}} k = 1 + (2 * k)
  --     where open Data.Nat
  -- WidthShrinks {{converges-ℚ-coerce q}} k = approxℚ-widthBounded q k


_+_ : ℝ → ℝ → ℝ
(ℝseq a) + (ℝseq b) = ℝseq c
  where
  import IntegerInterval as 𝕀ℤ

  c : _
  c = λ p → (a p) 𝕀ℤ.+ (b p)
