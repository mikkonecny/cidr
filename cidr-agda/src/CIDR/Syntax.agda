module CIDR.Syntax where

open import Data.Bool using (Bool)
import Data.Nat as Nat
import Data.Rational as Rational

module Types where

  open import Relation.Nullary
  open import Relation.Binary
  open import Relation.Binary.PropositionalEquality

  data Type : Set where
    𝔹 : Type
    -- 𝕂 : Type -- Kleenean
    ℕ : Type
    ℝ : Type
    _×-t_ : Type → Type → Type
    _→-t_ : Type → Type → Type

open Types public

module Variables where
  VarName : Set
  VarName = Nat.ℕ

  varX varY varZ varP : VarName
  varX = 1
  varY = 2
  varZ = 3
  varP = 10

open Variables public

module Context where

  open import Data.List
  open import Data.List.Any
  open import Data.Product
  open import Relation.Binary.PropositionalEquality

  Context : Set
  Context = List (VarName × Type)

  _WithType_InContext_ : VarName → Type → Context → Set
  _WithType_InContext_ vname vtype c = Any equal c
    where
    equal : VarName × Type → Set
    equal vt = vt ≡ (vname , vtype)

  empty : Context
  empty = []

open Context using (Context; _WithType_InContext_) public

module Terms where

  -- infix  4 _≅_ _≲_
  infixl 6 _+_ _+ℕ_ -- _-_
  -- infixl 7 _*_ _/_
  -- infixl 8 _^_

  data Term : Context → Type →  Set₁ where
    -- clet_:=_cin_ : { tv t : Type } → VarName × Context → Term tv → Term t → Term t
    var :  (t : Type) → (v : VarName) → { c : Context } → { vtc : v WithType t InContext c } → Term c t
    -- appl : { t₁ t₂ : Type } → Term (t₁ →-t t₂) → Term t₁ → Term t₂
    -- _,_ : { t₁ t₂ : Type } → Term t₁ → Term t₂ → Term (t₁ ×-t t₂)
    -- proj₁ : { t₁ t₂ : Type } → Term (t₁ ×-t t₂) → Term t₁
    -- proj₂ : { t₁ t₂ : Type } → Term (t₁ ×-t t₂) → Term t₂

    nat : Nat.ℕ → {c : Context} → Term c ℕ
    -- real : {A : _} → {{_ : CanBeℚ A}} → A → {c : Context} → Term c ℝ -- allows natural, integer and rational literals
    realℚ : Rational.ℚ → {c : Context} → Term c ℝ -- allows only rational literals, working around a likely bug in Agda
    realℕ : Nat.ℕ → {c : Context} → Term c ℝ

    -- TODO: allow other type combinations
    _+_ : {c : Context} → Term c ℝ → Term c ℝ → Term c ℝ
    _+ℕ_ : {c : Context} → Term c ℕ → Term c ℕ → Term c ℕ
    -- _-_ : {c : Context} → Term c ℝ → Term c ℝ → Term c ℝ
    -- _*_ : {c : Context} → Term c ℝ → Term c ℝ → Term c ℝ
    -- _/_ : {c : Context} → Term c ℝ → Term c ℝ → Term c ℝ
    -- _^_ : {c : Context} → Term c ℝ → Term c ℕ → Term c ℝ

    -- mv-≲ : {c : Context} → Term c ℕ → Term c ℝ → Term c ℝ → Term c 𝔹
    -- mv-≅ : {c : Context} → Term c ℕ → Term c ℝ → Term c ℝ → Term c 𝔹
    -- if_then_else_ : {t : Type} → {c : Context} → Term c 𝔹 → Term c t → Term c t → Term c t
    --
    -- ≲ : {c : Context} → Term c ℝ → Term c ℝ → Term c 𝕂
    -- ≅ : {c : Context} → Term c ℝ → Term c ℝ → Term c 𝕂
    -- kif_then_else_ : {t : Type} → {c : Context} → Term c 𝕂 → Term c t → Term c t → Term c t
    -- -- TODO: for this we need a "unify" operation for all types
    --
    -- limit : {t : Type} → {c : Context} → (fastSequence : Term c (ℕ →-t t)) → Term c t
    --
    -- while_s=_loop_end-loop :
    --   {t s : Type} →
    --   {c : Context} →
    --   (init : Term c (t ×-t s)) →
    --   (condition : Term c ((t ×-t s) →-t 𝔹)) →
    --   (body : Term c ((t ×-t s) →-t (t ×-t s))) →
    --   Term c t

open Terms public
