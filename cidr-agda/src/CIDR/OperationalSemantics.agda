module CIDR.OperationalSemantics where

open import Data.Product
open import Data.Bool using (Bool)
import Data.Nat as Nat

open import Coercible
import CauchyReal as CR

open import CIDR.Syntax

⟦_⟧t : Type → Set
⟦ 𝔹 ⟧t = Bool
⟦ ℕ ⟧t = Nat.ℕ
⟦ ℝ ⟧t = CR.ℝ
⟦ t₁ ×-t t₂ ⟧t = ⟦ t₁ ⟧t × ⟦ t₂ ⟧t
⟦ t₁ →-t t₂ ⟧t = ⟦ t₁ ⟧t → ⟦ t₂ ⟧t

Valuation : Context -> Set
Valuation c = (v : VarName) → {t : Type} → { vtc : v WithType t InContext c } → ⟦ t ⟧t

emptyValuation : Valuation Context.empty
emptyValuation v {t} {()}

⟦_⟧ : {t : Type} → {c : Context} → Term c t → Valuation c → ⟦ t ⟧t

⟦ var t vname {c} {vtc} ⟧ valuation = valuation vname {t} {vtc}

-- ⟦ t satisfies _ ⟧ valuation = ⟦ t ⟧ valuation

⟦ nat n ⟧ _ = n

⟦ realℕ n ⟧ _ = CR.toℝ n
⟦ realℚ q ⟧ _ = CR.toℝ q

⟦ t₁ + t₂ ⟧ valuation = (⟦ t₁ ⟧ valuation) CR.+ (⟦ t₂ ⟧ valuation)

⟦ t₁ +ℕ t₂ ⟧ valuation = (⟦ t₁ ⟧ valuation) Nat.+ (⟦ t₂ ⟧ valuation)

-- ⟦ _ ⟧ _ = _ -- TODO
