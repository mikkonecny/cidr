module CIDR.LogicalSemantics where

open import Data.Product
open import Relation.Binary.PropositionalEquality

open import Data.Bool using (Bool)
import Data.Nat as Nat

open import Coercible

open import AxiomaticReal using (IsReal)

open import CIDR.Syntax

module TypesLogicalSemantics where

  ⟦_⟧t : Type → (R : Set) → (S2S : Set₁) → Set₁
  ⟦ 𝔹 ⟧t R S2S         = Bool → S2S
  ⟦ ℕ ⟧t R S2S         = Nat.ℕ → S2S
  ⟦ ℝ ⟧t R S2S         = R → S2S
  ⟦ t₁ ×-t t₂ ⟧t R S2S = (⟦ t₁ ⟧t R) (⟦ t₂ ⟧t R S2S)
  ⟦ t₁ →-t t₂ ⟧t R S2S = (⟦ t₂ ⟧t R S2S) → (⟦ t₂ ⟧t R S2S)
  -- ⟦ _ ⟧t = _
open TypesLogicalSemantics using (⟦_⟧t)

Valuation : Context → Set₁
Valuation c =  (R : Set) → (v : VarName) → {t : Type} → { vtc : v WithType t InContext c } → ⟦ t ⟧t R Set

emptyValuation : Valuation Context.empty
emptyValuation R v {t} {()}

⟦_⟧ : {t : Type} → {c : Context} → Term c t → (R : Set) → { isRealR : IsReal R } → Valuation c → ⟦ t ⟧t R Set

⟦ var t vname {c} {vtc} ⟧ R valuation = valuation R vname {t} {vtc}

-- ⟦ t satisfies P ⟧ R {isRealR} valuation = ⟦ t ⟧ R {isRealR} valuation

⟦ nat n ⟧ R _ result = (result ≡ n)

⟦ realℕ n ⟧ R {isRealR} _ result = (result ≅ (ℚ→ℝ (ℕ→ℚ n)))
  where open IsReal isRealR

⟦ realℚ q ⟧ R {isRealR} _ result = (result ≅ (ℚ→ℝ q))
  where open IsReal isRealR
-- ⟦ real n ⟧ _ R {isRealR} u = ((ℚ→ℝ (toℚ n)) ≅ u) -- Agda gives up
--   where open IsReal isRealR
-- ⟦ real {{canbeℚ}} n ⟧ _ R {isRealR} u = ((ℚ→ℝ (toℚ {{canbeℚ}} n)) ≅ u) -- makes Agda loop!
--   where open IsReal isRealR

⟦ t₁ + t₂ ⟧ R {isRealR} valuation result =
  ∀ r₁ r₂ → (⟦ t₁ ⟧ R {isRealR} valuation r₁) → (⟦ t₂ ⟧ R {isRealR} valuation r₂) → (result ≅ r₁ AR.+ r₂)
  where module AR = IsReal isRealR; open AR

⟦ t₁ +ℕ t₂ ⟧ R {isRealR} valuation result =
  ∀ n₁ n₂ → (⟦ t₁ ⟧ R {isRealR} valuation n₁) → (⟦ t₂ ⟧ R {isRealR} valuation n₂) → (result ≡ n₁ Nat.+ n₂)
-- ⟦ _ ⟧ _ R = _
