# cidr-agda

A pilot (partial) implementation of CIRD deep embedding in Agda
with both operational semantics via a shallow embedding with constructive reals
and a logical semantics based on an axiomatic definition of real numbers.

To execute the (trivial) example, you need to install `ghc` and then run:

```
cd src
agda --compile CIDR.agda
MAlonzo/Code/CIDR
```

If you use Haskell stack, run the following instead:

```
cd src
agda --compile CIDR.agda
stack ghc -- --make MAlonzo/Code/CIDR.hs -main-is MAlonzo.Code.CIDR
MAlonzo/Code/CIDR
```
